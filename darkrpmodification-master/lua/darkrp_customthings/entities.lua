    --[[---------------------------------------------------------------------------
    DarkRP custom entities
    ---------------------------------------------------------------------------
    This file contains your custom entities.
    This file should also contain entities from DarkRP that you edited.
    Note: If you want to edit a default DarkRP entity, first disable it in darkrp_config/disabled_defaults.lua
    	Once you've done that, copy and paste the entity to this file and edit it.
    The default entities can be found here:
    https://github.com/FPtje/DarkRP/blob/master/gamemode/config/addentities.lua#L111
    For examples and explanation please visit this wiki page:
    http://wiki.darkrp.com/index.php/DarkRP:CustomEntityFields
    Add entities under the following line:
    ---------------------------------------------------------------------------]]
    DarkRP.createEntity("Heavy Armor", {
    	ent = "heavy kevlar armor",
    	model = "models/combine_vests/bogvest.mdl",
    	price = 0,
    	max = 1,
    	cmd = "heavyarmor",
		allowed = {TEAM_MGALEKGOLO, TEAM_SOSJACKELMEDIC, TEAM_SOSHEAVYGRUNT, TEAM_HOJB, TEAM_HOJSC, TEAM_HOJWJ, TEAM_ALPHACOMM, TEAM_BSQUADPLEAD, TEAM_MILITARYPOLICERIOT, TEAM_MILITARYPOLICERIOTLEADER, TEAM_CHIEFDRILLINST, TEAM_DOGMAWARLEAD, TEAM_DOGMAOFFLEAD, TEAM_RHINOTROOPER, TEAM_RHINOWARRLEAD, TEAM_RHINOOFFLEAD, TEAM_GOLFEODOFFLEAD, TEAM_GOLFEODTROOP, TEAM_GOLFWARRLEAD, TEAM_ASYLUMFLAMER, TEAM_ASYLUMSQUADLEAD}, 
    	customCheck = function(ply) return !ply.HeavyArmour end,
    	CustomCheckFailMsg = function(ply, entTable) return "You're only allowed to spawn one per life!" end,
    })
    DarkRP.createEntity("Medium Armor", {
    	ent = "medium kevlar armor",
    	model = "models/combine_vests/bogvest.mdl",
    	price = 0,
    	max = 1,
    	cmd = "mediumarmor",
    	customCheck = function(ply) return !ply.MediumArmour end,
    	CustomCheckFailMsg = function(ply, entTable) return "You're only allowed to spawn one per life!" end,
		allowed = {TEAM_SOSJACKAL, TEAM_SOSSPECOPSS, TEAM_SOSRJACK, TEAM_HOJHC, TEAM_HOJHGG, TEAM_HOJLC, TEAM_ALPHACOXO, TEAM_ALPHACOSGT, TEAM_BSQUADSARG, TEAM_BSQUADXO, TEAM_MILITARYPOLICESGT, TEAM_SMMCMARINES, TEAM_SDRILLINST, TEAM_GENERALMARINES, TEAM_KSARGT, TEAM_KSQUADPLEAD, TEAM_KSQUADLEAD, TEAM_VSARGT, TEAM_VSQUADPLEAD, TEAM_VSQUADLEAD, TEAM_ZSQUADPLEAD, TEAM_ZSARGT, TEAM_ZSQUADLEAD, TEAM_WHISKEYOFFLEAD, TEAM_WHISKEYWARRLEAD, TEAM_WSUPPORT, TEAM_WSHARPSHOOTER, TEAM_WRIFLEMAN, TEAM_WCORPSMAN, TEAM_DOGMAJTROOP, TEAM_ASYLUMASSAULT, TEAM_ASYLUMMEDIC},
    })
    DarkRP.createEntity("Light Armor", {
    	ent = "light kevlar armor",
    	model = "models/combine_vests/bogvest.mdl",
    	price = 0,
    	max = 1,
    	cmd = "lightarmor",
    	customCheck = function(ply) return !ply.LightArmour end,
    	CustomCheckFailMsg = function(ply, entTable) return "You're only allowed to spawn one per life!" end,
		allowed = {TEAM_SOSGRUNT, TEAM_SOSRGRUNT, TEAM_SOSOPGRUNT, TEAM_HOJ, TEAM_HOJS, TEAM_HOJJPJ, TEAM_MILITARYPOLICERECRUIT, TEAM_MILITARYPOLICE, TEAM_DRILLINST, TEAM_KCORPSMAN, TEAM_KRIFLEMAN, TEAM_KSHARPSHOOTER, TEAM_KSUPPORT, TEAM_VSUPPORT, TEAM_VSHARPSHOOTER, TEAM_VRIFLEMAN, TEAM_VCORPSMAN, TEAM_ZSUPPORT, TEAM_ZSHARPSHOOTER, TEAM_ZRIFLEMAN, TEAM_ZCORPSMAN},
    })