--[[-----------------------------------------------------------------------
Categories
---------------------------------------------------------------------------
The categories of the default F4 menu.

Please read this page for more information:
http://wiki.darkrp.com/index.php/DarkRP:Categories

In case that page can't be reached, here's an example with explanation:

DarkRP.createCategory{
    name = "Citizens", -- The name of the category.
    categorises = "jobs", -- What it categorises. MUST be one of "jobs", "entities", "shipments", "weapons", "vehicles", "ammo".
    startExpanded = true, -- Whether the category is expanded when you open the F4 menu.
    color = Color(0, 107, 0, 255), -- The color of the category header.
    canSee = function(ply) return true end, -- OPTIONAL: whether the player can see this category AND EVERYTHING IN IT.
    sortOrder = 100, -- OPTIONAL: With this you can decide where your category is. Low numbers to put it on top, high numbers to put it on the bottom. It's 100 by default.
}


Add new categories under the next line!
---------------------------------------------------------------------------]]
DarkRP.createCategory{
    name = "UNSC",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(0, 51, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 50, 
}
DarkRP.createCategory{
    name = "Military Police",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(0, 0, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 50, 
}
DarkRP.createCategory{
    name = "Kilo",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(218, 242, 67, 255),
    canSee = function(ply) return true end, 
    sortOrder = 51, 
}
DarkRP.createCategory{
    name = "Lima 3-1 Platoon",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(81, 65, 44, 255),
    canSee = function(ply) return true end, 
    sortOrder = 51, 
}
DarkRP.createCategory{
    name = "Lima 2-1 Platoon",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(33, 115, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 52, 
}
DarkRP.createCategory{
    name = "Lima 1-1 Platoon",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(4, 186, 16, 255),
    canSee = function(ply) return true end, 
    sortOrder = 53, 
}
DarkRP.createCategory{
    name = "HQ Command",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(161, 28, 28, 255),
    canSee = function(ply) return true end, 
    sortOrder = 54, 
}
DarkRP.createCategory{
    name = "Oscar",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(128, 0, 255, 255),
    canSee = function(ply) return true end, 
    sortOrder = 55, 
}
DarkRP.createCategory{
    name = "Sierra",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(239, 126, 243, 255),
    canSee = function(ply) return true end, 
    sortOrder = 55, 
}
DarkRP.createCategory{
    name = "Tombstone",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(109,109, 109, 255),
    canSee = function(ply) return true end, 
    sortOrder = 56, 
}
DarkRP.createCategory{
    name = "UNSC Central Fleet",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(0, 0, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 57, 
}
DarkRP.createCategory{
    name = "Naval Operations",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(0, 0, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 57, 
}
DarkRP.createCategory{
    name = "Naval STG",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(0, 0, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 57, 
}
DarkRP.createCategory{
    name = "Naval IA",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(0, 0, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 57, 
}
DarkRP.createCategory{
    name = "Naval Heads of Department",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(0, 0, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 57, 
}
DarkRP.createCategory{
    name = "Naval R&D",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(0, 0, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 57, 
}
DarkRP.createCategory{
    name = "Naval Command",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(0, 0, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 57, 
}
DarkRP.createCategory{
    name = "Vulcan",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(102, 0, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 58, 
}
DarkRP.createCategory{
    name = "Bulldog",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(255, 102, 102, 255),
    canSee = function(ply) return true end, 
    sortOrder = 59, 
}
DarkRP.createCategory{
    name = "Nemesis",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(51, 0, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 60, 
}
DarkRP.createCategory{
    name = "ONI",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(160, 160, 160, 255),
    canSee = function(ply) return true end, 
    sortOrder = 61, 
}
DarkRP.createCategory{
    name = "ONI Security Force",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(0, 0, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 61, 
}
DarkRP.createCategory{
    name = "Eclipse",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(0, 0, 255, 255),
    canSee = function(ply) return true end, 
    sortOrder = 62, 
}

DarkRP.createCategory{
    name = "Freelancer",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(32, 32, 32, 255),
    canSee = function(ply) return true end, 
    sortOrder = 64, 
}
DarkRP.createCategory{
    name = "Hellhound Squad Lead",
    categorises = "jobs",
    startExpanded = true,
    color = Color(32, 32, 32, 255),
    canSee = function(ply) return true end,
    sortOrder = 64,
}
DarkRP.createCategory{
    name = "AI",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(51, 51, 255, 255),
    canSee = function(ply) return true end, 
    sortOrder = 65, 
}
DarkRP.createCategory{
    name = "Admin",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(51, 51, 255, 255),
    canSee = function(ply) return true end, 
    sortOrder = 66, 
}
DarkRP.createCategory{
    name = "Swords of Sanghelios",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(127, 0, 255, 255),
    canSee = function(ply) return true end, 
    sortOrder = 67, 
}
DarkRP.createCategory{
    name = "Forerunner",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(100, 100, 100, 255),
    canSee = function(ply) return true end, 
    sortOrder = 67, 
}
DarkRP.createCategory{
    name = "Recon 1-2",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(32, 32, 32, 255),
    canSee = function(ply) return true end, 
    sortOrder = 68, 
}
DarkRP.createCategory{
    name = "Cerberus",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(62, 44, 90, 255),
    canSee = function(ply) return true end, 
    sortOrder = 54, 
}
DarkRP.createCategory{
    name = "Bullfrogs",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(54, 47, 47, 255),
    canSee = function(ply) return true end, 
    sortOrder = 55, 
}
DarkRP.createCategory{
    name = "Alpha 9",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(109,109, 109, 255),
    canSee = function(ply) return true end, 
    sortOrder = 100, 
}
DarkRP.createCategory{
    name = "Exodus",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(255, 153, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 100, 
}
DarkRP.createCategory{
    name = "Orion",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(10, 124, 188, 255),
    canSee = function(ply) return true end, 
    sortOrder = 100, 
}
DarkRP.createCategory{
    name = "Xerxes",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(105, 105, 105, 255),
    canSee = function(ply) return true end, 
    sortOrder = 64, 
}
DarkRP.createCategory{
    name = "Eden",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(199, 175, 31, 255),
    canSee = function(ply) return true end, 
    sortOrder = 64, 
}
DarkRP.createCategory{
    name = "Nomad",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(210, 180, 140, 255),
    canSee = function(ply) return true end, 
    sortOrder = 65, 
}
DarkRP.createCategory{
   name = "The Last Chancers",
   categorises = "jobs",
   startExpanded = true,
   color = Color(0, 128, 128, 255),
   canSee = function(ply) return true end,
   sortOrder = 15,
}
DarkRP.createCategory{
    name = "ONI Headhunters",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(165, 165, 165, 250),
    canSee = function(ply) return true end, 
    sortOrder = 68, 
}
DarkRP.createCategory{
    name = "Alpha Six",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(165, 165, 165, 250),
    canSee = function(ply) return true end, 
    sortOrder = 69, 
}
DarkRP.createCategory{
    name = "Guardian",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(165, 165, 165, 250),
    canSee = function(ply) return true end, 
    sortOrder = 70, 
}
DarkRP.createCategory{
    name = "Sentinel",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(165, 165, 165, 250),
    canSee = function(ply) return true end, 
    sortOrder = 71, 
}
DarkRP.createCategory{
    name = "Armored Angels",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(138, 138, 138, 255),
    canSee = function(ply) return true end, 
    sortOrder = 71, 
}
DarkRP.createCategory{
    name = "Sabre",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(165, 165, 165, 250),
    canSee = function(ply) return true end, 
    sortOrder = 72, 
}
DarkRP.createCategory{
    name = "Phoenix",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(165, 165, 165, 250),
    canSee = function(ply) return true end, 
    sortOrder = 73, 
}
DarkRP.createCategory{
    name = "Fox 5-1",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(165, 165, 165, 250),
    canSee = function(ply) return true end, 
    sortOrder = 74, 
}
DarkRP.createCategory{
    name = "Sabre Pilot",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(0, 0, 255, 250),
    canSee = function(ply) return true end, 
    sortOrder = 70, 
}

DarkRP.createCategory{
    name = "MAJCOM",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(0, 0, 0, 250),
    canSee = function(ply) return true end, 
    sortOrder = 70, 
}

DarkRP.createCategory{
    name = "Valkyrie",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(255, 0, 0, 250),
    canSee = function(ply) return true end,   
    sortOrder = 70, 
}

DarkRP.createCategory{
    name = "Other",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(255, 0, 0, 250),
    canSee = function(ply) return true end, 
    sortOrder = 70, 
}

DarkRP.createCategory{
    name = "ODST Command",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(255, 0, 0, 250),
    canSee = function(ply) return true end, 
    sortOrder = 70, 
}
DarkRP.createCategory{
    name = "Terra Squad",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(255, 0, 0, 250),
    canSee = function(ply) return true end, 
    sortOrder = 70, 
}
DarkRP.createCategory{
    name = "Asylum 8-3",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(27, 72, 89, 250),
    canSee = function(ply) return true end, 
    sortOrder = 70, 
}
DarkRP.createCategory{
    name = "Geiger Company",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(255, 0, 0, 250),
    canSee = function(ply) return true end, 
    sortOrder = 70, 
}
DarkRP.createCategory{
    name = "Men Of Steel",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(255, 0, 0, 250),
    canSee = function(ply) return true end, 
    sortOrder = 70, 
}
DarkRP.createCategory{
    name = "UNSC Baker Brigade",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(0, 0, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 50, 
}
DarkRP.createCategory{
    name = "UNSC Headquarters Element",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(71, 0, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 50, 
}
DarkRP.createCategory{
    name = "Alpha Infantry Company",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(117, 0, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 50, 
}
DarkRP.createCategory{
    name = "Bravo Support Platoon",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(0, 0, 117, 255),
    canSee = function(ply) return true end, 
    sortOrder = 50, 
}
DarkRP.createCategory{
    name = "Pararescue",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(168, 0, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 50, 
}
DarkRP.createCategory{
    name = "Hellfire",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(122, 0, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 70, 
}
DarkRP.createCategory{
    name = "Insurrection",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(53, 53, 53, 255),
    canSee = function(ply) return true end, 
    sortOrder = 70, 
}
DarkRP.createCategory{
    name = "Demon Squad",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(53, 0, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 80, 
}
DarkRP.createCategory{
    name = "Inferno Squad",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(53, 0, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 80, 
}
DarkRP.createCategory{
    name = "Phalanx Commander’s",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(0, 102, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 80, 
}
DarkRP.createCategory{
    name = "Crimson",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(220, 20, 60, 255),
    canSee = function(ply) return true end, 
    sortOrder = 80, 
}
DarkRP.createCategory{
    name = "War Dogs",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(220, 20, 60, 255),
    canSee = function(ply) return true end, 
    sortOrder = 80, 
}

DarkRP.createCategory{
    name = "Trident",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(75,0,130,255),
    canSee = function(ply) return true end, 
    sortOrder = 70, 
}

DarkRP.createCategory{
    name = "Orion",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(46,139,87,255),
    canSee = function(ply) return true end, 
    sortOrder = 90, 
}

DarkRP.createCategory{
    name = "HIGHCOM",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(50,50,50,255),
    canSee = function(ply) return true end, 
    sortOrder = 100, 
}

DarkRP.createCategory{
    name = "Devil Dogs",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(220, 20, 60, 255),
    canSee = function(ply) return true end, 
    sortOrder = 80, 
}