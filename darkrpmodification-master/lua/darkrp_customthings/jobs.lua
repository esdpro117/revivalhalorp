--[[---------------------------------------------------------------------------
DarkRP custom jobs
---------------------------------------------------------------------------
This file contains your custom jobs.
This file should also contain jobs from DarkRP that you edited.
Note: If you want to edit a default DarkRP job, first disable it in darkrp_config/disabled_defaults.lua
	Once you've done that, copy and paste the job to this file and edit it.
The default jobs can be found here:
https://github.com/FPtje/DarkRP/blob/master/gamemode/config/jobrelated.lua
For examples and explanation please visit this wiki page:
http://wiki.darkrp.com/index.php/DarkRP:CustomJobFields
Add jobs under the following line:
---------------------------------------------------------------------------]]
TEAM_RECRUIT = DarkRP.createJob("Recruit", {  
   color = Color(0, 51, 0, 255),
   model = {"models/ishi/halo_rebirth/offduty/male/offduty_heretic.mdl"},
   description = [[A UNSC Recruit, Pledged to fight in the Human-Covenant war, They trained in camp before they were stationed with the 405th]],
   weapons = {},
   command = "recruit",
   max = 0,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "UNSC",
})

TEAM_KRIFLEMAN = DarkRP.createJob("Lima 3-1 Rifleman", { -- Edit done by Head General 4/14 (Changed from Kilo to Lima 3-1)
   color = Color(2, 97, 0, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_bella.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_snippy.mdl"},
   description = [[A UNSC grunt force, Kilo 2-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"tfa_h4_spartan_weapon_saw","tfa_rebirth_ma5c","tfa_rebirth_m6g","csgo_huntsman"},
   command = "krifleman",
   max = 20,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Kilo",
PlayerSpawn = function(ply)
   ply:SetHealth(100)
   ply.HaloArmor=20
   ply:SetMaxHealth(100)
end
})

TEAM_WSUPPORT = DarkRP.createJob("Lima 3-1 Support", {
   color = Color(81, 65, 44, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_bella.mdl","models/ishi/halo_rebirth/player/marines/male/marine_snippy.mdl"},   
   weapons = {"tfa_rebirth_m41","tfa_rebirth_m90","Tfa_h4_spartan_weapon_saw","h3_br_tfa","halo_tfa_assaultrifle","halo_frag","Tfa_rebirth_m7ds","tfa_rebirth_m6g","climb_swep2", "tfa_m247_gpmg"},
   description = [[Fearless, Ambitious and Loyal, Lima 3-6 was one of the few marine regiments to storm the front lines.]],
   command = "wsupport",
   max = 5,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Lima 3-1 Platoon",
PlayerSpawn = function(ply)
   ply:SetHealth(275)
   ply.HaloArmor=63
   ply:SetMaxHealth(275)
   ply:GiveAmmo(5, "grenade")
end
})
TEAM_WSHARPSHOOTER = DarkRP.createJob("Lima 3-1 Sharpshooter", {
   color = Color(81, 65, 44, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_bella.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_snippy.mdl"},
   description = [[Fearless, Ambitious and Loyal, Lima 3-1 was one of the few marine regiments to storm the front lines.]],
   weapons = {"tfa_rebirth_srs99s2am","tfa_rebirth_m392","tfa_rebirth_m90","halo_frag","Tfa_rebirth_m7ds","tfa_rebirth_m392","tfa_rebirth_m6g","Climb_swep2"},
   command = "wsharpshooter",
   max = 5,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Lima 3-1 Platoon",
PlayerSpawn = function(ply)
   ply:SetHealth(225)
   ply.HaloArmor=63
   ply:SetMaxHealth(255)
   ply:GiveAmmo(5, "grenade")
end
})
TEAM_WRIFLEMAN = DarkRP.createJob("Lima 3-1 CQC Specialist", {
   color = Color(81, 65, 44, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_bella.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_snippy.mdl"},
   description = [[Fearless, Ambitious and Loyal, Lima 3-1 was one of the few marine regiments to storm the front lines.]],
   weapons = {"climb_swep2","tfa_rebirth_m90","halo_tfa_assaultrifle","halo_frag","h3_br_tfa","huntcam","tfa_rebirth_m90","tfa_rebirth_m6g","Climb_swep2","","tfa_rebirth_m7ds"},
   command = "wrifleman",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Lima 3-1 Platoon",
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply.HaloArmor=58
   ply:SetMaxHealth(250)
   ply:GiveAmmo(5, "grenade")
end
})
TEAM_WCORPSMAN = DarkRP.createJob("Lima 3-1 Corpsman", {
   color = Color(81, 65, 44, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_bella.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_snippy.mdl"},
   description = [[Fearless, Ambitious and Loyal, Lima 3-6 was one of the few marine regiments to storm the front lines.]],
   weapons = {"tfa_rebirth_m7ds","tfa_rebirth_m6g","tfa_rebirth_m392","weapon_bactainjector","med_kit","darkrp_defibrillator","climb_swep2"},
   command = "wcorpsman",
   max = 5,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Lima 3-1 Platoon",
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply.HaloArmor=43
   ply:SetMaxHealth(250)
   ply:GiveAmmo(5, "grenade")
end
})
TEAM_VSUPPORT = DarkRP.createJob("Lima 2-1 Support", {
   color = Color(0, 107, 31, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_bella.mdl","models/ishi/halo_rebirth/player/marines/male/marine_snippy.mdl"},
   description = [[A UNSC grunt force, Lima 2-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"tfa_rebirth_ma5d","tfa_rebirth_m394","tfa_h4_spartan_weapon_saw","csgo_huntsman","tfa_rebirth_m6g","keys","swep_radiodevice"}, -- Edit done by Head General(removed LMG)
   command = "vsupport",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Lima 2-1 Platoon",
PlayerSpawn = function(ply)
   ply:SetHealth(175)
   ply.HaloArmor=45
   ply:SetMaxHealth(175)
end
})
TEAM_VSHARPSHOOTER = DarkRP.createJob("Lima 2-1 Sharpshooter", {
   color = Color(0, 107, 31, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_bella.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_snippy.mdl"},
   description = [[A UNSC grunt force, Lima 2-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"tfa_rebirth_ma5d","tfa_rebirth_m394","tfa_rebirth_srs99c2b","csgo_huntsman","tfa_rebirth_m6g","weapon_rpw_binoculars","keys","swep_radiodevice","climb_swep2"},
   command = "vsharpshooter",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Lima 2-1 Platoon",
PlayerSpawn = function(ply)
   ply:SetHealth(125)
   ply.HaloArmor=45
   ply:SetMaxHealth(125)
end
})
TEAM_VRIFLEMAN = DarkRP.createJob("Lima 2-1 Rifleman", {
   color = Color(0, 107, 31, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_bella.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_snippy.mdl"},
   description = [[A UNSC grunt force, Lima 2-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"tfa_rebirth_ma5d","tfa_rebirth_m394","csgo_huntsman","tfa_rebirth_m6g","keys","swep_radiodevice"},
   command = "vrifleman",
   max = 20,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Lima 2-1 Platoon",
PlayerSpawn = function(ply)
   ply:SetHealth(150)
   ply.HaloArmor=45
   ply:SetMaxHealth(150)
end
})
TEAM_VCORPSMAN = DarkRP.createJob("Lima 2-1 Corpsman", {
   color = Color(0, 107, 31, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_bella.mdl","models/ishi/halo_rebirth/player/marines/male/marine_snippy.mdl"},
   description = [[A UNSC grunt force, Lima 2-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"tfa_rebirth_ma5d","weapon_bactainjector","med_kit","darkrp_defibrillator","tfa_rebirth_m394","csgo_huntsman","tfa_rebirth_m6g","keys","swep_radiodevice"},
   command = "vcorpsman",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Lima 2-1 Platoon",
PlayerSpawn = function(ply)
   ply:SetHealth(150)
   ply.HaloArmor=45
   ply:SetMaxHealth(150)
end
})
TEAM_VSQUADLEAD = DarkRP.createJob("Lima 2-1 Squad Leader", {
   color = Color(0, 107, 31, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_bella.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_jeffrey.mdl"},
   description = [[A UNSC grunt force, Lima 2-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"tfa_rebirth_ma5d","tfa_rebirth_m394","csgo_huntsman","tfa_rebirth_m6g","weapon_rpw_binoculars","keys","voice_amplifier","swep_radiodevice"},
   command = "vsquadleader",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Lima 2-1 Platoon",
PlayerSpawn = function(ply)
   ply:SetHealth(150)
   ply.HaloArmor=45
   ply:SetMaxHealth(150)
end
})
TEAM_VSQUADPLEAD = DarkRP.createJob("Lima 2-1 Actual", {
   color = Color(0, 107, 31, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_bella.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_jeffrey.mdl"},
   description = [[A UNSC grunt force, Lima 2-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"tfa_rebirth_ma5d","tfa_rebirth_m394","csgo_huntsman","tfa_rebirth_m6g","weapon_rpw_binoculars","keys","climb_swep2","voice_amplifier","tfa_rebirth_m90","swep_radiodevice"},
   command = "vsquadpleader",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Lima 2-1 Platoon",
PlayerSpawn = function(ply)
   ply:SetHealth(200)
   ply.HaloArmor=45
   ply:SetMaxHealth(200)
end
})
TEAM_LSUPPORT = DarkRP.createJob("Lima 1-1 Support", {
   color = Color(4, 186, 16, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_bella.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_snippy.mdl"},
   description = [[A UNSC grunt force, Lima 1-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"tfa_rebirth_ma5d","tfa_rebirth_br55","tfa_h4_spartan_weapon_saw","csgo_huntsman","tfa_rebirth_m6g","keys","swep_radiodevice"}, -- Edit done by Head General 4/14 (removed LMG)
   command = "lsupport",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Lima 1-1 Platoon",
PlayerSpawn = function(ply)
   ply:SetHealth(175)
   ply.HaloArmor=48
   ply:SetMaxHealth(175)
end
})
TEAM_LSHARPSHOOTER = DarkRP.createJob("Lima 1-1 Sharpshooter", {
   color = Color(4, 186, 16, 255),
   model = {"models/ishi/halo_rebirth/player/marines/male/marine_snippy.mdl","models/ishi/halo_rebirth/player/marines/female/marine_bella.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_snippy.mdl"},
   description = [[A UNSC grunt force, Lima 1-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"tfa_rebirth_ma5d","tfa_rebirth_br55","tfa_rebirth_srs99c2b","csgo_huntsman","tfa_rebirth_m6g","weapon_rpw_binoculars","keys","swep_radiodevice","climb_Swep2"},
   command = "lsharpshooter",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Lima 1-1 Platoon",
PlayerSpawn = function(ply)
   ply:SetHealth(125)
   ply.HaloArmor=45
   ply:SetMaxHealth(125)
end
})
TEAM_LRIFLEMAN = DarkRP.createJob("Lima 1-1 Rifleman", {
   color = Color(4, 186, 16, 255),
   model = {"models/ishi/halo_rebirth/player/marines/male/marine_snippy.mdl","models/ishi/halo_rebirth/player/marines/female/marine_bella.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_snippy.mdl"},
   description = [[A UNSC grunt force, Lima 1-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"tfa_rebirth_ma5d","tfa_rebirth_br55","csgo_huntsman","tfa_rebirth_m6g","keys","swep_radiodevice"},
   command = "zrifleman",
   max = 20,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Lima 1-1 Platoon",
PlayerSpawn = function(ply)
   ply:SetHealth(150)
   ply.HaloArmor=45
   ply:SetMaxHealth(150)
end
})
TEAM_LCORPSMAN = DarkRP.createJob("Lima 1-1 Corpsman", {
   color = Color(4, 186, 16, 255),
   model = {"models/ishi/halo_rebirth/player/marines/male/marine_snippy.mdl","models/ishi/halo_rebirth/player/marines/female/marine_bella.mdl"},
   description = [[A UNSC grunt force, Lima 1-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"tfa_rebirth_ma5d","weapon_bactainjector","med_kit","darkrp_defibrillator","tfa_rebirth_br55","csgo_huntsman","tfa_rebirth_m6g","keys","swep_radiodevice"},
   command = "lcorpsman",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Lima 1-1 Platoon",
PlayerSpawn = function(ply)
   ply:SetHealth(150)
   ply.HaloArmor=45
   ply:SetMaxHealth(150)
end
})
TEAM_LIMAACTUAL = DarkRP.createJob("Lima 1-1 Actual", {
   color = Color(4, 186, 16, 255),
   model = {"models/ishi/halo_rebirth/player/marines/male/marine_snippy.mdl","models/ishi/halo_rebirth/player/marines/female/marine_bella.mdl"},
   description = [[A UNSC grunt force, Lima 1-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"tfa_rebirth_ma5d","tfa_rebirth_br55","csgo_huntsman","tfa_rebirth_m6g","weapon_rpw_binoculars","keys","climb_swep2","voice_amplifier","tfa_rebirth_m90","swep_radiodevice"},
   command = "lima1actual",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Lima 1-1 Platoon",
PlayerSpawn = function(ply)
   ply:SetHealth(200)
   ply.HaloArmor=45
   ply:SetMaxHealth(200)
end
})
TEAM_BULLFROG = DarkRP.createJob("Bullfrog", {
   color = Color(54, 47, 47, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[Bullfrog jumpers specialized in extreme tasks in which they can use air travel to reach their goal.]],
   weapons = {"tfa_rebirth_m90","tfa_rebirth_ma37s", "tfa_rebirth_m6cs", "revival_c4", "climb_swep2", "tfa_rebirth_m7ds", "csgo_huntsman", "tfa_rebirth_m45e", "tfa_rebirth_m394b","tfa_rebirth_srs99c2s"},
   command = "bullfrog",
   max = 7,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Bullfrogs",
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply:SetArmor(200)
   ply.HaloArmor=65
   ply:SetMaxHealth(300)
end
})
TEAM_BULLFROGR = DarkRP.createJob("Bullfrog Rifleman", {
   color = Color(54, 47, 47, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[Bullfrog jumpers specialized in extreme tasks in which they can use air travel to reach their goal.]],
   weapons = {"h3_smg_tfa_odst","h3_odst_socom_tfa","tfa_rebirth_br55","tfa_h4_spartan_weapon_saw","tfa_rebirth_ma5d","csgo_huntsman", "Tfa_rebirth_m90", "climb_swep2", "revival_c4", "halo_frag", "tfa_rebirth_ma5d"},
   command = "bullfrogr",
   max = 3,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Bullfrogs",
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply:SetArmor(200)
   ply.HaloArmor=67
   ply:SetMaxHealth(300)
end
})
TEAM_BULLFROGMED = DarkRP.createJob("Bullfrog Corpsman", {
   color = Color(54, 47, 47, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[Bullfrog jumpers specialized in extreme tasks in which they can use air travel to reach their goal.]],
   weapons = {"h3_smg_tfa_odst","h3_odst_socom_tfa","tfa_rebirth_m90","tfa_h4_spartan_weapon_saw","tfa_rebirth_m394","revival_c4", "csgo_huntsman", "weapon_bactainjector", "darkrp_defibrillator", "Med_kit", "climb_swep2", "tfa_rebirth_ma5d", "halo_frag"},
   command = "bullfrogcorp",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Bullfrogs",
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply:SetArmor(200)
   ply.HaloArmor=66
   ply:SetMaxHealth(300)
end
})
TEAM_BULLFROGDEMO = DarkRP.createJob("Bullfrog Demolitionist", {
   color = Color(54, 47, 47, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[Bullfrog jumpers specialized in extreme tasks in which they can use air travel to reach their goal.]],
   weapons = {"h3_smg_tfa_odst","h3_odst_socom_tfa","tfa_rebirth_m394","csgo_huntsman", "tfa_h4_spartan_weapon_saw", "tfa_rebirth_m90", "tfa_rebirth_m41", "revival_c4", "halo_frag", "climb_swep2", "tfa_rebirth_ma5d", "tfa_m247_gpmg"},
   command = "bullfrogdemo",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Bullfrogs",
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply:SetArmor(200)
   ply.HaloArmor=67
   ply:SetMaxHealth(300)
end
})
TEAM_BULLFROGSNIPER = DarkRP.createJob("Bullfrog Marksman", {
   color = Color(54, 47, 47, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[Bullfrog jumpers specialized in extreme tasks in which they can use air travel to reach their goal.]],
   weapons = {"h3_smg_tfa_odst", "h3_odst_socom_tfa","tfa_rebirth_m90","tfa_rebirth_m394","tfa_h4_spartan_weapon_saw","revival_c4","csgo_huntsman", "tfa_rebirth_srs99c2s", "tfa_rebirth_br55", "climb_swep2", "tfa_rebirth_ma5d", "halo_frag"},
   command = "bullfrogsniper",
   max = 2,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Bullfrogs",
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply:SetArmor(200)
   ply.HaloArmor=66
   ply:SetMaxHealth(300)
end
})
TEAM_BULLFROGCO = DarkRP.createJob("Bullfrog Squad Leader", {
   color = Color(54, 47, 47, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[Bullfrog jumpers specialized in extreme tasks in which they can use air travel to reach their goal.]],
   weapons = {"tfa_rebirth_m90", "h3_smg_tfa_odst", "h3_odst_socom_tfa","hr_tfa_shotgun","tfa_h4_spartan_weapon_saw","tfa_rebirth_m394","revival_c4","climb_swep2","voice_amplifier", "weapon_rpw_binoculars", "csgo_huntsman", "tfa_rebirth_srs99c2s", "Tfa_rebirth_ma5d", "halo_frag", "tfa_rebirth_br55"},
   command = "bullfrogsquadleader",
   max = 2,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Bullfrogs",
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply:SetArmor(200)
   ply.HaloArmor=67
   ply:SetMaxHealth(300)
end
})
TEAM_TRIFLEMAN = DarkRP.createJob("Tombstone 2-4 Rifleman", {
   color = Color(109, 109, 109, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[Tombstone 2-4 is an elite division of volunteers many died in their drop, so only the best of the best is who make it.]],
   weapons = {"weapon_cuff_mp", "h3_smg_tfa_odst", "h3_odst_socom_tfa","hr_tfa_shotgun","tfa_rebirth_br55","tfa_rebirth_m394b","tfa_ishi_ma37", "tfa_rebirth_m90", "climb_swep2", "weapon_sh_doorcharge", "csgo_huntsman", "halo_frag", "weapon_sh_flashbang", "tfa_h5_br85n_h5"},
   command = "trifleman",
   max = 2,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Tombstone",
PlayerSpawn = function(ply)
   ply:SetHealth(350)
   ply:SetArmor(200)
   ply.HaloArmor=70
   ply:SetMaxHealth(350)
end
})
TEAM_TSHARPSHOOTER = DarkRP.createJob("Tombstone 2-4 Sharpshooter", {
   color = Color(109, 109, 109, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[Tombstone 2-4 is an elite division of volunteers many died in their drop, so only the best of the best is who make it.]],
   weapons = {"Weapon_cuff_mp", "h3_smg_tfa_odst","h3_odst_socom_tfa","tfa_rebirth_m90","hr_tfa_shotgun","tfa_rebirth_srs99s4am", "tfa_rebirth_m394b" , "climb_swep2", "weapon_sh_doorcharge", "csgo_huntsman", "halo_frag", "weapon_sh_flashbang", "tfa_h5_br85n_h5"},
   command = "tsharpshooter",
   max = 2,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Tombstone",
PlayerSpawn = function(ply)
   ply:SetHealth(350)
   ply:SetArmor(200)
   ply.HaloArmor=70
   ply:SetMaxHealth(350)
end
})
TEAM_TSUPPORT = DarkRP.createJob("Tombstone 2-4 Support", {
   color = Color(109, 109, 109, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[Tombstone 2-4 is an elite division of volunteers many died in their drop, so only the best of the best is who make it.]],
   weapons = {"Weapon_cuff_mp", "h3_smg_tfa_odst", "h3_odst_socom_tfa","tfa_rebirth_m90","hr_tfa_shotgun","tfa_rebirth_br55","tfa_h4_spartan_weapon_saw","climb_swep2", "tfa_rebirth_m41", "weapon_sh_doorcharge", "csgo_huntsman", "tfa_rebirth_m45e", "halo_frag", "weapon_sh_flashbang", "tfa_h5_br85n_h5", "tfa_m247_gpmg", "revival_c4"},
   command = "tsupport",
   max = 2,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Tombstone",
PlayerSpawn = function(ply)
   ply:SetHealth(350)
   ply:SetArmor(200)
   ply.HaloArmor=70
   ply:SetMaxHealth(350)
end
})
TEAM_TSQUADLEAD = DarkRP.createJob("Tombstone 2-4 Squad Leader", {
   color = Color(109, 109, 109, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[Tombstone 2-4 is an elite division of volunteers many died in their drop, so only the best of the best is who make it.]],
   weapons = {"Tfa_rebirth_ma37s", "h3_smg_tfa_odst", "h3_odst_socom_tfa","tfa_rebirth_m90","hr_tfa_shotgun","tfa_rebirth_br55","Weapon_cuff_mp", "stungun", "tfa_rebirth_m394b" ,"voice_amplifier", "climb_swep2", "weapon_sh_doorcharge", "weapon_rpw_binoculars", "csgo_huntsman", "tfa_rebirth_m45e", "halo_frag", "weapon_sh_flashbang"},
   command = "tsquadleader",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Tombstone",
PlayerSpawn = function(ply)
   ply:SetHealth(350)
   ply:SetArmor(200)
   ply.HaloArmor=70
   ply:SetMaxHealth(350)
end
})
TEAM_TMEDIC = DarkRP.createJob("Tombstone 2-4 Corpsman", {
   color = Color(109, 109, 109, 255),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[Tombstone 2-4 is an elite division of volunteers many died in their drop, so only the best of the best is who make it.]],
   weapons = {"Weapon_cuff_mp", "h3_smg_tfa_odst", "h3_odst_socom_tfa","tfa_rebirth_m90","hr_tfa_shotgun","tfa_rebirth_m394b", "climb_swep2","darkrp_defibrillator","med_kit", "csgo_huntsman", "weapon_bactainjector", "halo_frag", "weapon_sh_flashbang", "tfa_h5_br85n_h5"},
   command = "tcorpsman",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Tombstone",
PlayerSpawn = function(ply)
   ply:SetHealth(350)
   ply:SetArmor(200)
   ply.HaloArmor=70
   ply:SetMaxHealth(350)
end
})
TEAM_MILITARYPOLICERECRUIT = DarkRP.createJob("Military Police Recruit", {
   color = Color(68, 0, 255, 255),
   model = {"models/ishi/halo_rebirth/player/nmpd/male/nmpd_eyecberg.mdl", "models/ishi/halo_rebirth/player/nmpd/female/nmpd_miia.mdl"},
   description = [[Military Police were a set of individuals who pledged to keep order in the corps. You are the Recruit you are going to be trained and trusted with the power of the MP.]],
   weapons = {"arrest_stick","unarrest_stick","tfa_rebirth_m7ds","climb_swep2","tfa_rebirth_m6cp","Stunstick","Weapon_cuff_mp","tfa_rebirth_xbr55s"},
   command = "militarypolicerecruit",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Military Police",
PlayerSpawn = function(ply)
   ply:SetHealth(200)
   ply.HaloArmor=65
   ply:SetMaxHealth(200)
   ply:SetRunSpeed(275)
   ply:SetWalkSpeed(175)
end
})
TEAM_MILITARYPOLICE = DarkRP.createJob("Military Police", {
   color = Color(68, 0, 255, 255),
   model = {"models/ishi/halo_rebirth/player/nmpd/male/nmpd_eyecberg.mdl", "models/ishi/halo_rebirth/player/nmpd/female/nmpd_miia.mdl"},
   description = [[Military Police were a set of individuals who pledged to keep order in the corps]],
   weapons = {"arrest_stick","unarrest_stick","tfa_rebirth_m7ds","climb_swep2","stungun","tfa_rebirth_xbr55s","tfa_rebirth_m90","tfa_rebirth_m6cp","Stunstick","Weapon_cuff_mp"},
   command = "militarypolice",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Military Police",
PlayerSpawn = function(ply)
   ply:SetHealth(225)
   ply.HaloArmor=65
   ply:SetMaxHealth(225)
   ply:SetRunSpeed(275)
   ply:SetWalkSpeed(175)
end
})
TEAM_MILITARYPOLICESGT = DarkRP.createJob("Military Police Sergeant", {
   color = Color(68, 0, 255, 255),
   model = {"models/ishi/halo_rebirth/player/nmpd/male/nmpd_eyecberg.mdl", "models/ishi/halo_rebirth/player/nmpd/female/nmpd_miia.mdl"},
   description = [[Military Police were a set of individuals who pledged to keep order in the corps.]],
   weapons = {"arrest_stick","unarrest_stick","tfa_rebirth_m7ds","climb_swep2","stungun","Voice_amplifier","Weapon_rpw_binoculars","tfa_rebirth_xbr55s","tfa_rebirth_m90","tfa_rebirth_m6cp","Stunstick","Weapon_sh_doorcharge","weapon_sh_flashbang","Weapon_cuff_mp"},
   command = "militarypolicesgt",
   max = 6,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Military Police",
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply.HaloArmor=65
   ply:SetMaxHealth(250)
   ply:SetRunSpeed(275)
   ply:SetWalkSpeed(175)
end
})
TEAM_MILITARYPOLICERIOT = DarkRP.createJob("Military Police Riot", {
   color = Color(68, 0, 255, 255),
   model = {"models/ishi/halo_rebirth/player/nmpd/male/nmpd_eyecberg.mdl", "models/ishi/halo_rebirth/player/nmpd/female/nmpd_miia.mdl"},
   description = [[Military Police were a set of individuals who pledged to keep order in the corps. You are an MP Riot with access to Riot Shields.]],
   weapons = {"arrest_stick","unarrest_stick","tfa_rebirth_m7ds","climb_swep2","weapon_policeshield","stungun","voice_amplifier","weapon_rpw_binoculars","tfa_rebirth_xbr55s","tfa_rebirth_m90","tfa_rebirth_m6cp","Stunstick","Weapon_sh_doorcharge","weapon_sh_flashbang","Weapon_cuff_mp"},
   command = "militarypoliceriot",
   max = 6,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Military Police",
PlayerSpawn = function(ply)
   ply:SetHealth(275)
   ply.HaloArmor=70
   ply:SetMaxHealth(275)
   ply:SetRunSpeed(275)
   ply:SetWalkSpeed(175)
end
})
TEAM_MILITARYPOLICERIOTLEADER = DarkRP.createJob("Military Police Chief", {
   color = Color(68, 0, 255, 255),
   model = {"models/ishi/halo_rebirth/player/nmpd/male/nmpd_eyecberg.mdl", "models/ishi/halo_rebirth/player/nmpd/female/nmpd_miia.mdl"},
   description = [[Military Police were a set of individuals who pledged to keep order in the corps. You are an MP Leader.]],
   weapons = {"arrest_stick","unarrest_stick","tfa_rebirth_m7ds","climb_swep2","Weapon_policeshield","Stungun","Voice_amplifier","Weapon_rpw_binoculars","tfa_rebirth_xbr55s","tfa_rebirth_m90","tfa_rebirth_m6cp","Stunstick","Weapon_sh_doorcharge","weapon_sh_flashbang","Weapon_cuff_mp"},
   command = "militarypoliceleader",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Military Police",
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply.HaloArmor=65
   ply:SetMaxHealth(300)
   ply:SetRunSpeed(275)
   ply:SetWalkSpeed(175)
end
})
TEAM_NAVALRECRUIT = DarkRP.createJob("Naval Recruit", {
   color = Color(0, 0, 0, 255),
   model = {"models/player/scifi_bill.mdl","models/player/scifi_fang.mdl"},
   description = [[You are new crew member and you must learn about Naval.]],
   weapons = {"tfa_rebirth_m6c"},
   command = "navalrecruit",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "UNSC Central Fleet",
})
TEAM_NAVALSOLDIEROFFICER = DarkRP.createJob("Naval Field Officer", {
   color = Color(0, 0, 0, 255),
   model = {"models/gonzo/deviliousmarines/dragon/dragonm.mdl"},
   description = [[You are an Infantry officer for naval.]],
   weapons = {"tfa_rebirth_xbr55s", "csgo_huntsman", "tfa_rebirth_m45e", "tfa_rebirth_srs99s2am", "tfa_rebirth_m7ds"},
   command = "navalsoldierofficer",
   max = 5,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "UNSC Central Fleet",
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply:SetArmor(150)
   ply:SetMaxHealth(250)
end
})
TEAM_NAVALFOOTSOLDIER = DarkRP.createJob("Naval Infantry", {
   color = Color(0, 0, 0, 255),
   model = {"models/gonzo/deviliousmarines/bull/bullm.mdl"},
   description = [[You are Infantry for naval.]],
   weapons = {"tfa_rebirth_ma5b", "climb_swep2", "csgo_huntsman", "tfa_rebirth_m6c", "tfa_rebirth_m7ds"},
   command = "navalinfantry",
   max = 20,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "UNSC Central Fleet",
PlayerSpawn = function(ply)
   ply:SetHealth(225)
   ply:SetArmor(100)
   ply:SetMaxHealth(225)
end
})
TEAM_NAVALADMIRAL = DarkRP.createJob("Naval Admiral", {
   color = Color(0, 0, 0, 255),
   model = {"models/gonzo/unscofficers/cutter/cutter.mdl"},
    description = [[You are to command on the bridge and ships.You are to give orders to all units and watch over units.]],
   weapons = {"tfa_rebirth_m6g","weapon_cuff_mp", "voice_amplifier"},
   command = "navaladmiral",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "UNSC Central Fleet",
PlayerSpawn = function (ply)
   ply:SetHealth(200)
   ply:SetMaxHealth(200)
 end
})
TEAM_ONISPECIALIST = DarkRP.createJob("ONI S.T.R.I.K.E. Team", {
   color = Color(0, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl"},
   description = [[ONI were a secretive organization they-NULL DATA]],
   weapons = {"tfa_rebirth_srs99c2s", "tfa_rebirth_m6cs","tfa_rebirth_ma37s", "tfa_rebirth_m90", "weapon_cuff_mp", "bkeycardscanner_procracker", "climb_swep2", "csgo_huntsman", "Tfa_rebirth_m7ds","Tfa_h5_br85n_h5","tfa_rebirth_m6cs", "halo_frag","weapon_sh_flashbang","weapon_sh_doorcharge","revival_c4","tfa_rebirth_m41","weapon_policeshield","invisibility_cloak", "stungun"},
   command = "onispecialist",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "ONI Security Force", 
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply:SetArmor(300)
   ply.HaloArmor=65
   ply:SetMaxHealth(400)
   ply:SetRunSpeed(270)
   ply:SetWalkSpeed(170)
   ply:GiveAmmo(5, "grenade")
   ply:GiveAmmo(5, "grenade")
end
})
TEAM_ONISZERO = DarkRP.createJob("ONI Section 0", {
   color = Color(160, 160, 160, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_danlin.mdl","models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl"},
   description = [[ONI were a secretive organization they-NULL DATA]],
   weapons = {"tfa_rebirth_m6cs","halo_forerunner_weapon_binary","tfa_rebirth_srs99c2b","Tfa_rebirth_m7ds", "climb_swep2", "tfa_rebirth_m45e", "weapon_cuff_admin", "csgo_huntsman", "tfa_hr_swep_spartan_laser", "Tfa_h5_br85n_h5","invisibility_cloak","tfa_rebirth_m6cs","tfa_rebirth_srs99s4am","deika_plasmarifle_tfa"},
   command = "onisectionzero",
   max = 5,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "ONI",
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply:SetArmor(300)
   ply.HaloArmor=65
   ply:SetMaxHealth(400)
end
})
TEAM_ONISTHREE = DarkRP.createJob("ONI Section 3", {
   color = Color(160, 160, 160, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_danlin.mdl","models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl"},
   description = [[ONI were a secretive organization they-NULL DATA]],
   weapons = {"tfa_rebirth_m6cs","weapon_bactainjector","bw2_brifle","tfa_rebirth_m6cs", "halorepair_tool", "weapon_bactanade", "med_kit", "darkrp_defibrillator", "Tfa_h5_br85n_h5", "climb_swep2", "csgo_huntsman", "tfa_rebirth_m45e", "weapon_cuff_admin", "Tfa_rebirth_m7ds", "invisibility_cloak"},
   command = "onisthree",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "ONI",
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply:SetArmor(200)
   ply.HaloArmor=60
   ply:SetMaxHealth(400)
end
})
TEAM_ONISTWO = DarkRP.createJob("ONI Section 2", {
   color = Color(160, 160, 160, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_danlin.mdl","models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl"},
   description = [[ONI were a secretive organization they-NULL DATA]],
   weapons = {"stunstick","tfa_rebirth_m6cs","gmod_camera","voice_amplifier","tfa_rebirth_m6cs","climb_swep2", "csgo_huntsman", "Tfa_rebirth_m7ds", "Tfa_h5_br85n_h5","tfa_rebirth_m45e", "weapon_cuff_admin", "invisibility_cloak"},
   command = "onistwo",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "ONI",
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply:SetArmor(200)
   ply.HaloArmor=60
   ply:SetMaxHealth(400)
end
})
TEAM_ONISONE = DarkRP.createJob("ONI Section 1", {
   color = Color(160, 160, 160, 255),
   model = {"tfa_rebirth_m41","weapon_sh_flashbang","halo_frag","models/ishi/halo_rebirth/player/odst/male/odst_danlin.mdl", "models/ishi/halo_rebirth/player/odst/female/odst_ltd.mdl"},
   description = [[ONI were a secretive organization they-NULL DATA]],
   weapons = {"tfa_rebirth_m41","weapon_sh_flashbang","halo_frag","tfa_rebirth_m6cs","tfa_rebirth_srs99c2b","tfa_rebirth_m45e", "climb_swep2", "invisibility_cloak", "csgo_huntsman", "Tfa_rebirth_m7ds", "Tfa_h5_br85n_h5", "tfa_rebirth_m6cs", "weapon_cuff_admin"},
   command = "onisone",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "ONI",
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply:SetArmor(200)
   ply.HaloArmor=60
   ply:SetMaxHealth(400)
end
})
TEAM_ONISZEROH = DarkRP.createJob("ONI Section 0 Head", {
   color = Color(160, 160, 160, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl"},
   description = [[ONI were a secretive organization they-NULL DATA]],
   weapons = {"tfa_rebirth_m6cs","halo_forerunner_weapon_binary","tfa_rebirth_srs99c2b", "climb_swep2", "tfa_rebirth_m45e", "weapon_cuff_admin", "tfa_hr_swep_spartan_laser","Tfa_rebirth_m7ds","Tfa_h5_br85n_h5","tfa_rebirth_m6cs","csgo_huntsman","invisibility_cloak","tfa_rebirth_srs99s4am","deika_plasmarifle_tfa"},
   command = "onisectionzerohead",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "ONI",
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply:SetArmor(300)
   ply.HaloArmor=65
   ply:SetMaxHealth(400)
end
})
TEAM_ONISONEH = DarkRP.createJob("ONI Section 1 Head", {
   color = Color(160, 160, 160, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_danlin.mdl"},
   description = [[ONI were a secretive organization they-NULL DATA]],
   weapons = {"tfa_rebirth_m41","weapon_sh_flashbang","halo_frag","tfa_rebirth_m6cs","tfa_rebirth_srs99c2b", "tfa_rebirth_m6cs", "climb_swep2", "tfa_rebirth_m45e", "invisibility_cloak", "weapon_cuff_admin", "csgo_huntsman", "tfa_h4_spartan_weapon_saw", "Tfa_rebirth_m7ds", "Tfa_h5_br85n_h5"},
   command = "onisectiononehead",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "ONI",
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply:SetArmor(300)
   ply.HaloArmor=60
   ply:SetMaxHealth(400)
end
})
TEAM_ONISTWOH = DarkRP.createJob("ONI Section 2 Head", {
   color = Color(160, 160, 160, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_danlin.mdl"},
   description = [[ONI were a secretive organization they-NULL DATA]],
   weapons = {"gmod_camera","stunstick","stungun","tfa_rebirth_m6cs","voice_amplifier","Tfa_h5_br85n_h5", "tfa_rebirth_m6cs", "Tfa_rebirth_m7ds", "climb_swep2", "tfa_rebirth_m45e", "weapon_cuff_admin", "csgo_huntsman", "invisibility_cloak"},
   command = "onisectiontwohead",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "ONI",
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply:SetArmor(300)
   ply.HaloArmor=60
   ply:SetMaxHealth(400)
end
})
TEAM_ONISTHREEH = DarkRP.createJob("ONI Section 3 Head", {
   color = Color(160, 160, 160, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_danlin.mdl"},
   description = [[ONI were a secretive organization they-NULL DATA]],
   weapons = {"weapon_bactainjector","tfa_rebirth_m7ds", "bw2_brifle", "Tfa_h5_br85n_h5", "climb_swep2", "tfa_rebirth_m45e", "halorepair_tool", "weapon_bactanade", "med_kit", "darkrp_defibrillator", "weapon_cuff_admin","tfa_rebirth_m6cs","csgo_huntsman","invisibility_cloak"},
   command = "onisectionthreehead",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "ONI",
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply:SetArmor(300)
   ply.HaloArmor=60
   ply:SetMaxHealth(400)
end
})
TEAM_ONISPN = DarkRP.createJob("ONI Spartan", {
   color = Color(32, 32, 32, 255),
   model = {"models/valk/haloreach/unsc/spartan/spartan.mdl","models/suno/player/zeus/female/fang.mdl", "models/suno/player/zeus/male/formal_bill.mdl"},
   description = [[DATA EXPUNGED]],
   weapons = {"Tfa_h5_br85n_h5","h3_esword_tfa","tfa_rebirth_srs99s4am","halo_forerunner_weapon_binary","halo3_turret","weapon_bactainjector","tfa_hr_swep_magnum","weapon_bactanade","weapon_sh_doorcharge","tfa_hr_swep_assault_rifle","tfa_h5_br85n_h5","climb_swep2","weapon_fistsofreprisal","tfa_hr_swep_shotgun","tfa_rebirth_m7ds","tfa_rebirth_m41","stungun","weapon_cuff_mp","halo_frag","invisibility_cloak","tfa_hr_swep_spartan_laser"},
   command = "onispartan",
   max = 5,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "ONI",
   ammo = {["item_ammo_smg1_grenade"] = 50},
PlayerSpawn = function(ply)
   ply:SetHealth(800)
   ply:SetArmor(600)
   ply.HaloArmor=75
   ply:SetMaxHealth(800)
   ply:SetRunSpeed(325)
   ply:SetWalkSpeed(225)
   ply:SetJumpPower(300)
end
})
TEAM_ONISPCOMMAND = DarkRP.createJob("Spartan Commander", {
   color = Color(0, 0, 0, 0),
   model = {"models/valk/haloreach/unsc/spartan/spartan.mdl","models/suno/player/zeus/female/female_02.mdl","models/suno/player/zeus/male/formal_bill.mdl"},
   description = [[DATA EXPUNGED]],
   weapons = {"weapon_bactainjector","weapon_bactanade","tfa_rebirth_srs99s4am","weapon_sh_doorcharge","tfa_hr_swep_assault_rifle","tfa_hr_swep_dmr","tfa_hr_swep_magnum","climb_swep2","weapon_fistsofreprisal","tfa_hr_swep_shotgun","tfa_rebirth_m7ds","tfa_rebirth_m41","stungun","weapon_cuff_mp","halo_frag","invisibility_cloak","tfa_hr_swep_spartan_laser"},
   command = "spartancommander",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "ONI",
   ammo = {["item_ammo_smg1_grenade"] = 50},
PlayerSpawn = function(ply)
   ply:SetHealth(800)
   ply:SetArmor(600)
   ply.HaloArmor=75
   ply:SetMaxHealth(800)
   ply:SetRunSpeed(325)
   ply:SetWalkSpeed(225)
   ply:SetJumpPower(300)
end
})
TEAM_ONIDIR = DarkRP.createJob("ONI Director", {
   color = Color(32, 32, 32, 255),
   model = {"models/valk/haloreach/unsc/spartan/spartan.mdl","models/suno/player/zeus/female/fang.mdl","models/suno/player/zeus/male/formal_male_09.mdl"},
   description = [[DATA EXPUNGED]],
   weapons = {"halo3_turret","weapon_bactainjector","weapon_bactanade","Tfa_rebirth_m7ds","weapon_sh_doorcharge","tfa_hr_swep_assault_rifle","Tfa_h5_br85n_h5","tfa_rebirth_m6cs","climb_swep2","weapon_fistsofreprisal","tfa_hr_swep_shotgun","tfa_rebirth_m41","stungun","weapon_cuff_mp","halo_frag","invisibility_cloak","tfa_hr_swep_spartan_laser", "tfa_rebirth_srs99s4am","tfa_hr_swep_magnum"},
   command = "onidir",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "ONI",
   ammo = {["item_ammo_smg1_grenade"] = 50},
PlayerSpawn = function(ply)
   ply:SetHealth(800)
   ply:SetArmor(700)
   ply.HaloArmor=80
   ply:SetMaxHealth(800)
   ply:SetRunSpeed(325)
   ply:SetWalkSpeed(225)
   ply:SetJumpPower(300)
end
})
TEAM_ONIASSISTDIR = DarkRP.createJob("ONI Assistant Director", {
   color = Color(32, 32, 32, 255),
   model = {"models/valk/haloreach/unsc/spartan/spartan.mdl","models/suno/player/zeus/female/fang.mdl","models/suno/player/zeus/male/formal_louis.mdl"},
   description = [[DATA EXPUNGED]],
   weapons = {"Tfa_h5_br85n_h5","tfa_rebirth_srs99s4am","halo_forerunner_weapon_binary","halo3_turret","weapon_bactainjector","tfa_hr_swep_magnum","weapon_bactanade","bkeycardscanner_procracker","tfa_hr_swep_assault_rifle","tfa_h5_br85n_h5","climb_swep2","weapon_fistsofreprisal","tfa_hr_swep_shotgun","tfa_rebirth_m7ds","tfa_rebirth_m41","stungun","weapon_cuff_mp","halo_frag","invisibility_cloak","tfa_hr_swep_spartan_laser"},
   command = "oniassistdir",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "ONI",
   ammo = {["item_ammo_smg1_grenade"] = 50},
PlayerSpawn = function(ply)
   ply:SetHealth(800)
   ply:SetArmor(700)
   ply.HaloArmor=80
   ply:SetMaxHealth(800)
   ply:SetRunSpeed(325)
   ply:SetWalkSpeed(225)
   ply:SetJumpPower(300)
end
})
TEAM_ONIAGENT = DarkRP.createJob("ONI Agent", {
   color = Color(160, 160, 160, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/suno/player/zeus/male/formal_male_07.mdl","models/suno/player/zeus/female/fang.mdl"},
   description = [[ONI were a secretive organization they-NULL DATA]],
   weapons = {"tfa_rebirth_m6cs", "weapon_cuff_mp","climb_swep2", "csgo_huntsman","tfa_rebirth_m392","Tfa_rebirth_m7ds","tfa_rebirth_m6cs"},
   command = "oniagent",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "ONI",
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply:SetArmor(200)
   ply.HaloArmor=60
   ply:SetMaxHealth(400)
end
})
TEAM_ONIBATTLEBOTBASIC = DarkRP.createJob("ONI Battlebot", {  
   color = Color(10, 10, 10, 255),
   model = {"models/dizcordum/reaper.mdl"},
   description = [[ONI were a secretive organization they-NULL DATA]],
   weapons = {"climb_swep2","tfa_rec_hmini", "tfa_rebirth_m394", "tfa_rebirth_m7ds","tfa_h5_br85n_h5","tfa_rebirth_ma37","tfa_rebirth_m90","med_kit","halo_flamethrower"},
   command = "onibattlebotbasic",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "ONI",
PlayerSpawn = function(ply)
   ply:SetHealth(350)
   ply:SetArmor(300)
   ply.HaloArmor=60
   ply:SetMaxHealth(350)
end
})
TEAM_SPARTANTRAINEE = DarkRP.createJob("Spartan Trainee", {
   color = Color(160, 160, 160, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl","models/suno/player/zeus/female/fang.mdl","models/suno/player/zeus/male/formal_bill.mdl"},
   description = [[ONI were a secretive organization they-NULL DATA]],
   weapons = {"invisibility_cloak","tfa_hr_swep_assault_rifle","tfa_hr_swep_magnum","climb_swep2"},
   command = "spartantrainee",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "ONI",
   ammo = {["item_ammo_smg1_grenade"] = 50},
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply:SetArmor(400)
   ply.HaloArmor=60
   ply:SetMaxHealth(400)
end
})
TEAM_EDENSQUADLEADER = DarkRP.createJob("Eden Fireteam Leader", {
   color = Color(199, 175, 31, 255),
   model = {"models/valk/haloreach/UNSC/Spartan/Spartan.mdl","models/suno/player/zeus/female/fang.mdl","models/suno/player/zeus/male/formal_bill.mdl"},
   description = [[NULL DATA]],
   weapons = {"tfa_rebirth_srs99s4am","weapon_sh_doorcharge","tfa_hr_swep_assault_rifle","Tfa_h5_br85n_h5","tfa_hr_swep_magnum","climb_swep2","weapon_fistsofreprisal","tfa_hr_swep_shotgun","tfa_rebirth_m7ds","stungun","weapon_cuff_mp","invisibility_cloak","halo_frag","weapon_sh_flashbang","tfa_hr_swep_spartan_laser"},
   command = "edensquadleader",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Eden",
   ammo = {["item_ammo_smg1_grenade"] = 50},
PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply.HaloArmor=70
   ply:SetArmor(600)
   ply:SetMaxHealth(600)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(210)
   ply:SetJumpPower(280)
end
})
TEAM_EDENRIFLEMAN = DarkRP.createJob("Eden Rifleman", {
   color = Color(199, 175, 31, 255),
   model = {"models/valk/haloreach/UNSC/Spartan/Spartan.mdl","models/suno/player/zeus/female/fang.mdl","models/suno/player/zeus/male/formal_bill.mdl"},
   description = [[NULL DATA]],
   weapons = {"weapon_policeshield","tfa_hr_swep_shotgun","tfa_hr_swep_assault_rifle","Tfa_h5_br85n_h5","tfa_hr_swep_magnum","climb_swep2","weapon_fistsofreprisal","weapon_bactainjector","weapon_bactanade","med_kit","darkrp_defibrillator","tfa_rebirth_m7ds","invisibility_cloak","stungun","weapon_cuff_mp","halo_frag","weapon_sh_flashbang","weapon_sh_doorcharge"},
   command = "edenrifleman",
   max = 2,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Eden",
   ammo = {["item_ammo_smg1_grenade"] = 50},
PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply.HaloArmor=70
   ply:SetArmor(600)
   ply:SetMaxHealth(600)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(210)
   ply:SetJumpPower(280)
end
})
TEAM_EDENSUPPORT = DarkRP.createJob("Eden Support", {
   color = Color(199, 175, 31, 255),
   model = {"models/valk/haloreach/UNSC/Spartan/Spartan.mdl","models/suno/player/zeus/female/fang.mdl","models/suno/player/zeus/male/formal_bill.mdl"},
   description = [[NULL DATA]],
   weapons = {"tfa_rec_hmini","tfa_rebirth_m41","tfa_hr_swep_assault_rifle","tfa_hr_swep_magnum","climb_swep2","weapon_fistsofreprisal","tfa_hr_swep_shotgun","tfa_rebirth_m7ds","invisibility_cloak","revival_c4","stungun","weapon_cuff_mp","halo_frag","weapon_sh_flashbang","weapon_sh_doorcharge"},
   command = "edensupport",
   max = 2,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Eden",
   ammo = {["item_ammo_smg1_grenade"] = 50},
PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply.HaloArmor=70
   ply:SetArmor(600)
   ply:SetMaxHealth(600)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(210)
   ply:SetJumpPower(280)
end
})
TEAM_EDENSHARPSHOOTER = DarkRP.createJob("Eden Sharpshooter", {
   color = Color(199, 175, 31, 255),
   model = {"models/valk/haloreach/UNSC/Spartan/Spartan.mdl","models/suno/player/zeus/female/fang.mdl", "models/suno/player/zeus/female/hawke.mdl"},
   description = [[NULL DATA]],
   weapons = {"tfa_hr_swep_shotgun","tfa_rebirth_srs99s4am","tfa_hr_swep_assault_rifle","tfa_hr_swep_magnum","climb_swep2","weapon_fistsofreprisal","tfa_rebirth_m7ds","invisibility_cloak","stungun","weapon_cuff_mp","halo_frag","weapon_sh_flashbang","weapon_sh_doorcharge"},
   command = "edensharpshooter",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Eden",
   ammo = {["item_ammo_smg1_grenade"] = 50},
PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply.HaloArmor=70
   ply:SetArmor(600)
   ply:SetMaxHealth(600)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(210)
   ply:SetJumpPower(280)
end
})
TEAM_AI = DarkRP.createJob("AI", {
   color = Color(51, 51, 255, 255),
   model = {"models/player/scifi_bill.mdl", "models/player/scifi_female_01.mdl", "models/player/scifi_wraith.mdl","models/player/scifi_zoey.mdl"},
   description = [[DATA EXPUNGED]],
   weapons = {"med_kit"},
   command = "ai",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "AI",
})
TEAM_SOSINFANTRY = DarkRP.createJob("SoS Elite", {
   color = Color(127, 0, 255, 255),
   model = {"models/player/killer_instinct/red/retro_arbiter_h2a_elite_red.mdl"},
   description = [[The Swords of Sanghelios are a fractured group of the Covenant that have joined in alliance with the UNSC to stop the Great Journey]],
   weapons = {"tfa_hr_swep_needle_rifle", "weapon_plasmanade", "climb_swep2","tfa_hr_swep_plasma_rifle"},
   command = "sosinfantry",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Swords of Sanghelios",
PlayerSpawn = function(ply)
   ply:SetHealth(500)
   ply:SetArmor(350)
   ply.HaloArmor=86
   ply:SetMaxHealth(500)
   ply:SetRunSpeed(270)
   ply:SetWalkSpeed(170)
end
})
TEAM_SPECOPS = DarkRP.createJob("SoS Spec-Ops", {
   color = Color(127, 0, 255, 255),
   model = {"models/player/specops_armour.mdl"},
   description = [[The Swords of Sanghelios are a fractured group of the Covenant that have joined in alliance with the USNC to stop the Great Journey]],
   weapons = {"tfa_h2_covenant_carbine","tfa_hr_swep_needle_rifle", "weapon_plasmanade", "invisibility_cloak", "climb_swep2", "h3_esword_tfa","tfa_hr_swep_plasma_rifle"},
   command = "sosspecops",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Swords of Sanghelios",
PlayerSpawn = function(ply)
   ply:SetHealth(450)
   ply:SetArmor(400)
   ply.HaloArmor=86
   ply:SetMaxHealth(450)
   ply:SetRunSpeed(270)
   ply:SetWalkSpeed(170)
end
})
TEAM_RANGER = DarkRP.createJob("SoS Ranger", {
   color = Color(127, 0, 255, 255),
   model = {"models/player/ranger_armour.mdl"},
   description = [[The Swords of Sanghelios are a fractured group of the Covenant that have joined in alliance with the USNC to stop the Great Journey]],
   weapons = {"tfa_hr_swep_needle_rifle", "weapon_plasmanade", "h3_beam_rifle_tfa", "climb_swep2","tfa_hr_swep_plasma_rifle"},
   command = "sosranger",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Swords of Sanghelios",
PlayerSpawn = function(ply)
   ply:SetHealth(450)
   ply:SetArmor(400)
   ply.HaloArmor=76
   ply:SetMaxHealth(450)
   ply:SetRunSpeed(270)
   ply:SetWalkSpeed(170)
end
})
TEAM_SOSOFFICER = DarkRP.createJob("SoS Officer", {
   color = Color(127, 0, 255, 255),
   model = {"models/player/ultra_armour.mdl","models/player/officer_armour.mdl"},
   description = [[The Swords of Sanghelios are a fractured group of the Covenant that have joined in alliance with the USNC to stop the Great Journey]],
   weapons = {"tfa_hr_swep_plasma_rifle","weapon_plasmanade", "invisibility_cloak", "climb_swep2","halo3_dualplasmarifle","tfa_h2_covenant_carbine","h3_esword_tfa"},
   command = "sosofficer",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Swords of Sanghelios",
PlayerSpawn = function(ply)
   ply:SetHealth(450)
   ply:SetArmor(450)
   ply.HaloArmor=76
   ply:SetMaxHealth(450)
   ply:SetRunSpeed(270)
   ply:SetWalkSpeed(170)
end
})
TEAM_SOSLOWCOMMAND = DarkRP.createJob("SoS Low Command", {
   color = Color(127, 0, 255, 255),
   model = {"models/killer_instinct/characters/marshall/marshall.mdl", "models/killer_instinct/characters/commander/commander_var3.mdl"},
   description = [[The Swords of Sanghelios are a fractured group of the Covenant that have joined in alliance with the USNC to stop the Great Journey]],
   weapons = {"tfa_hr_swep_plasma_rifle","halo3_sword","weapon_plasmanade", "h3_esword_tfa", "invisibility_cloak", "tfa_hr_swep_concussion_rifle", "climb_swep2","halo3_dualplasmarifle","tfa_h2_covenant_carbine"},
   command = "soslowcommand",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Swords of Sanghelios",
PlayerSpawn = function(ply)
   ply:SetHealth(500)
   ply:SetArmor(500)
   ply.HaloArmor=76
   ply:SetMaxHealth(500)
   ply:SetRunSpeed(285)
   ply:SetWalkSpeed(185)
end
})
TEAM_HONORGUARD = DarkRP.createJob("SoS Honor Guard", {
   color = Color(127, 0, 255, 255),
   model = {"models/player/killer_instinct/honorelite/honorelite.mdl", "models/player/killer_instinct/honorelite/white/honorelite_white.mdl"},
   description = [[The Swords of Sanghelios are a fractured group of the Covenant that have joined in alliance with the USNC to stop the Great Journey]],
   weapons = {"tfa_hr_swep_plasma_rifle","tfa_hr_swep_needler","invisibility_cloak","halo3_sword","weapon_bactanade","tfa_hr_swep_needle_rifle","h3_esword_tfa","weapon_plasmanade","tfa_h2_covenant_carbine","climb_swep2","halo3_dualplasmarifle","weapon_cuff_mp","h3_beam_rifle_tfa"},
   command = "soshonorguard",
   max = 6,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Swords of Sanghelios",
PlayerSpawn = function(ply)
   ply:SetHealth(500)
   ply:SetArmor(500)
   ply.HaloArmor=76
   ply:SetMaxHealth(500)
   ply:SetRunSpeed(270)
   ply:SetWalkSpeed(170)
end
})
TEAM_SOSHELIOS = DarkRP.createJob("SoS Helios", {
   color = Color(127, 0, 255, 255),
   model = {"models/player/killer_instinct/honorelite/honorelite.mdl"},
   description = [[The Swords of Sanghelios are a fractured group of the Covenant that have joined in alliance with the UNSC to stop the Great Journey]],
   weapons = {"tfa_hr_swep_plasma_rifle","tfa_h2_covenant_carbine","tfa_hr_swep_needler","halo3_sword","invisibility_cloak","h3_beam_rifle_tfa","weapon_bactanade","climb_swep2","keys","h3_beam_rifle_tfa ","halo3_dualplasmapistol","halo3_dualplasmarifle","h3_esword_tfa","tfa_hr_swep_concussion_rifle","halo3_fuelrodcannon","weapon_cuff_mp","weapon_plasmanade","bkeycard"},
   command = "soshelios",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Swords of Sanghelios",
PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply:SetArmor(500)
   ply.HaloArmor=76
   ply:SetMaxHealth(600)
   ply:SetRunSpeed(285)
   ply:SetWalkSpeed(185)
end
})
TEAM_HIGHCOMMAND = DarkRP.createJob("SoS High Command", {
   color = Color(127, 0, 255, 255),
   model = {"models/killer_instinct/characters/shipmaster/shipmaster_var3.mdl", "models/killer_instinct/characters/shipmaster/shipmaster_var4.mdl"},
   description = [[The Swords of Sanghelios are a fractured group of the Covenant that have joined in alliance with the USNC to stop the Great Journey]],
   weapons = {"tfa_hr_swep_plasma_rifle","halo3_sword","tfa_hr_swep_needle_rifle", "tfa_hr_swep_needler", "weapon_plasmanade", "h3_beam_rifle_tfa", "invisibility_cloak", "tfa_hr_swep_concussion_rifle", "climb_swep2", "h3_esword_tfa","halo3_dualplasmarifle","halo3_fuelrodcannon","halo3_dualplasmapistol","tfa_h2_covenant_carbine","weapon_cuff_mp"},
   command = "soshighcommand",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Swords of Sanghelios",
PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply:SetArmor(500)
   ply.HaloArmor=76
   ply:SetMaxHealth(600)
   ply:SetRunSpeed(285)
   ply:SetWalkSpeed(185)
end
})
TEAM_SENIORCOMMAND = DarkRP.createJob("SoS Senior Command", {
   color = Color(127, 0, 255, 255),
   model = {"models/player/killer_instinct/gold/retro_arbiter_h2a_elite_gold.mdl", "models/player/killer_instinct/concilelite/concilelite.mdl"},
   description = [[The Swords of Sanghelios are a fractured group of the Covenant that have joined in alliance with the USNC to stop the Great Journey]],
   weapons = {"tfa_hr_swep_plasma_rifle","halo3_sword","tfa_hr_swep_needle_rifle", "tfa_hr_swep_needler", "weapon_plasmanade", "h3_beam_rifle_tfa", "invisibility_cloak", "tfa_hr_swep_concussion_rifle", "climb_swep2", "h3_esword_tfa", "tfa_h2_covenant_carbine","halo3_dualplasmarifle","halo3_fuelrodcannon","halo3_dualplasmapistol","weapon_cuff_mp"},
   command = "sosseniorcommand",
   max = 5,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Swords of Sanghelios",
PlayerSpawn = function(ply)
   ply:SetHealth(800)
   ply:SetArmor(500)
   ply.HaloArmor=76
   ply:SetMaxHealth(800)
   ply:SetRunSpeed(285)
   ply:SetWalkSpeed(185)
end
})
TEAM_SOSGRUNT = DarkRP.createJob("SoS Grunt", {
   color = Color(127, 0, 255, 255),
   model = {"models/jds_01/characters/unggoy01.mdl"},
   description = [[The Swords of Sanghelios are a fractured group of the Covenant that have joined in alliance with the USNC to stop the Great Journey]],
   weapons = {"tfa_hr_swep_needler", "weapon_plasmanade", "climb_swep2", "halo3_plasmapistol"},
   command = "sosgrunt",
   max = 0,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Swords of Sanghelios",
PlayerSpawn = function(ply)
   ply:SetHealth(275)
   ply.HaloArmor=25
   ply:SetMaxHealth(275)
end
})
TEAM_SOSDEACONGRUNT = DarkRP.createJob("SoS Deacon Grunt", {
   color = Color(127, 0, 255, 255),
   model = {"models/jds_deacon/characters/unggoy_deacon.mdl"},
   description = [[A respectful unggoy member, blessed by the Arbiter of the Swords of Sanghelios]],
   weapons = {"halo3_needler","weapon_plasmanade","climb_swep2","tfa_hr_swep_plasma_rifle","tfa_h4_cov_fuel_rod"},
   command = "sosdeacongrunt",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Swords of Sanghelios",
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply:SetArmor(50)
   ply.HaloArmor=30
   ply:SetMaxHealth(250)
end
})
TEAM_SOSJACKAL = DarkRP.createJob("SoS Jackal", {
   color = Color(127, 0, 255, 255),
   model = {"models/player/jackal.mdl"},
   description = [[The Swords of Sanghelios are a fractured group of the Covenant that have joined in alliance with the USNC to stop the Great Journey]],
   weapons = {"Climb_swep2","Keys","h3_beam_rifle_tfa","tfa_hr_swep_needler", "weapon_plasmanade","swep_radiodevice", "tfa_hr_swep_needle_rifle", "halo3_plasmapistol"},
   command = "sosjackal",
   max = 12,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Swords of Sanghelios",
PlayerSpawn = function(ply)
   ply:SetHealth(150)
   ply.HaloArmor=30
   ply:SetMaxHealth(150)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(200)
end
})
TEAM_SOSJACKALSNIPER = DarkRP.createJob("SoS Jackal Sniper", {
   color = Color(127, 0, 255, 255),
   model = {"models/player/jackal.mdl"},
   description = [[You are a Legendary Jackal Sniper, apart of the SoS]],
   weapons = {"Climb_swep2", "tfa_hr_swep_plasma_rifle", "h3_beam_rifle_tfa", "tfa_h2_covenant_carbine", "halo3_beamrifle","invisibility_cloak","tfa_h2_covenant_carbine"},
   command = "sosjackalsniper",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Swords of Sanghelios",
PlayerSpawn = function(ply)
   ply:SetHealth(150)
   ply.HaloArmor=25
   ply:SetMaxHealth(150)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(200)
end
})
TEAM_ARBITER = DarkRP.createJob("SoS Arbiter", {
   color = Color(127, 0, 255, 255),
   model = {"models/killer_instinct/characters/arbiter/arbiter_mreze.mdl"},
   description = [[The Arbiter led the SoS to  y, exposing the exploits of the prophet's lies]],
   weapons = {"h3_esword_tfa","tfa_hr_swep_plasma_rifle","tfa_hr_swep_needle_rifle", "tfa_hr_swep_needler", "weapon_plasmanade", "h3_beam_rifle_tfa", "invisibility_cloak", "tfa_hr_swep_concussion_rifle", "climb_swep2", "halo5_prophet_bane_sword","tfa_h2_covenant_carbine","halo3_dualplasmarifle","halo3_fuelrodcannon","halo3_dualplasmapistol","weapon_cuff_mp"},
   command = "sosarbiter",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Swords of Sanghelios",
PlayerSpawn = function(ply)
   ply:SetHealth(800)
   ply:SetArmor(600)
   ply.HaloArmor=76
   ply:SetMaxHealth(800)
   ply:SetRunSpeed(285)
   ply:SetWalkSpeed(185)
end
})
TEAM_MGALEKGOLO = DarkRP.createJob("SoS Mgalekgolo", {
   color = Color(127, 0, 255, 255),
   model = {"models/hce/spv3/cov/hunter/hunter.mdl"},
   description = [[Mgalekgolo are groups of worms called Lekgolo, Spanning from 13 feet tall to massive heights depending on the planet's gravity. Rarely any sided with the SoS but these heavy troops will decimate armies.]],
   weapons = {"tfa_hr_swep_needler","weapon_plasmanade","climb_swep2","halo3_fuelrodcannon","h3_beam_rifle_tfa","halo3_dualplasmarifle", "tfa_h4_cov_fuel_rod"},
   command = "Mgalekgolo",
   max = 2,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Swords of Sanghelios",
PlayerSpawn = function(ply)
   ply:SetHealth(1600)
   ply.HaloArmor=95
   ply:SetMaxHealth(1600)
   ply:SetRunSpeed(200)
   ply:SetWalkSpeed(120)
end
})
TEAM_STAFF1 = DarkRP.createJob("Mod On Duty", {
	color = Color(230,12,40, 255),
	model = "models/revival/mod/mod.mdl",
	description = [[For use by Trial Moderators and Moderators only!]],
	weapons = {"weapon_physgun", "weapon_toolgun"},
	command = "mod",
	max = 0,
	salary = 0,
	admin = 0,
	vote = false,
	hasLicense = false,
	category = "Admin",
})
TEAM_STAFF2 = DarkRP.createJob("Admin on Duty", {
	color = Color(230,12,40, 255),
	model = "models/revival/red/red.mdl",
	description = [[For use by Admins and Senior Admins only!]],
	weapons = {"weapon_physgun", "weapon_toolgun"},
	command = "aod",
	max = 0,
	salary = 0,
	admin = 0,
	vote = false,
	hasLicense = false,
	category = "Admin",
})
TEAM_STAFF3 = DarkRP.createJob("Upper Staff on Duty", {
	color = Color(230,12,40, 255),
	model = "models/revival/gold/gold.mdl",
	description = [[For use by Developers and anyone above Head Admin only!]],
	weapons = {"weapon_physgun", "weapon_toolgun"},
	command = "uod",
	max = 0,
	salary = 0,
	admin = 0,
	vote = false,
	hasLicense = false,
	category = "Admin",
})
TEAM_STAFF4 = DarkRP.createJob("Gamemaker on Duty", {
	color = Color(230,12,40, 255),
	model = "models/revival/red/red.mdl",
	description = [[For use by gamemakers only!]],
	weapons = {"weapon_physgun", "weapon_toolgun"},
	command = "god",
	max = 0,
	salary = 0,
	admin = 0,
	vote = false,
	hasLicense = false,
	category = "Admin",
})

TEAM_XERXESFL = DarkRP.createJob("Xerxes Fireteam Leader", {
   color = Color(105, 105, 105, 255),
   model = {"models/valk/haloreach/UNSC/Spartan/Spartan.mdl","models/suno/player/zeus/female/fang.mdl", "models/suno/player/zeus/male/formal_bill.mdl"},
   description = [[NULL DATA]],
   weapons = {"Tfa_h5_br85n_h5", "tfa_rebirth_srs99s4am","weapon_sh_doorcharge","tfa_hr_swep_assault_rifle","tfa_hr_swep_dmr","tfa_hr_swep_magnum","climb_swep2","weapon_fistsofreprisal","tfa_hr_swep_shotgun","tfa_rebirth_m7ds","stungun","weapon_cuff_mp","invisibility_cloak","halo_frag","weapon_sh_flashbang","tfa_hr_swep_spartan_laser"},
   command = "xerxessquadleader",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Xerxes",
   ammo = {["item_ammo_smg1_grenade"] = 50},
PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply.HaloArmor=70
   ply:SetArmor(600)
   ply:SetMaxHealth(600)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(210)
   ply:SetJumpPower(280)
end
})
TEAM_XERXESRIFLEMAN = DarkRP.createJob("Xerxes Rifleman", {
   color = Color(105, 105, 105, 255),
   model = {"models/valk/haloreach/UNSC/Spartan/Spartan.mdl","models/suno/player/zeus/female/fang.mdl", "models/suno/player/zeus/male/formal_bill.mdl"},
   description = [[NULL DATA]],
   weapons = {"weapon_policeshield","tfa_hr_swep_shotgun","tfa_hr_swep_assault_rifle","Tfa_h5_br85n_h5","tfa_hr_swep_magnum","climb_swep2","weapon_fistsofreprisal","weapon_bactainjector","weapon_bactanade","med_kit","darkrp_defibrillator","tfa_rebirth_m7ds","invisibility_cloak","stungun","weapon_cuff_mp","halo_frag","weapon_sh_flashbang","weapon_sh_doorcharge"},
   command = "xerxesrifleman",
   max = 2,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Xerxes",
   ammo = {["item_ammo_smg1_grenade"] = 50},
PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply.HaloArmor=70
   ply:SetArmor(600)
   ply:SetMaxHealth(600)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(210)
   ply:SetJumpPower(280)
end
})
TEAM_XERXESSUPPORT = DarkRP.createJob("Xerxes Support", {
   color = Color(105, 105, 105, 255),
   model = {"models/valk/haloreach/UNSC/Spartan/Spartan.mdl","models/suno/player/zeus/female/fang.mdl", "models/suno/player/zeus/male/formal_bill.mdl"},
   description = [[NULL DATA]],
   weapons = {"tfa_rec_hmini","tfa_rebirth_m41","tfa_hr_swep_assault_rifle","tfa_hr_swep_magnum","climb_swep2","weapon_fistsofreprisal","tfa_hr_swep_shotgun","tfa_rebirth_m7ds","invisibility_cloak","revival_c4","stungun","weapon_cuff_mp","halo_frag","weapon_sh_flashbang","weapon_sh_doorcharge"},
   command = "xerxessupport",
   max = 2,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Xerxes",
   ammo = {["item_ammo_smg1_grenade"] = 50},
PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply.HaloArmor=70
   ply:SetArmor(600)
   ply:SetMaxHealth(600)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(210)
   ply:SetJumpPower(280)
end                                                       
})
TEAM_XERXESSHARPSHOOTER = DarkRP.createJob("Xerxes Sharpshooter", {
   color = Color(105, 105, 105, 255),
   model = {"models/valk/haloreach/UNSC/Spartan/Spartan.mdl", "models/suno/player/zeus/female/fang.mdl", "models/suno/player/zeus/male/formal_bill.mdl"},
   description = [[NULL DATA]],
   weapons = {"tfa_hr_swep_shotgun","tfa_rebirth_srs99s4am","tfa_hr_swep_assault_rifle","tfa_hr_swep_magnum","climb_swep2","weapon_fistsofreprisal","tfa_rebirth_m7ds","invisibility_cloak","stungun","weapon_cuff_mp","halo_frag","weapon_sh_flashbang","weapon_sh_doorcharge"},
   command = "xerxessharpshooter",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Xerxes",
   ammo = {["item_ammo_smg1_grenade"] = 50},
PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply.HaloArmor=70
   ply:SetArmor(600)
   ply:SetMaxHealth(600)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(210)
   ply:SetJumpPower(280)
end
})
TEAM_FREELANCERCHAMPION = DarkRP.createJob("Freelancer Grand Champion", {
   color = Color(10, 124, 188, 255),
   model = {"models/player/lordvipes/h3_spartans_mps/h3spartan_mps_cvp.mdl"},
   description = [[The grand champion of the freelancer tournaments.]],
   weapons = {"csgo_huntsman", "tfa_rebirth_m6g","tfa_rebirth_m90","tfa_rebirth_srs99c2s","tfa_rec_hmini","Tfa_rebirth_m7ds","tfa_rebirth_m6cs","invisibility_cloak","climb_swep2","tfa_rebirth_br55"},
   command = "flc1",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Freelancer",
PlayerSpawn = function(ply)
   ply:SetHealth(500)
   ply:SetArmor(450)
   ply:SetMaxHealth(500)
   ply.HaloArmor=75
   ply:SetRunSpeed(290)
   ply:SetWalkSpeed(185)
end
})
TEAM_FREELANCERCHAMPION2 = DarkRP.createJob("Freelancer Champion", {
   color = Color(10, 124, 188, 255),
   model = {"models/player/lordvipes/h3_spartans_mps/h3spartan_mps_cvp.mdl"},
   description = [[The second place champion of the freelancer tournaments.]],
   weapons = {"csgo_huntsman", "tfa_rebirth_m6g","tfa_rebirth_m90","tfa_rebirth_srs99c2s","tfa_rec_hmini","Tfa_rebirth_m7ds","tfa_rebirth_m6cs","invisibility_cloak","climb_swep2","tfa_rebirth_br55"},
   command = "flc2",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Freelancer",
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply:SetArmor(450)
   ply:SetMaxHealth(400)
   ply.HaloArmor=75
   ply:SetRunSpeed(290)
   ply:SetWalkSpeed(185)
end
})
TEAM_SOSHEAVYGRUNT = DarkRP.createJob("SoS Heavy grunt", {
    color = Color(86, 86, 86, 255),
    model = {"models/jds_heavy/characters/unggoy_heavy.mdl"},
    description = [[SOS Heavy Grunt (Donation)]],
    weapons = {"halo3_fuelrodcannon", "tfa_hr_swep_needler", "climb_swep2", "weapon_plasmanade", "tfa_h4_cov_fuel_rod", "tfa_hr_swep_plasma_rifle"},
    command = "sosheavygrunt",
    max = 20,
    salary = 35,
    admin = 0,
    vote = false,
    hasLicense = false,
    category = "Swords of Sanghelios",
    PlayerLoadout = function(ply) -- this line is added. Remember a comma at the end of above line if it is not there.
        ply:SetHealth(300)
        ply.HaloArmor=60
		ply:SetMaxHealth(300)
    end
})
TEAM_SOSJACKELMEDIC = DarkRP.createJob("SoS Jackal medic", {
    color = Color(86, 86, 86, 255),
    model = {"models/player/jackal.mdl"},
    description = [[SOS Jackel Medic. (Donation)]],
    weapons = {"med_kit","darkrp_defibrillator","climb_swep2","weapon_plasmanade","tfa_hr_swep_needler","weapon_bactainjector", "h3_beam_rifle_tfa", "tfa_h2_covenant_carbine", "halo3_dualplasmapistol"},
    command = "sosjackelmedic",
    max = 20,
    salary = 35,
    admin = 0,
    vote = false,
    hasLicense = false,
    category = "Swords of Sanghelios",
    PlayerLoadout = function(ply) -- this line is added. Remember a comma at the end of above line if it is not there.
        ply:SetHealth(150)
        ply.HaloArmor=30
		ply:SetRunSpeed(310)
		ply:SetWalkSpeed(200)
    end
})
TEAM_ALPHASIXSQL = DarkRP.createJob("Alpha Six Squad Lead", {
    color = Color(86, 86, 86, 255),
     model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
    description = [[Alpha Six Squad. (Donation)]],
    weapons = {"h3_smg_tfa_odst", "h3_odst_socom_tfa","tfa_rebirth_srs99c2s","tfa_rebirth_m394b","tfa_rebirth_xbr55s","hr_tfa_shotgun","tfa_rebirth_m90", "keys", "climb_swep2", "csgo_huntsman", "halo_frag", "tfa_rebirth_br55"},
    command = "alphasixsquadlead",
    max = 1,
    salary = 35,
    admin = 0,
    vote = false,
    hasLicense = false,
    category = "Alpha Six",
    PlayerLoadout = function(ply) -- this line is added. Remember a comma at the end of above line if it is not there.
        ply:SetHealth(250)
        ply:SetMaxHealth(250)
        ply:SetArmor(200)
        ply.HaloArmor=65
        ply:SetRunSpeed(250)
    end
})
TEAM_ALPHASIXRIFLE = DarkRP.createJob("Alpha Six Rifleman", {
    color = Color(86, 86, 86, 255),
     model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
    description = [[Alpha Six Squad. (Donation)]],
    weapons = {"h3_smg_tfa_odst", "h3_odst_socom_tfa", "tfa_rebirth_srs99c2s", "tfa_rebirth_xbr55s","tfa_rebirth_m394b","tfa_rebirth_ma37", "tfa_rebirth_m90", "keys", "climb_swep2", "csgo_huntsman" },
    command = "alphasixsquadrifle",
    max = 2,
    salary = 35,
    admin = 0,
    vote = false,
    hasLicense = false,
    category = "Alpha Six",
    PlayerLoadout = function(ply) -- this line is added. Remember a comma at the end of above line if it is not there.
        ply:SetHealth(250)
        ply:SetMaxHealth(250)
        ply:SetArmor(200)
        ply.HaloArmor=65
        ply:SetRunSpeed(250)
    end
})
TEAM_ALPHASIXCORPS = DarkRP.createJob("Alpha Six Corpsman", {
    color = Color(86, 86, 86, 255),
     model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
    description = [[Alpha Six Squad. (Donation)]],
    weapons = {"h3_smg_tfa_odst","h3_odst_socom_tfa","tfa_rebirth_m90","tfa_rebirth_srs99c2s","tfa_rebirth_ma37s", "tfa_rebirth_m394b", "weapon_medkit", "weapon_bactainjector", "darkrp_defibrillator", "keys", "climb_swep2", "csgo_huntsman", "halo_frag", "tfa_rebirth_br55"},
    command = "alphasixsquadcorps",
    max = 1,
    salary = 35,
    admin = 0,
    vote = false,
    hasLicense = false,
    category = "Alpha Six",
    PlayerLoadout = function(ply) -- this line is added. Remember a comma at the end of above line if it is not there.
        ply:SetHealth(250)
        ply:SetMaxHealth(250)
        ply:SetArmor(200)
        ply.HaloArmor=65
        ply:SetRunSpeed(250)
    end
})
TEAM_ALPHASIXMARKSMAN = DarkRP.createJob("Alpha Six Marksman", {
    color = Color(86, 86, 86, 255),
     model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
    description = [[Alpha Six Squad. (Donation)]],
    weapons = {"h3_smg_tfa_odst", "h3_odst_socom_tfa","tfa_rebirth_m90","tfa_rebirth_srs99c2b","tfa_rebirth_srs99c2s", "tfa_rebirth_m394b", "keys", "climb_swep2", "csgo_huntsman", "halo_frag", "tfa_rebirth_br55",},
    command = "alphasixsquadmarksman",
    max = 2,
    salary = 35,
    admin = 0,
    vote = false,
    hasLicense = false,
    category = "Alpha Six",
    PlayerLoadout = function(ply) -- this line is added. Remember a comma at the end of above line if it is not there.
        ply:SetHealth(250)
        ply:SetMaxHealth(250)
        ply:SetArmor(200)
        ply.HaloArmor=65
        ply:SetRunSpeed(250)
    end
})
TEAM_ALPHASIXDEMOLITIONIST = DarkRP.createJob("Alpha Six Demolitionist", {
    color = Color(86, 86, 86, 255),
     model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
    description = [[Alpha Six Squad. (Donation)]],
    weapons = {"tfa_h4_spartan_weapon_saw", "tfa_rebirth_m41", "h3_smg_tfa_odst", "h3_odst_socom_tfa","tfa_rebirth_m394b","tfa_rebirth_srs99c2s","Weapon_slam", "tfa_rebirth_m90", "keys", "climb_swep2", "csgo_huntsman", "tfa_m247_gpmg", "tfa_rebirth_br55", "halo_frag", "revival_c4"},
    command = "alphasixsquad",
    max = 1,
    salary = 35,
    admin = 0,
    vote = false,
    hasLicense = false,
    category = "Alpha Six",
    PlayerLoadout = function(ply) -- this line is added. Remember a comma at the end of above line if it is not there.
        ply:SetHealth(250)
        ply:SetMaxHealth(250)
        ply:SetArmor(200)
        ply.HaloArmor=65
        ply:SetRunSpeed(250)
    end
})
TEAM_SWORDFISHPILOT = DarkRP.createJob("Vulcan Pilot", {
   color = Color(102, 0, 0, 255),
   model = {"models/jessev92/halo/unsc_h3_marine/pilot.mdl"},
   description = [[We are the Guardians Of The Sky. We are the few and the proud to sweep the enemies from darkness and bring the light that our bullets shine upon the earth and so we make sure our airspace is forever clear]],
   weapons = {"halorepair_tool","tfa_ishi_ma37","tfa_rebirth_m7d","tfa_rebirth_br55","tfa_rebirth_m6g"},
   command = "swordfishpilot",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Vulcan",
PlayerSpawn = function(ply)
	ply:SetHealth(140)
	ply.HaloArmor=35
	ply:SetMaxHealth(140)
end
})
TEAM_SWORDFISHSL = DarkRP.createJob("Vulcan Squad Leader", {
   color = Color(102, 0, 0, 255),
   model = {"models/jessev92/halo/unsc_h3_marine/pilot.mdl"},
   description = [[We are the Guardians Of The Sky. We are the few and the proud to sweep the enemies from darkness and bring the light that our bullets shine upon the earth and so we make sure our airspace is forever clear]],
   weapons = {"halorepair_tool","tfa_ishi_ma37", "tfa_rebirth_m90c", "tfa_rebirth_br55","climb_swep2", "weapon_rpw_binoculars","tfa_rebirth_m6g", "weapon_vj_flaregun"},
   command = "swordfishlead",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Vulcan",
PlayerSpawn = function(ply)
	ply:SetHealth(140)
	ply.HaloArmor=35
	ply:SetMaxHealth(140)
end
})
TEAM_SWORDFISHCOPILOT = DarkRP.createJob("Vulcan Co-Pilot", {
   color = Color(102, 0, 0, 255),
   model = {"models/jessev92/halo/unsc_h3_marine/pilot.mdl"},
   description = [[Vulcan were pilots that undertook heavy tasks in the air]],
   weapons = {"halorepair_tool","tfa_ishi_ma37","tfa_rebirth_m7d","tfa_rebirth_br55","tfa_rebirth_m6g"},
   command = "swordfishcopilot",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Vulcan",
PlayerSpawn = function(ply)
	ply:SetHealth(140)
	ply.HaloArmor=35
	ply:SetMaxHealth(140)
end
})
TEAM_REAPERPILOT = DarkRP.createJob("Nemesis Pilot", {
   color = Color(51, 0, 0, 255),
   model = {"models/jessev92/halo/unsc_h3_odst/pilot.mdl"},
   description = [[Nemesis are pilots that undertook heavy tasks in the air]],
   weapons = {"halorepair_tool"," tfa_ishi_ma37","Tfa_rebirth_m7ds","tfa_rebirth_br55","climb_swep2","tfa_rebirth_m6cs"},
   command = "reaperpilot",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Nemesis",
PlayerSpawn = function(ply)
	ply:SetHealth(200)
	ply.HaloArmor=40
	ply:SetMaxHealth(200)
end
})
TEAM_REAPERCOPILOT = DarkRP.createJob("Nemesis Co-Pilot", {
   color = Color(51, 0, 0, 255),
   model = {"models/jessev92/halo/unsc_h3_odst/pilot.mdl"},
   description = [[Nemesis were pilots that undertook heavy tasks in the air]],
   weapons = {"halorepair_tool","tfa_ishi_ma37","Tfa_rebirth_m7ds","tfa_rebirth_br55","climb_swep2","tfa_rebirth_m6cs"},
   command = "reapercopilot",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Nemesis",
PlayerSpawn = function(ply)
	ply:SetHealth(200)
	ply.HaloArmor=40
	ply:SetMaxHealth(200)
end
})
TEAM_REAPERSL = DarkRP.createJob("Nemesis Squad Leader", {
   color = Color(51, 0, 0, 255),
   model = {"models/jessev92/halo/unsc_h3_odst/pilot.mdl"},
   description = [[Nemesis were pilots that undertook heavy tasks in the air]],
   weapons = {"halorepair_tool","tfa_ishi_ma37","Tfa_rebirth_m7ds","tfa_rebirth_br55","climb_swep2", "weapon_rpw_binoculars","tfa_rebirth_m6cs", "weapon_vj_flaregun"},
   command = "reapersquadlead",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Nemesis",
PlayerSpawn = function(ply)
	ply:SetHealth(200)
	ply.HaloArmor=40
	ply:SetMaxHealth(200)
end
})
TEAM_REAPERHPILOT = DarkRP.createJob("Nemesis Heavy Pilot", {
   color = Color(51, 0, 0, 255),
   model = {"models/gonzo/unscofficers/delrio/delrio.mdl"},
   description = [[Nemesis were pilots that undertook heavy tasks in the air]],
   weapons = {"halorepair_tool","tfa_ishi_ma37","climb_swep2","tfa_rebirth_m90c","tfa_rebirth_m392","voice_amplifier","fas2_m79","tfa_rebirth_br55s","tfa_rebirth_m6cs"},
   command = "reaperheavy",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Nemesis",
PlayerSpawn = function(ply)
	ply:SetHealth(225)
	ply.HaloArmor=40
	ply:SetMaxHealth(225)
end
})
TEAM_AIRBORNE = DarkRP.createJob("Valkyrie XO", {
   color = Color(51, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_danlin.mdl"},
   description = [[The XO of Valkyrie.]],
   weapons = {"Tfa_rebirth_m7ds","tfa_rebirth_br55","climb_swep2","tfa_rebirth_m6cs"},
   command = "headofairborne",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Valkyrie",
PlayerSpawn = function(ply)
   ply.HaloArmor=20
end
})
TEAM_AIRBORNEHOP = DarkRP.createJob("Head Of Pilots", {
   color = Color(51, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/gonzo/unscofficers/keyes/keyes.mdl"},
   description = [[The Head Of Pilots was a High ranking officer commissioned to command and organize Pilot squads.]],
   weapons = {"tfa_rebirth_m7d","tfa_rebirth_br55","climb_swep2","Tfa_rebirth_m6c", "weapon_vj_flaregun", "Halorepair_tool", "Halorepair_tool"},
   command = "headofpilots",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "MAJCOM",
PlayerSpawn = function(ply)
   ply:SetHealth(125)
	ply.HaloArmor=35
	ply:SetMaxHealth(125)
end
})
TEAM_HORT = DarkRP.createJob("Head Of Research Team", {
   color = Color(51, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/ishi/halo_rebirth/player/odst/female/odst_miia.mdl"},
   description = [[The Head Of Research was a High ranking officer commissioned to command and organize Research Team]],
   weapons = {"Halorepair_tool","tfa_rebirth_m7d","tfa_rebirth_br55","climb_swep2","Tfa_rebirth_m6c"},
   command = "headofrt",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "MAJCOM",
PlayerSpawn = function(ply)
   ply.HaloArmor=20
end
})
TEAM_UNSCAFCO = DarkRP.createJob("UNSCAF Commissioned Officer", {
   color = Color(51, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/gonzo/unscofficers/delrio/delrio.mdl"},
   description = [[You are a CO for the UNSCAF]],
   weapons = {"tfa_rebirth_m7d","tfa_rebirth_br55","climb_swep2","Tfa_rebirth_m6c", "halorepair_tool"},
   command = "unscafco",
   max = 5,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "MAJCOM",
PlayerSpawn = function(ply)
   ply:SetHealth(125)
	ply.HaloArmor=35
	ply:SetMaxHealth(125)
end
})
TEAM_UNSCAFG = DarkRP.createJob("General", {
   color = Color(51, 0, 0, 255),
   model = {"models/gonzo/unscofficers/lasky/lasky.mdl", "models/ishi/halo_rebirth/player/odst/male/odst_heretic.mdl"},
   description = [[You are the General in the UNSCAF]],
   weapons = {"tfa_rebirth_ma5c","tfa_rebirth_m90c","weapon_cuff_mp","tfa_rebirth_m392","voice_amplifier","tfa_rebirth_br55s","tfa_rebirth_m6g"},
   command = "general",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "MAJCOM",
PlayerSpawn = function(ply)
   ply.HaloArmor=10
end
})
TEAM_UNSCAFGOAF = DarkRP.createJob("General Of The Airforce", {
   color = Color(51, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl", "models/gonzo/unscofficers/dress/dress.mdl"},
   description = [[You are the General for the UNSCAF]],
   weapons = {"tfa_rebirth_ma5c","tfa_rebirth_m90c","weapon_cuff_mp","tfa_rebirth_m392","voice_amplifier","fas2_m79","tfa_rebirth_m41","tfa_rebirth_br55s","Tfa_rebirth_m6c", "Halorepair_tool", "climb_swep2"},
   command = "goaf",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "MAJCOM",
PlayerSpawn = function(ply)
   ply:SetHealth(125)
   ply.HaloArmor=35
   ply:SetMaxHealth(125)
end
})
TEAM_ONIPROFESSOR = DarkRP.createJob("ONI Professor", {
   color = Color(0, 0, 0, 0),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl"},
   description = [[ONI professor with medical and technology expertise.]],
   weapons = {"weapon_bactainjector","tfa_rebirth_srs99c2b", "tfa_h5_br85n_h5", "h3_smg_tfa", "climb_swep2", "tfa_rebirth_m45e", "halorepair_tool", "weapon_bactanade", "med_kit", "darkrp_defibrillator", "weapon_cuff_admin","tfa_rebirth_m6cs","csgo_huntsman","invisibility_cloak", "tfa_rebirth_m6cs", "tfa_rebirth_m7ds"},
   command = "oniprofessor",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "ONI",
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply.HaloArmor=60
   ply:SetMaxHealth(400)
   ply:SetArmor(200)
end
})
TEAM_ODSTCOMMANDER = DarkRP.createJob("ODST Commander", {
   color = Color(0, 0, 0, 0),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[ODST Commander.]],
   weapons = {"climb_swep2","csgo_huntsman_night", "h3_smg_tfa_odst","h3_odst_socom_tfa","hr_tfa_shotgun","tfa_rebirth_br55","tfa_rebirth_m90","Tfa_rebirth_srs99c2b", "weapon_rpw_binoculars_scout","voice_amplifier","tfa_rebirth_m41", "halo_frag", "weapon_sh_flashbang", "tfa_h5_br85n_h5"},
   command = "odstcommanderone",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "ODST Command",
PlayerSpawn = function(ply)
   ply:SetHealth(350)
   ply.HaloArmor=70
   ply:SetMaxHealth(350)
   ply:SetArmor(200)
end
})
TEAM_ODSTEXECUTIVE = DarkRP.createJob("ODST Platoon Sergeant", {
   color = Color(0, 0, 0, 0),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[ODST Platoon Corpsman.]],
   weapons = {"climb_swep2","csgo_huntsman_night", "h3_smg_tfa_odst","h3_odst_socom_tfa","hr_tfa_shotgun","tfa_rebirth_br55","tfa_rebirth_m90","Tfa_rebirth_srs99c2b", "weapon_rpw_binoculars_scout","voice_amplifier","tfa_rebirth_m41", "halo_frag", "weapon_sh_flashbang"},
   command = "odstsergeant",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "ODST Command",
PlayerSpawn = function(ply)
   ply:SetHealth(350)
   ply.HaloArmor=70
   ply:SetMaxHealth(350)
   ply:SetArmor(200)
end
})
TEAM_ODSTPLATOONSERGEANT = DarkRP.createJob("ODST Platoon Executive", {
   color = Color(0, 0, 0, 0),
   model = {"models/valk/h3odst/unsc/odst/odst.mdl"},
   description = [[ODST Platoon Executive.]],
   weapons = {"climb_swep2","csgo_huntsman_night", "h3_smg_tfa_odst","h3_odst_socom_tfa","hr_tfa_shotgun","tfa_rebirth_br55","tfa_rebirth_m90","Tfa_rebirth_srs99c2b", "weapon_rpw_binoculars_scout","voice_amplifier","tfa_rebirth_m41", "halo_frag", "weapon_sh_flashbang"},
   command = "odstexecutive",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "ODST Command",
PlayerSpawn = function(ply)
   ply:SetHealth(350)
   ply.HaloArmor=70
   ply:SetMaxHealth(350)
   ply:SetArmor(200)
end
})
TEAM_VALKRYIEAT = DarkRP.createJob("Valkyrie Assault", {
   color = Color(51, 0, 0, 255),
   model = {"models/chaosnarwhal/unscab_oni_outlander_base2.mdl"},
   description = [[Valkyrie]],
   weapons = {"tfa_rebirth_xbr55","tfa_rebirth_m6g","tfa_rebirth_ma5c","tfa_rebirth_m90","weapon_fists","climb_swep2", "csgo_huntsman_crimsonwebs", "halo_frag", "tfa_rebirth_m392", "swep_radiodevice"},
   command = "valkyrieassaulter",
   max = 5,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Valkyrie",
PlayerSpawn = function(ply)
   ply:SetHealth(150)
   ply.HaloArmor=30
   ply:SetMaxHealth(150)
   ply:SetArmor(100)
end
})
TEAM_VALKRYIEHT = DarkRP.createJob("Valkyrie Heavy Trooper", {
   color = Color(51, 0, 0, 255),
   model = {"models/chaosnarwhal/unscab_oni_outlander_base2.mdl"},
   description = [[Valkyrie]],
   weapons = {"tfa_h4_spartan_weapon_saw","tfa_rebirth_m90","recon_halominigun","tfa_rebirth_m6g","climb_swep2","weapon_fists","tfa_rebirth_m41", "csgo_huntsman_crimsonwebs", "halo_frag", "tfa_rebirth_m392", "swep_radiodevice"},
   command = "valkyrieheavyrooper",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Valkyrie",
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply.HaloArmor=40
   ply:SetMaxHealth(250)
   ply:SetArmor(250)
end
})
TEAM_VALKRYIE = DarkRP.createJob("Valkyrie Squad Leader", {
   color = Color(51, 0, 0, 255),
   model = {"models/chaosnarwhal/unscab_oni_outlander_base2.mdl"},
   description = [[Valkyrie]],
   weapons = {"tfa_rebirth_m90","tfa_rebirth_ma5d","climb_swep2","voice_amplifier","invisibility_cloak","tfa_rebirth_m392","Tfa_rebirth_m7ds","tfa_rebirth_ma37s","tfa_rebirth_xbr55","weapon_fists", "weapon_rpw_binoculars", "csgo_huntsman_crimsonwebs", "halo_frag", "swep_radiodevice","jorge_chaingun"},
   command = "valkyriesquadleader",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Valkyrie",
PlayerSpawn = function(ply)
   ply:SetHealth(200)
   ply.HaloArmor=40
   ply:SetMaxHealth(200)
   ply:SetArmor(200)
end
})
TEAM_VALKRYIESS = DarkRP.createJob("Valkyrie Sharpshooter", {
   color = Color(51, 0, 0, 255),
   model = {"models/chaosnarwhal/unscab_oni_outlander_base2.mdl"},
   description = [[Valkyrie]],
   weapons = {"tfa_rebirth_srs99s2am", "tfa_rebirth_m392", "tfa_rebirth_m6c", "tfa_rebirth_ma5c", "tfa_rebirth_br55", "climb_swep2","Weapon_fists","Tfa_rebirth_m7ds", "csgo_huntsman_crimsonwebs", "halo_frag", "swep_radiodevice"},
   command = "valkyriesharpshooter",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Valkyrie",
PlayerSpawn = function(ply)
	ply:SetHealth(150)
	ply.HaloArmor=30
	ply:SetMaxHealth(150)
   ply:SetArmor(100)
end
})
TEAM_VALKRYIEMED = DarkRP.createJob("Valkyrie Medic", {
   color = Color(51, 0, 0, 255),
   model = {"models/chaosnarwhal/unscab_oni_outlander_base2.mdl"},
   description = [[Valkyrie Medic]],
   weapons = {"darkrp_defibrillator","weapon_bactainjector","tfa_rebirth_m7d","tfa_rebirth_br55","climb_swep2","tfa_rebirth_m90","tfa_rebirth_m6g","tfa_rebirth_ma5c","weapon_fists", "csgo_huntsman_crimsonwebs", "halo_frag", "tfa_rebirth_m392", "swep_radiodevice", "med_kit" },
   command = "valkyriemedic",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Valkyrie",
PlayerSpawn = function(ply)
   ply:SetHealth(150)
   ply.HaloArmor=45
   ply:SetMaxHealth(150)
   ply:SetArmor(150)
end
})
TEAM_VALKRYIECLIPP = DarkRP.createJob("Valkyrie Clipper", {
   color = Color(51, 0, 0, 255),
   model = {"models/chaosnarwhal/unscab_oni_outlander_base2.mdl"},   
   description = [[Valkyrie Clipper]],
   weapons = {"Weapon_fists","climb_swep2","h3_smg_tfa","tfa_l4d2_kfkat","tfa_rebirth_m90","tfa_rebirth_m6g","tfa_rebirth_m394","tfa_rebirth_ma5d", "csgo_huntsman_crimsonwebs", "halo_frag", "tfa_rebirth_m392", "swep_radiodevice"},
   command = "valkyrieclipper",
   max = 2,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Valkyrie",
PlayerSpawn = function(ply)
   ply:SetHealth(150)
   ply.HaloArmor=45
   ply:SetMaxHealth(150)
   ply:SetArmor(150)
end
})
TEAM_VALKRYIESTALK = DarkRP.createJob("Valkyrie Stalkers", {
   color = Color(51, 0, 0, 255),
   model = {"models/chaosnarwhal/unscab_oni_outlander_base2.mdl"},   
   description = [[Valkyrie Stalkers]],
   weapons = {"Weapon_fists","climb_swep2","tfa_rebirth_m6c","invisibility_cloak","Tfa_rebirth_m7ds","tfa_rebirth_srs99c2s","tfa_rebirth_br55s","tfa_rebirth_ma5d", "csgo_huntsman_crimsonwebs", "halo_frag", "tfa_rebirth_m392", "swep_radiodevice"},
   command = "valkyriestalkers",
   max = 3,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Valkyrie",
PlayerSpawn = function(ply)
   ply:SetHealth(200)
   ply.HaloArmor=45
   ply:SetMaxHealth(200)
   ply:SetArmor(150)
end
})
TEAM_HURGAOK = DarkRP.createJob("Huragok", {
   color = Color(10, 124, 188, 255),
   model = {"models/hawkshuragok/engineer.mdl","models/nxtius/characters/odst/engineer/engineer.mdl"},
   description = [[Huragok (Donation)]],
   weapons = {"halorepair_tool","darkrp_defibrillator", "med_kit", "weapon_shaman", "med_kit", "weapon_bactanade"},
   command = "huragok",
   max = 5,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Swords of Sanghelios",
PlayerSpawn = function(ply)
   ply:SetHealth(100)
   ply:SetArmor(100)
   ply:SetMaxHealth(100)
   ply.HaloArmor=25
end
})
TEAM_MOSLEADER = DarkRP.createJob("MOS Squad Leader", {
   color = Color(85,107,47, 0),
   model = {"models/gonzo/deviliousmarines/skull/skullm.mdl", "models/gonzo/deviliousmarines/skull/skullf.mdl"},
   description = [[MOS, AKA Men of Steel. Mission Objective, Protect and take all orders from Fleet Admiral and only Fleet Admiral.]],
   weapons = {"tfa_rec_hmini", "tfa_rebirth_m7ds","tfa_rebirth_m6c","tfa_rebirth_m45e","tfa_rebirth_m7d", "climb_swep2", "tfa_hr_spartan_laser", "tfa_rebirth_m392", "halo_frag", "revival_c4", "weapon_sh_flashbang", "weapon_rpw_binoculars", "csgo_huntsman", "weapon_cuff_mp"},
   command = "menofsteelleader",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Men Of Steel",
PlayerSpawn = function(ply)
   ply:SetHealth(350)
   ply.HaloArmor=70
   ply:SetMaxHealth(350)
   ply:SetArmor(400)
end
})
TEAM_MOSMEMBER = DarkRP.createJob("MOS Squad Member", {
   color = Color(85,107,47, 0),
   model = {"models/gonzo/deviliousmarines/skull/skullm.mdl", "models/gonzo/deviliousmarines/skull/skullf.mdl"},
   description = [[MOS, AKA Men of Steel. Mission Objective, Protect and take all orders from Fleet Admiral and only Fleet Admiral.]],
   weapons = {"tfa_rec_hmini","tfa_rebirth_m45e","tfa_rebirth_m7d", "climb_swep2", "tfa_hr_spartan_laser", "tfa_rebirth_m392", "halo_frag", "revival_c4", "weapon_sh_flashbang", "weapon_rpw_binoculars", "csgo_huntsman", "tfa_rebirth_m6c", "weapon_cuff_mp", "tfa_rebirth_m7ds"},
   command = "menofsteelmember",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Men Of Steel",
PlayerSpawn = function(ply)
   ply:SetHealth(350)
   ply.HaloArmor=25
   ply:SetMaxHealth(350)
   ply:SetArmor(200)
end
})
TEAM_STRIKECOMMAND = DarkRP.createJob("ONI S.T.R.I.K.E. Commander", {
   color = Color(0, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl"},
   description = [[ONI were a secretive organization they-NULL DATA]],
   weapons = {"tfa_rebirth_m6cs", "tfa_rebirth_srs99c2s","tfa_rebirth_ma37s","weapon_cuff_mp", "tfa_rebirth_m90", "bkeycardscanner_procracker", "tfa_h4_spartan_weapon_saw", "climb_swep2", "csgo_huntsman", "Tfa_h5_br85n_h5","Tfa_rebirth_m7ds","tfa_rebirth_m6cs","halo_frag","weapon_sh_flashbang","weapon_sh_doorcharge","revival_c4","tfa_rebirth_m41","weapon_policeshield","invisibility_cloak","stungun"},
   command = "onistrikecommander",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "ONI Security Force", 
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply:SetArmor(300)
   ply.HaloArmor=65
   ply:SetMaxHealth(400)
   ply:SetRunSpeed(270)
   ply:SetWalkSpeed(170)
   ply:GiveAmmo(5, "grenade")
   ply:GiveAmmo(5, "grenade")
end
})
TEAM_STRIKEXO = DarkRP.createJob("ONI S.T.R.I.K.E. XO", {
   color = Color(0, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl"},
   description = [[ONI were a secretive organization they-NULL DATA]],
   weapons = {"tfa_rebirth_srs99c2s", "tfa_rebirth_ma37s","tfa_rebirth_m90", "weapon_cuff_mp", "bkeycardscanner_procracker", "tfa_rebirth_m6cs", "climb_swep2", "csgo_huntsman", "Tfa_h5_br85n_h5","tfa_rebirth_m7ds","tfa_rebirth_m6cs","halo_frag","weapon_sh_flashbang","weapon_sh_doorcharge","revival_c4","tfa_rebirth_m41","weapon_policeshield","invisibility_cloak", "stungun"},
   command = "onistrikexo",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "ONI Security Force", 
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply:SetArmor(300)
   ply.HaloArmor=65
   ply:SetMaxHealth(400)
   ply:SetRunSpeed(270)
   ply:SetWalkSpeed(170)
   ply:GiveAmmo(5, "grenade")
   ply:GiveAmmo(5, "grenade")
end
})
TEAM_BATTALIONCOMMANDER = DarkRP.createJob("Battalion Commander", {
   color = Color(0, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_danlin.mdl","models/ishi/halo_rebirth/player/odst/female/odst_dominique.mdl"},
   description = [[No-Description]],
   weapons = {"Voice_amplifier","tfa_h4_spartan_weapon_saw","Tfa_rebirth_ma5d","tfa_rebirth_m7d","tfa_rebirth_m392","tfa_rebirth_m90","tfa_rebirth_m6g","csgo_huntsman","weapon_rpw_binoculars","climb_swep2", "tfa_rebirth_ma5d"},
   command = "battalionco",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "HQ Command", 
PlayerSpawn = function(ply)
   ply:SetHealth(350)
   ply.HaloArmor=55
   ply:SetMaxHealth(350)
end
})
TEAM_BATTALIONEXECUTIVE = DarkRP.createJob("Battalion Executive Officer", {
   color = Color(0, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_danlin.mdl","models/ishi/halo_rebirth/player/odst/female/odst_dominique.mdl"},
   description = [[No-Description]],
   weapons = {"Voice_amplifier","tfa_h4_spartan_weapon_saw","Tfa_rebirth_ma5d","tfa_rebirth_m7d","tfa_rebirth_m392","tfa_rebirth_m90","tfa_rebirth_m6g","csgo_huntsman","weapon_rpw_binoculars","climb_swep2", "tfa_rebirth_ma5d"},
   command = "battalionexec",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "HQ Command", 
PlayerSpawn = function(ply)
   ply:SetHealth(350)
   ply.HaloArmor=55
   ply:SetMaxHealth(350)
end
})
TEAM_SMMCMARINES = DarkRP.createJob("Battalion Sergeant Major", {
   color = Color(0, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_danlin.mdl","models/ishi/halo_rebirth/player/odst/female/odst_dominique.mdl"},
   description = [[No-Description]],
   weapons = {"Voice_amplifier","tfa_h4_spartan_weapon_saw","Tfa_rebirth_ma5d","tfa_rebirth_m7d","tfa_rebirth_m392","tfa_rebirth_m90","tfa_rebirth_m6g","csgo_huntsman","weapon_rpw_binoculars","climb_swep2", "tfa_rebirth_ma5d"},
   command = "smmcmarines",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "HQ Command", 
PlayerSpawn = function(ply)
   ply:SetHealth(350)
   ply.HaloArmor=50
   ply:SetMaxHealth(350)
end
})
TEAM_REPORTER = DarkRP.createJob("UNSC Reporter", {
   color = Color(121, 23, 196, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_bella.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_snippy.mdl"},
   description = [[The UNSC Reporter is the person that is assigned to document the UNSC and their Allies and submit photos to higher and articles to highlights the efforts accomplished by the UNSC.]],
   weapons = {"Gmod_camera","Tfa_rebirth_ma5c","tfa_rebirth_m6c","climb_swep2"},
   command = "unscreporter",
   max = 3,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "UNSC Headquarters Element", 
PlayerSpawn = function(ply)
   ply.HaloArmor=25
end
})
TEAM_ALPHACOMM = DarkRP.createJob("Lima Company CO", {
   color = Color(167, 217, 62, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_snippy.mdl", "models/ishi/halo_rebirth/player/odst/female/odst_miia.mdl"},
   description = [[The Basic line infantry is responsible for most land-based military operations, as well as the protection of Naval vessels and installations from attack from enemy forces. The Basic Infantry Company is the backbone of the UNSC and they form the main body of any force that will be assigned to fight any battle.]],
   weapons = {"tfa_rebirth_ma5d","tfa_rebirth_br55","csgo_huntsman","tfa_rebirth_m6g","weapon_rpw_binoculars","climb_swep2","voice_amplifier","tfa_rebirth_m90","keys","swep_radiodevice"},
   command = "alphacocomm",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "HQ Command", 
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply.HaloArmor=50
   ply:SetMaxHealth(300)
end
})
TEAM_ALPHACOXO = DarkRP.createJob("Lima Company XO", {
   color = Color(167, 217, 62, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_snippy.mdl", "models/ishi/halo_rebirth/player/odst/female/odst_miia.mdl"},
   description = [[The Basic line infantry is responsible for most land-based military operations, as well as the protection of Naval vessels and installations from attack from enemy forces. The Basic Infantry Company is the backbone of the UNSC and they form the main body of any force that will be assigned to fight any battle.]],
   weapons = {"tfa_rebirth_ma5d","tfa_rebirth_br55","csgo_huntsman","tfa_rebirth_m6g","weapon_rpw_binoculars","keys","climb_swep2","voice_amplifier","tfa_rebirth_m90","swep_radiodevice"},
   command = "alphacoxo",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "HQ Command", 
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply.HaloArmor=50
   ply:SetMaxHealth(300)
end
})
TEAM_ALPHACOSGT = DarkRP.createJob("Lima Company 1st Sergeant", {
   color = Color(167, 217, 62, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_snippy.mdl", "models/ishi/halo_rebirth/player/odst/female/odst_miia.mdl"},
   description = [[The Basic line infantry is responsible for most land-based military operations, as well as the protection of Naval vessels and installations from attack from enemy forces. The Basic Infantry Company is the backbone of the UNSC and they form the main body of any force that will be assigned to fight any battle.]],
   weapons = {"tfa_rebirth_ma5d","tfa_rebirth_br55","csgo_huntsman","tfa_rebirth_m6g","weapon_rpw_binoculars","keys","climb_swep2","voice_amplifier","tfa_rebirth_m90","swep_radiodevice"},
   command = "alphacosgt",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "HQ Command", 
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply.HaloArmor=50
   ply:SetMaxHealth(300)
end
})
TEAM_LIMASARGT = DarkRP.createJob("Lima 1-1 Platoon Sergeant", {
   color = Color(4, 186, 16, 255),
   model = {"models/ishi/halo_rebirth/player/marines/male/marine_snippy.mdl"},
   description = [[A UNSC grunt force, Lima 1-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"tfa_rebirth_ma5d","tfa_rebirth_br55","csgo_huntsman","weapon_rpw_binoculars","weapon_rpw_binoculars","tfa_rebirth_m6g","keys","voice_amplifier","tfa_rebirth_m90","swep_radiodevice", "climb_swep2"},
   command = "limasquadpsgt",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Lima 1-1 Platoon",
PlayerSpawn = function(ply)
   ply:SetHealth(175)
   ply.HaloArmor=45
   ply:SetMaxHealth(175)
end
})
TEAM_LIMASQUADLEAD = DarkRP.createJob("Lima 1-1 Squad Leader", {
   color = Color(4, 186, 16, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_bella.mdl","models/ishi/halo_rebirth/player/marines/male/marine_jeffrey.mdl"},
   description = [[A UNSC grunt force, Lima 1-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"tfa_rebirth_ma5d","tfa_rebirth_br55","csgo_huntsman","tfa_rebirth_m6g","weapon_rpw_binoculars","keys","voice_amplifier","swep_radiodevice"},
   command = "lsquadleader",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Lima 1-1 Platoon",
PlayerSpawn = function(ply)
   ply:SetHealth(150)
   ply.HaloArmor=45
   ply:SetMaxHealth(150)
end
})
TEAM_VSARGT = DarkRP.createJob("Lima 2-1 Platoon Sergeant", {
   color = Color(0, 107, 31, 255),
   model = {"models/ishi/halo_rebirth/player/marines/male/marine_snippy.mdl"},
   description = [[A UNSC grunt force, Lima 2-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"tfa_rebirth_ma5d","tfa_rebirth_br55","csgo_huntsman","weapon_rpw_binoculars","tfa_rebirth_m6g","keys","voice_amplifier","tfa_rebirth_m90","swep_radiodevice","tfa_rebirth_m394","climb_swep2"},
   command = "vsquadpsgt",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Lima 2-1 Platoon",
PlayerSpawn = function(ply)
   ply:SetHealth(175)
   ply.HaloArmor=45
   ply:SetMaxHealth(175)
end
})
TEAM_KSQUADLEAD = DarkRP.createJob("Kilo Squad Leader", {
   color = Color(218, 242, 67, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_bella.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_jeffrey.mdl"},
   description = [[A UNSC grunt force, Kilo 2-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"Voice_amplifier","tfa_rebirth_m392","Tfa_rebirth_ma5c","tfa_rebirth_m7d","tfa_rebirth_m45e","tfa_rebirth_m6g","csgo_huntsman","weapon_rpw_binoculars","climb_swep2"},
   command = "ksquadleader",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Kilo",
PlayerSpawn = function(ply)
   ply:SetHealth(150)
   ply.HaloArmor=45
   ply:SetMaxHealth(150)
end
})
TEAM_KSARGT = DarkRP.createJob("Kilo Platoon Sergeant", {
   color = Color(218, 242, 67, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_bella.mdl"},
   description = [[A UNSC grunt force, Kilo 2-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"Voice_amplifier","tfa_rebirth_m392","Tfa_rebirth_ma5c","tfa_rebirth_m7d","tfa_rebirth_m45e","tfa_rebirth_m6g","csgo_huntsman","weapon_rpw_binoculars","climb_swep2"},
   command = "ksquadpsgt",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Kilo",
PlayerSpawn = function(ply)
   ply:SetHealth(150)
   ply.HaloArmor=45
   ply:SetMaxHealth(150)
end
})
TEAM_KSQUADPLEAD = DarkRP.createJob("Kilo Platoon Leader", {
   color = Color(218, 242, 67, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_bella.mdl"},
   description = [[A UNSC grunt force, Kilo 2-1 was one of the many frontline infantry during the Human-Covenant war.]],
   weapons = {"Voice_amplifier","tfa_rebirth_m392","Tfa_rebirth_ma5c","tfa_rebirth_m7d","tfa_rebirth_m45e","tfa_rebirth_m6g","csgo_huntsman","weapon_rpw_binoculars","climb_swep2"},
   command = "ksquadpleader",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Kilo",
PlayerSpawn = function(ply)
   ply:SetHealth(150)
   ply.HaloArmor=45
   ply:SetMaxHealth(150)
end
})
TEAM_DOGMAOFFLEAD = DarkRP.createJob("Cerberus Spec OPS Officer Lead", {
   color = Color(62, 44, 90, 255),
   model = {"models/chaosnarwhal/unscab_danlin_base2.mdl", "models/chaosnarwhal/marine_miia_base2.mdl"},
   description = [[The Cerberus Jump Squad is the UNSC’s most mobile and agile unit. Utilizing Jet Packs they are able to get in and out of Hot Zones with ease. Cerberus Prides themselves on their ability to operate both on the ground as well as in Zero Gravity. Nothing is out of reach when these troopers strap on their packs and que up for take off.]],
   weapons = {"Voice_amplifier","revival_c4","med_kit","darkrp_defibrilator","weapon_sh_flashbang","weapon_cuff_mp","tfa_rebirth_srs99c2s","tfa_rebirth_ma37s","tfa_rebirth_m7ds","tfa_rebirth_m394b","tfa_rebirth_m45e","tfa_rebirth_m6cs","csgo_huntsman","weapon_rpw_binoculars","climb_swep2"},
   command = "dogmaofflead",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Cerberus",
PlayerSpawn = function(ply)
   ply:SetHealth(200)
   ply.HaloArmor=50
   ply:SetArmor(150)
   ply:SetMaxHealth(200)
end
})
TEAM_DOGMAWARLEAD = DarkRP.createJob("Cerberus Spec Ops Warrant Lead", {
   color = Color(62, 44, 90, 255),
   model = {"models/chaosnarwhal/unscab_oni_outlander_base2.mdl", "models/chaosnarwhal/marine_miia_base2.mdl"},
   description = [[The Cerberus Jump Squad is the UNSC’s most mobile and agile unit. Utilizing Jet Packs they are able to get in and out of Hot Zones with ease. Cerberus Prides themselves on their ability to operate both on the ground as well as in Zero Gravity. Nothing is out of reach when these troopers strap on their packs and que up for take off.]],
   weapons = {"Voice_amplifier","revival_c4","med_kit","darkrp_defibrilator","weapon_sh_flashbang","weapon_cuff_mp","tfa_rebirth_srs99c2s","tfa_rebirth_ma37s","tfa_rebirth_m7ds","tfa_rebirth_m394b","tfa_rebirth_m45e","tfa_rebirth_m6cs","csgo_huntsman","weapon_rpw_binoculars","climb_swep2"},
   command = "dogmawarlead",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Cerberus",
PlayerSpawn = function(ply)
   ply:SetHealth(200)
   ply.HaloArmor=50
   ply:SetArmor(150)
   ply:SetMaxHealth(200)
end
})
TEAM_DOGMAJTROOPC = DarkRP.createJob("Cerberus Spec OPS", {
   color = Color(62, 44, 90, 255),
   model = {"models/chaosnarwhal/unscab_oni_outlander_base2.mdl", "models/chaosnarwhal/marine_miia_base2.mdl"},
   description = [[The Cerberus Jump Squad is the UNSC’s most mobile and agile unit. Utilizing Jet Packs they are able to get in and out of Hot Zones with ease. Cerberus Prides themselves on their ability to operate both on the ground as well as in Zero Gravity. Nothing is out of reach when these troopers strap on their packs and que up for take off.]],
   weapons = {"tfa_rebirth_ma37s","revival_c4","med_kit","darkrp_defibrilator","tfa_rebirth_srs99c2s","tfa_rebirth_m7ds","tfa_rebirth_m394b","tfa_rebirth_m45e","tfa_rebirth_m6cs","csgo_huntsman","climb_swep2"},
   command = "dogmajumptroop",
   max = 6,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Cerberus",
PlayerSpawn = function(ply)
   ply:SetHealth(200)
   ply.HaloArmor=50
   ply:SetArmor(150)
   ply:SetMaxHealth(200)
end
})
TEAM_GOLFEODOFFLEAD = DarkRP.createJob("Lima 1-6 Golf Actual", {
   color = Color(161, 28, 28, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_bella.mdl","models/ishi/halo_rebirth/player/marines/male/marine_jeffrey.mdl"},
   description = [[The detection, identification, on-site evaluation, rendering safe, recovery, and final disposal of unexploded explosive ordnance, this is the task of the Golf EOD Squad. These Troopers are tasked with the job of defusing life threatening situations that find their way in the way of the UNSC. The boy on the station call them IED finders and with that they protect the men and women that serve from the things that go hidden from the naked eye. Along with disposal Golf is tasked with ordinance so you know that when you are pinned down and need aid Golf will bring hell to the battlefield.]],
   weapons = {"voice_amplifier","tfa_rebirth_m41","tfa_rebirth_ma5d","tfa_rebirth_m7d","tfa_rebirth_m392","tfa_rebirth_m45e","tfa_rebirth_m6g","csgo_huntsman","weapon_sh_doorcharge","halo_frag","weapon_rpw_binoculars","climb_swep2", "revival_c4"},
   command = "golfeodofflead",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Lima 1-1 Platoon",
PlayerSpawn = function(ply)
   ply:SetHealth(325)
   ply.HaloArmor=48
   ply:SetMaxHealth(325)
   ply:GiveAmmo(10, "grenade")
end
})
TEAM_GOLFWARRLEAD = DarkRP.createJob("Lima 1-6 Golf Instructor", {
   color = Color(161, 28, 28, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_bella.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_jeffrey.mdl"},
   description = [[The detection, identification, on-site evaluation, rendering safe, recovery, and final disposal of unexploded explosive ordnance, this is the task of the Golf EOD Squad. These Troopers are tasked with the job of defusing life threatening situations that find their way in the way of the UNSC. The boy on the station call them IED finders and with that they protect the men and women that serve from the things that go hidden from the naked eye. Along with disposal Golf is tasked with ordinance so you know that when you are pinned down and need aid Golf will bring hell to the battlefield.]],
   weapons = {"Voice_amplifier","tfa_rebirth_m41","tfa_rebirth_ma5d","tfa_rebirth_m7d","tfa_rebirth_m392","tfa_rebirth_m45e","tfa_rebirth_m6g","csgo_huntsman","weapon_sh_doorcharge","Halo_frag","weapon_rpw_binoculars","climb_swep2","revival_c4"},
   command = "golfwarlead",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Lima 1-1 Platoon",
PlayerSpawn = function(ply)
   ply:SetHealth(325)
   ply.HaloArmor=48
   ply:SetMaxHealth(325)
   ply:GiveAmmo(10, "grenade")
end
})
TEAM_GOLFEODTROOP = DarkRP.createJob("Lima 1-6 EOD Specialist", {
   color = Color(161, 28, 28, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_bella.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_jeffrey.mdl"},
   description = [[The detection, identification, on-site evaluation, rendering safe, recovery, and final disposal of unexploded explosive ordnance, this is the task of the Golf EOD Squad. These Troopers are tasked with the job of defusing life threatening situations that find their way in the way of the UNSC. The boy on the station call them IED finders and with that they protect the men and women that serve from the things that go hidden from the naked eye. Along with disposal Golf is tasked with ordinance so you know that when you are pinned down and need aid Golf will bring hell to the battlefield.]],
   weapons = {"Voice_amplifier","tfa_rebirth_m41","tfa_rebirth_ma5d","tfa_rebirth_m7d","tfa_rebirth_m392","tfa_rebirth_m45e","tfa_rebirth_m6g","csgo_huntsman","weapon_sh_doorcharge","Halo_frag","weapon_rpw_binoculars","climb_swep2","revival_c4"},
   command = "golfeodtroop",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Lima 1-1 Platoon",
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply.HaloArmor=48
   ply:SetMaxHealth(300)
   ply:GiveAmmo(10, "grenade")
end
})
TEAM_RHINOOFFLEAD = DarkRP.createJob("Lima 3-6 Rhino HazOp", {
   color = Color(204, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_bella.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_jeffrey.mdl"},
   description = [[Rhino Heavy Squad is the support squad that is quick to place effect fire on any objective that is in there way. Suppression is there priority, filling the bodies of their enemies with holes is their joy. If you ever need a quick response unit that specializes in pulling you and your men out of a tough position, you know who call. These tough beast will charge threw hell and high water to make sure that their mission is accomplished.]],
   weapons = {"voice_amplifier","tfa_m247_gpmg","tfa_h4_spartan_weapon_saw","tfa_rebirth_m392","tfa_rebirth_m7ds","tfa_rebirth_m6g","csgo_huntsman","halo_frag","weapon_rpw_binoculars","climb_swep2","halo_flamethrower","tfa_rebirth_m90","weapon_policeshield", "halo_tfa_assaultrifle", "weapon_bactainjector", "darkrp_defibrillator"}, -- Edit done by Head General 4/14 (Added new LMG)
   command = "rhinoofflead",
   max = 3,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Lima 2-1 Platoon",
PlayerSpawn = function(ply)
   ply:SetHealth(325)
   ply.HaloArmor=48
   ply:SetMaxHealth(325)
   ply:GiveAmmo(10, "grenade")
end
})
TEAM_RHINOWARRLEAD = DarkRP.createJob("Lima 3-6 Rhino Fireteam Leader", {
   color = Color(204, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_bella.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_jeffrey.mdl"},
   description = [[Rhino Heavy Squad is the support squad that is quick to place effect fire on any objective that is in there way. Suppression is there priority, filling the bodies of their enemies with holes is their joy. If you ever need a quick response unit that specializes in pulling you and your men out of a tough position, you know who call. These tough beast will charge threw hell and high water to make sure that their mission is accomplished.]],
   weapons = {"voice_amplifier","tfa_m247_gpmg","tfa_h4_spartan_weapon_saw","tfa_rebirth_m392","tfa_rebirth_m7ds","tfa_rebirth_m6g","csgo_huntsman","halo_frag","weapon_rpw_binoculars","climb_swep2","tfa_rebirth_m90","weapon_policeshield", "halo_tfa_assaultrifle", "weapon_bactainjector", "darkrp_defibrillator", "tfa_rebirth_srs99s2am"}, -- Edit done by Head General 4/14 (Added new LMG)
   command = "rhinowarrlead",
   max = 3,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Lima 2-1 Platoon",
PlayerSpawn = function(ply)
   ply:SetHealth(325)
   ply.HaloArmor=45
   ply:SetMaxHealth(325)
   ply:GiveAmmo(10, "grenade")
end
})
TEAM_RHINOTROOPER = DarkRP.createJob("Lima 3-6 Rhino Commando", {
   color = Color(204, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_bella.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_jeffrey.mdl"},
   description = [[Rhino Heavy Squad is the support squad that is quick to place effect fire on any objective that is in there way. Suppression is there priority, filling the bodies of their enemies with holes is their joy. If you ever need a quick response unit that specializes in pulling you and your men out of a tough position, you know who call. These tough beast will charge threw hell and high water to make sure that their mission is accomplished.]],
   weapons = {"tfa_h4_spartan_weapon_saw","tfa_m247_gpmg","tfa_rebirth_m392","tfa_rebirth_m6g","csgo_huntsman","halo_frag","climb_swep2","tfa_rebirth_m90", "weapon_policeshield", "tfa_rebirth_m7ds", "halo_tfa_assaultrifle", "tfa_rebirth_srs99s2am", "weapon_bactainjector", "med_kit", "darkrp_defibrillator"}, -- Edit done by Head General 4/14 (Added new LMG)
   command = "rhinotrooper",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Lima 2-1 Platoon",
PlayerSpawn = function(ply)
   ply:SetHealth(325)
   ply.HaloArmor=48
   ply:SetMaxHealth(325)
   ply:GiveAmmo(10, "grenade")
end
})
TEAM_WHISKEYOFFLEAD = DarkRP.createJob("Lima 3-1 Whiskey Actual", {
   color = Color(81, 65, 44, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_bella.mdl", "models/ishi/halo_rebirth/player/marines/male/marine_jeffrey.mdl"},
   description = [[The Whiskey Spec Ops Squad is on of the most elite units that falls under the UNSC. Whiskey’s primary task is classified as Covert Operations that pit the best men and women that the UNSC has to offer against any foreign assalant that faces the UNSC. These Troopers will eliminate it at all cost and you will never see them coming. csgo_huntsman in the Wind as it were.]],
   weapons = {"voice_amplifier","tfa_m247_gpmg","tfa_rebirth_m90","halo_tfa_assaultrifle","halo_frag","tfa_rebirth_m392","tfa_rebirth_m7ds","tfa_rebirth_m6g","Climb_swep2"}, -- Edit Done by Head General 4/14 (Added new LMG)
   command = "whiskeyofflead",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Lima 3-1 Platoon",
PlayerSpawn = function(ply)
   ply:SetHealth(275)
   ply.HaloArmor=48
   ply:SetMaxHealth(275)
   ply:GiveAmmo(5, "grenade")
end
})
TEAM_WHISKEYWARRLEAD = DarkRP.createJob("Lima 3-1 Whiskey Instructor", {
   color = Color(81, 65, 44, 255),
   model = {"models/ishi/halo_rebirth/player/marines/female/marine_bella.mdl", "models/ishi/halo_rebirth/player/marines/female/marine_dominique.mdl"},
   description = [[The Whiskey Spec Ops Squad is on of the most elite units that falls under the UNSC. Whiskey’s primary task is classified as Covert Operations that pit the best men and women that the UNSC has to offer against any foreign assalant that faces the UNSC. These Troopers will eliminate it at all cost and you will never see them coming. csgo_huntsman in the Wind as it were.]],
   weapons = {"Voice_amplifier","tfa_m247_gpmg","tfa_h4_spartan_weapon_saw","tfa_rebirth_m392","tfa_rebirth_m7ds","tfa_rebirth_m6g","tfa_rebirth_m90","halo_tfa_assaultrifle","halo_frag","Climb_swep2"}, -- Edit Done by Head General 4/14 (Added new LMG)
   command = "whiskeywarrlead",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Lima 3-1 Platoon",
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply.HaloArmor=48
   ply:SetMaxHealth(250)
   ply:GiveAmmo(5, "grenade")
end
})
TEAM_SOSRGRUNT = DarkRP.createJob("SoS Ranger Grunt", {
   color = Color(127, 0, 255, 255),
   model = {"models/jds/characters/unggoy.mdl"},
   description = [[The Swords of Sanghelios are a fractured group of the Covenant that have joined in alliance with the USNC to stop the Great Journey]],
   weapons = {"Climb_swep2","Keys","tfa_hr_swep_plasma_rifle","tfa_hr_swep_needler","weapon_plasmanade","swep_radiodevice"},
   command = "sosrgrunt",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Swords of Sanghelios",
PlayerSpawn = function(ply)
   ply:SetHealth(200)
   ply.HaloArmor=15
   ply:SetMaxHealth(200)
end
})
TEAM_SOSSPECOPSS = DarkRP.createJob("SoS Spec Ops Skirmisher", {
   color = Color(127, 0, 255, 255),
   model = {"models/player/skirmisher.mdl"},
   description = [[The Swords of Sanghelios are a fractured group of the Covenant that have joined in alliance with the USNC to stop the Great Journey]],
   weapons = {"Climb_swep2","Keys","h3_beam_rifle_tfa","tfa_hr_swep_needler","invisibility_cloak","weapon_plasmanade","swep_radiodevice", "tfa_h2_covenant_carbine", "halo3_dualplasmapistol", "tfa_hr_swep_needle_rifle"},
   command = "sossoprskirm",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Swords of Sanghelios",
PlayerSpawn = function(ply)
   ply:SetHealth(200)
   ply.HaloArmor=30
   ply:SetMaxHealth(200)
   ply:SetRunSpeed(350)
   ply:SetWalkSpeed(225)
end
})
TEAM_SOSOPGRUNT = DarkRP.createJob("SoS Spec Ops Grunt", {
   color = Color(127, 0, 255, 255),
   model = {"models/jds_02/characters/unggoy02.mdl"},
   description = [[The Swords of Sanghelios are a fractured group of the Covenant that have joined in alliance with the USNC to stop the Great Journey]],
   weapons = {"Climb_swep2","Keys","tfa_hr_swep_plasma_rifle","tfa_hr_swep_needler","weapon_plasmanade","invisibility_cloak","swep_radiodevice"},
   command = "sosopgrunt",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Swords of Sanghelios",
PlayerSpawn = function(ply)
   ply:SetHealth(225)
   ply.HaloArmor=30
   ply:SetMaxHealth(225)
end
})
TEAM_SOSRJACK = DarkRP.createJob("SoS Ranger Jackal", {
   color = Color(127, 0, 255, 255),
   model = {"models/player/jackal.mdl"},
   description = [[The Swords of Sanghelios are a fractured group of the Covenant that have joined in alliance with the USNC to stop the Great Journey]],
   weapons = {"Climb_swep2","Keys","h3_beam_rifle_tfa","halo3_dualplasmapistol","weapon_plasmanade","swep_radiodevice", "tfa_h2_covenant_carbine", "tfa_hr_swep_needler"},
   command = "sosrjack",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Swords of Sanghelios",
PlayerSpawn = function(ply)
   ply:SetHealth(175)
   ply.HaloArmor=30
   ply:SetMaxHealth(175)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(200)
end
})
TEAM_SOSMEDICSANG = DarkRP.createJob("SoS Elite Medic", {
   color = Color(127, 0, 255, 255),
   model = {"models/player/minor_armour.mdl"},
   description = [[The Swords of Sanghelios are a fractured group of the Covenant that have joined in alliance with the UNSC to stop the Great Journey]],
   weapons = {"h3_esword_tfa", "Tfa_hr_swep_needler","H3_beam_rifle_tfa", "Weapon_plasmanade", "tfa_hr_swep_concussion_rifle", "tfa_hr_swep_plasma_rifle","h3_esword_tfa","tfa_h2_covenant_carbine","climb_swep2","Weapon_bactainjector","Weapon_bactanade", "darkrp_defibrillator"},
   command = "sossangmed",
   max = 5,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Swords of Sanghelios",
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply:SetArmor(350)
   ply.HaloArmor=76
   ply:SetMaxHealth(400)
   ply:SetRunSpeed(270)
   ply:SetWalkSpeed(170)
end
})
TEAM_NAVALOPPERSONNEL = DarkRP.createJob("Naval Operations Personnel", {
   color = Color(0, 0, 0, 255),
   model = {"models/gonzo/unscofficers/dress/dress.mdl", "models/gonzo/unscofficers/femalekeyes/femalekeyes.mdl"},
    description = [[Operations deals with navigation, Weaponry and act as command of the bridge. Each Naval Officer will have a certain job to do such as Navigation (NAV), Weaponry (WEP). As a Crewman (CN) and up you are able to gun a Frigate during events and mini events on station to help protect the UNSC Revival or to help the UNSC overall during offensive or defensive missions. You will be able to pilot a frigate at a rank of Petty Officer 3 (PO3). If you are a Petty Officer 1 (PO1) and above you will have access to the MAC cannon during off station and on station events.]],
   weapons = {"tfa_rebirth_m6c"},
   command = "navaloppersonnel",
   max = 0,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Naval Operations",
PlayerSpawn = function (ply)
   ply:SetHealth(100)
   ply:SetMaxHealth(100)
 end
})
TEAM_NAVALOPOFFICER = DarkRP.createJob("Naval Operations Officer", {
   color = Color(0, 0, 0, 255),
   model = {"models/gonzo/unscofficers/delrio/delrio.mdl","models/gonzo/unscofficers/femalekeyes/femalekeyes.mdl"},
   description = [[Operations deals with navigation, Weaponry and act as command of the bridge. Each Naval Officer will have a certain job to do such as Navigation (NAV), Weaponry (WEP). As a Crewman (CN) and up you are able to gun a Frigate during events and mini events on station to help protect the UNSC Revival or to help the UNSC overall during offensive or defensive missions. You will be able to pilot a frigate at a rank of Petty Officer 3 (PO3). If you are a Petty Officer 1 (PO1) and above you will have access to the MAC cannon during off station and on station events.]],
   weapons = {"tfa_rebirth_m6c"},
   command = "navalopofficer",
   max = 0,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Naval Operations",
PlayerSpawn = function (ply)
   ply:SetHealth(100)
   ply:SetMaxHealth(100)
 end
})
TEAM_NAVALSTG = DarkRP.createJob("Naval Special Task Group", {
   color = Color(0, 0, 0, 255),
   model = {"models/gonzo/unscofficers/lasky/lasky.mdl", "models/gonzo/deviliousmarines/snake/snakem.mdl"},
   description = [[The Naval Special Task group is assigned to gather information, conduct elite operations, and handle any other order issued by the Naval Captain. They carry a class 3 security clearance.]],
   weapons = {"tfa_rebirth_xbr55s", "climb_swep2", "tfa_rebirth_srs99s2am", "tfa_rebirth_m7ds", "tfa_rebirth_m7d","csgo_huntsman", "tfa_rebirth_ma5d", "tfa_rebirth_m45e"},
   command = "navalstg",
   max = 15,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Naval STG",
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply.HaloArmor=25
   ply:SetArmor(150)
   ply:SetMaxHealth(250)
end
})
TEAM_NAVALLASTCH = DarkRP.createJob("Naval Last Chancers", {
   color = Color(0, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/marines/male/marine_eyecberg.mdl","models/ishi/halo_rebirth/player/marines/female/marine_snowy.mdl"},
   description = [[NULL DATA]],
   weapons = {"climb_swep2","tfa_rebirth_m394","Tfa_rebirth_srs99s2am","tfa_rebirth_m6g","tfa_rebirth_m90","tfa_rebirth_ma37", "tfa_rebirth_br55","tfa_h4_spartan_weapon_saw"},
   command = "navalchancer",
   max = 15,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Naval STG",
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply.HaloArmor=55
   ply:SetMaxHealth(400)
   ply:SetRunSpeed(250)
end
})
TEAM_NAVALSTGRANDDHEAD = DarkRP.createJob("Naval Head Of R&D", {
   color = Color(0, 0, 0, 255),
   model = {"models/gonzo/unscofficers/dress/dress.mdl"},
    description = [[The Special Task Group's Resarch and Devolpment department are incharge of processing and transfering all intellgince and reasarch gathered by the STG.]],
   weapons = {"tfa_rebirth_m6c"},
   command = "navalstgrndd",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Naval R&D",
PlayerSpawn = function (ply)
   ply:SetHealth(100)
   ply:SetMaxHealth(100)
 end
})
TEAM_NAVALSTGRANDD = DarkRP.createJob("Naval Research and Development", {
   color = Color(0, 0, 0, 255),
   model = {"models/gonzo/unscofficers/femalekeyes/femalekeyes.mdl", "models/gonzo/unscofficers/lasky/lasky.mdl"},
    description = [[The Special Task Group's Resarch and Devolpment department are incharge of processing and transfering all intellgince and reasarch gathered by the STG.]],
   weapons = {"tfa_rebirth_m6c"},
   command = "navalstgr&d",
   max = 8,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Naval R&D",
PlayerSpawn = function (ply)
   ply:SetHealth(100)
   ply:SetMaxHealth(100)
 end
})
TEAM_NAVALHEADOP = DarkRP.createJob("Naval Head of Operations", {
   color = Color(0, 0, 0, 255),
   model = {"models/gonzo/unscofficers/dress/dress.mdl"},
    description = [[ The head of operations deals with everything to do with operations of the station, frigates and guns. They can give out any order within the sector and have to be followed unless told by the UNSC Naval Command. You will have permission to promote people within your roster of operations and will have to carefully manage documents within google drive.]],
   weapons = {"tfa_rebirth_m6c"},
   command = "navalheadop",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Naval Heads of Department",
PlayerSpawn = function (ply)
   ply:SetHealth(100)
   ply:SetMaxHealth(100)
 end
})
TEAM_NAVALHEADSTG = DarkRP.createJob("Naval Head of the Special Task Group", {
   color = Color(0, 0, 0, 255),
   model = {"models/gonzo/deviliousmarines/snake/snakem.mdl", "models/gonzo/unscofficers/dress/dress.mdl"},
    description = [[You are the Head of the Naval Special Task Group]],
   weapons = {"tfa_rebirth_m6c", "tfa_rebirth_m6cp"},
   command = "navalheadstg",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Naval Heads of Department",
PlayerSpawn = function (ply)
   ply:SetHealth(100)
   ply:SetMaxHealth(100)
 end
})
TEAM_NAVALCOMMANDOFFICER = DarkRP.createJob("Naval Commanding Officer", {
   color = Color(0, 0, 0, 255),
   model = {"models/gonzo/unscofficers/femalekeyes/femalekeyes.mdl", "models/gonzo/unscofficers/dress/dress.mdl"},
    description = [[You are apart of Naval Command.]],
   weapons = {"voice_amplifier", "tfa_rebirth_m6c", "weapon_cuff_mp"},
   command = "navalcommander",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Naval Command",
PlayerSpawn = function (ply)
   ply:SetHealth(100)
   ply:SetMaxHealth(100)
 end
})
TEAM_CHAOS = DarkRP.createJob("RnD Specialist Chaotic", {
   color = Color(100, 100, 100, 255),
   model = {"models/lt_c/sci_fi/humans/male_09.mdl"},
   description = [[The Research and Development Specialist is a role within the UNSC Engineering Corps tasked with the research and development of technologies potentially beneficial to the UNSC and its allies.]],
   weapons = {"tfa_rebirth_srs99","tfa_rebirth_m7ds"},
   command = "rndchaos",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "UNSC Central Fleet",
PlayerSpawn = function (ply)
   ply:SetHealth(250)
   ply:SetMaxHealth(250)
   ply:SetArmor(100)
 end
})
TEAM_PARARESCU = DarkRP.createJob("UNSCAF Pararescuemen", {
   color = Color(168, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_heretic.mdl", "models/ishi/halo_rebirth/player/odst/female/odst_miia.mdl"},
   description = [[PJ Operators tasked with recovery and medical treatment of personnel in humanitarian and combat environments.]],
   weapons = {"weapon_bactainjector","tfa_rebirth_m392","tfa_rebirth_m7d","tfa_rebirth_m6g","csgo_huntsman","climb_swep2","tfa_rebirth_m90","Tfa_rebirth_m7d","Halo_frag","darkrp_defibrillator","med_kit"},
   command = "pararesss",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Pararescue",
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply.HaloArmor=45
   ply:SetMaxHealth(300)
   ply:GiveAmmo(10, "grenade")
end
})
TEAM_PARARESCULEAD = DarkRP.createJob("PJ Chief Rescue Officer", {
   color = Color(168, 0, 0, 255),
   model = {"models/ishi/halo_rebirth/player/odst/female/odst_miia.mdl","models/ishi/halo_rebirth/player/odst/male/odst_heretic.mdl"},
   description = [[PJ Operators tasked with recovery and medical treatment of personnel in humanitarian and combat environments.]],
   weapons = {"weapon_bactainjector","tfa_rebirth_m392","tfa_rebirth_m7d","tfa_rebirth_m6g","csgo_huntsman","climb_swep2","Tfa_rebirth_m7d","Halo_frag","tfa_rebirth_m41","tfa_rebirth_m45e","darkrp_defibrillator","med_kit"},
   command = "pararessslead",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Pararescue",
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply.HaloArmor=45
   ply:SetMaxHealth(300)
   ply:GiveAmmo(10, "grenade")
end
})
TEAM_HELLSQL = DarkRP.createJob("Hellfire Squad Leader", {
   color = Color(112, 0, 0, 255),
   model = {"models/gonzo/odstheads/femaleodst/femaleodst.mdl", "models/gonzo/odstheads/maleodst/maleodst.mdl"},
   description =  [[Hellfire Squad Lead.]] ,
   weapons = {"tfa_doom_gauss", "deika_plasmarifle_tfa", "tfa_ishi_ma37", "tfa_hr_spartan_laser", "tfa_rebirth_m6cs", "tfa_ins2_codol_free", "tfa_rec_hmini", "tfa_rebirth_m45e", "tfa_wtno_mark1960", "invisibility_cloak", "stungun", "weapon_cuff_mp", "Climb_swep2", "Weapon_sh_flashbang", "Weapon_slam", "Halo_frag", "bw2_blauncher", "csgo_huntsman"},
   command = "hellfsql",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Hellfire",
PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply.HaloArmor=30
   ply:SetArmor(400)
   ply:SetMaxHealth(600)
end
})
TEAM_HELLASLT = DarkRP.createJob("Hellfire Squad Assault", {
   color = Color(112, 0, 0, 255),
   model = {"models/gonzo/odstheads/femaleodst/femaleodst.mdl", "models/gonzo/odstheads/maleodst/maleodst.mdl"},
   description =  [[Hellfire Squad Assault.]] ,
   weapons = {"deika_plasmarifle_tfa", "climb_swep2", "tfa_hr_spartan_laser", "tfa_ishi_ma37", "tfa_rebirth_m6cs", "halo_flamethrower", "Weapon_sh_flashbang", "bw2_blauncher", "Weapon_slam", "Halo_frag", "csgo_huntsman"},
   command = "hellfasl",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Hellfire",
PlayerSpawn = function(ply)
   ply:SetHealth(500)
   ply.HaloArmor=30
   ply:SetArmor(350)
   ply:SetMaxHealth(500)
end
})
TEAM_HELLSPEC = DarkRP.createJob("Hellfire Squad Specialist", {
   color = Color(112, 0, 0, 255),
   model = {"models/gonzo/odstheads/femaleodst/femaleodst.mdl", "models/gonzo/odstheads/maleodst/maleodst.mdl"},
   description =  [[Hellfire Squad Specialist.]] ,
   weapons = {"deika_plasmarifle_tfa", "tfa_hr_spartan_laser", "tfa_ishi_ma37", "tfa_rebirth_m6cs", "tfa_rebirth_m45e", "tfa_wtno_mark1960", "weapon_policeshield", "weapon_cuff_mp", "stungun", "Climb_swep2", "invisibility_cloak", "bw2_blauncher", "csgo_huntsman"},
   command = "hellfspec",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Hellfire",
PlayerSpawn = function(ply)
   ply:SetHealth(550)
   ply.HaloArmor=30
   ply:SetArmor(400)
   ply:SetMaxHealth(550)
end
})
TEAM_HELLHEV = DarkRP.createJob("Hellfire Squad Heavy", {
   color = Color(112, 0, 0, 255),
   model = {"models/gonzo/odstheads/femaleodst/femaleodst.mdl", "models/gonzo/odstheads/maleodst/maleodst.mdl"},
   description =  [[Hellfire Squad Heavy.]],
   weapons = {"deika_plasmarifle_tfa", "tfa_hr_spartan_laser", "tfa_ishi_ma37", "tfa_rebirth_m6cs", "tfa_rec_hmini", "tfa_h4_spartan_weapon_saw", "bw2_blauncher", "halo_frag", "weapon_policeshield", "Climb_swep2", "Revival_c4", "csgo_huntsman"},
   command = "hellfhev",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Hellfire",
PlayerSpawn = function(ply)
   ply:SetHealth(550)
   ply.HaloArmor=30
   ply:SetArmor(500)
   ply:SetMaxHealth(550)
end
})
TEAM_HELLSNIP = DarkRP.createJob("Hellfire Squad Sniper", {
   color = Color(112, 0, 0, 255),
   model = {"models/gonzo/odstheads/femaleodst/femaleodst.mdl", "models/gonzo/odstheads/maleodst/maleodst.mdl"},
   description =  [[Hellfire Squad Sniper.]],
   weapons = {"tfa_hr_spartan_laser", "tfa_ins2_codol_free", "tfa_ishi_ma37", "tfa_rebirth_m6cs", "tfa_ins2_codol_free", "tfa_rebirth_srs99c2s", "tfa_rebirth_m394b", "Climb_swep2", "invisibility_cloak", "bw2_blauncher", "halo_frag", "csgo_huntsman"},
   command = "hellfsnip",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Hellfire",
PlayerSpawn = function(ply)
   ply:SetHealth(350)
   ply.HaloArmor=30
   ply:SetArmor(300)
   ply:SetMaxHealth(200)
end
})
TEAM_URFSQL = DarkRP.createJob("URF Squad Leader", {
   color = Color(32, 32, 32, 255),
   model = {"models/ishi/suno/halo_rebirth/player/innie/male/innie_ray.mdl", "models/ishi/suno/halo_rebirth/player/innie/female/innie_dominique.mdl"},   
   description = [[URF Squad Leader]],
   weapons = {"tfa_doom_gauss", "deika_plasmarifle_tfa", "cw_ws_scifi_aug", "revival_c4", "tfa_ishi_m7", "csgo_huntsman", "tfa_rebirth_m394", "tfa_wtno_mark1960", "climb_swep2", "weapon_sh_doorcharge", "weapon_sh_flashbang", "m9k_sticky_grenade", "m9k_nitro", "grapplehookv2", "mp_weapon_mgl", "weapon_slam", "halo_frag", "m9k_orbital_strike", "tfa_ishi_ma37", "tfa_rebirth_m45e", "tfa_h4_spartan_weapon_saw", " tfa_rebirth_m6g"},
   command = "URFSQL",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Insurrection",
PlayerSpawn = function(ply)
   ply:SetHealth(500)
   ply.HaloArmor=40
   ply:SetMaxHealth(500)
   ply:SetArmor(400)
end
})
TEAM_URFASLT = DarkRP.createJob("URF Squad Assault", {
   color = Color(32, 32, 32, 255),
   model = {"models/ishi/suno/halo_rebirth/player/innie/male/innie_ray.mdl", "models/ishi/suno/halo_rebirth/player/innie/female/innie_dominique.mdl"},   
   description = [[URF Squad Assault]],
   weapons = {"tfa_doom_gauss", "cw_ws_scifi_aug", "tfa_rebirth_m6g", "tfa_ishi_ma37", "tfa_ishi_m7", "climb_swep2", "weapon_sh_doorcharge", "weapon_sh_flashbang", "m9k_sticky_grenade", "m9k_nitro", "weapon_slam", "halo_frag", "tfa_rebirth_m394", "csgo_huntsman"},
   command = "URFASLT",
   max = 15,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Insurrection",
PlayerSpawn = function(ply)
   ply:SetHealth(450)
   ply.HaloArmor=40
   ply:SetMaxHealth(450)
   ply:SetArmor(400)
end
})
TEAM_URFSPEC = DarkRP.createJob("URF Squad Specialist", {
   color = Color(32, 32, 32, 255),
   model = {"models/ishi/suno/halo_rebirth/player/innie/male/innie_ray.mdl", "models/ishi/suno/halo_rebirth/player/innie/female/innie_dominique.mdl"},   
   description = [[URF Squad Specialist]],
   weapons = {"deika_plasmarifle_tfa", "cw_ws_scifi_aug", "tfa_rebirth_m45e", "tfa_ishi_ma37", "tfa_wtno_mark1960", "climb_swep2", "invisibility_cloak", " tfa_rebirth_m6g", "m9k_nitro", "tfa_rebirth_m394", "revival_c4", "halo_frag", "csgo_huntsman"},
   command = "URFSPEC",
   max = 15,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Insurrection",
PlayerSpawn = function(ply)
   ply:SetHealth(450)
   ply.HaloArmor=40
   ply:SetMaxHealth(450)
   ply:SetArmor(350)
end
})
TEAM_URFHEV = DarkRP.createJob("URF Squad Heavy", {
   color = Color(32, 32, 32, 255),
   model = {"models/ishi/suno/halo_rebirth/player/innie/male/innie_ray.mdl", "models/ishi/suno/halo_rebirth/player/innie/female/innie_dominique.mdl"},   
   description = [[URF Squad Heavy]],
   weapons = {"deika_plasmarifle_tfa", "cw_ws_scifi_aug", "tfa_ishi_m7", "mp_weapon_defender", "mp_weapon_mgl", "mp_weapon_smr", "tfa_rebirth_m41", "climb_swep2", "revival_c4", "csgo_huntsman", "tfa_rebirth_m394", "tfa_rebirth_m6g", "tfa_h4_spartan_weapon_saw", "tfa_ins2_kabar"},
   command = "URFHEV",
   max = 15,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Insurrection",
PlayerSpawn = function(ply)
   ply:SetHealth(550)
   ply.HaloArmor=40
   ply:SetMaxHealth(550)
   ply:SetArmor(450)
end
})
TEAM_URFSNIP = DarkRP.createJob("URF Squad Sniper", {
   color = Color(32, 32, 32, 255),
   model = {"models/ishi/suno/halo_rebirth/player/innie/male/innie_ray.mdl", "models/ishi/suno/halo_rebirth/player/innie/female/innie_dominique.mdl"},   
   description = [[URF Squad Sniper]],
   weapons = {"deika_plasmarifle_tfa", "cw_ws_scifi_aug", "csgo_huntsman", "tfa_ishi_m7", "tfa_ins2_codol_free", "tfa_wtno_mark1960", "climb_swep2", "invisibility_cloak", "m9k_nitro", "grapplehookv2", "tfa_rebirth_m394", "tfa_ishi_ma37", "tfa_rebirth_m6g", "tfa_ins2_kabar"},
   command = "URFSNIP",
   max = 15,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Insurrection",
PlayerSpawn = function(ply)
   ply:SetHealth(450)
   ply.HaloArmor=40
   ply:SetMaxHealth(450)
   ply:SetArmor(400)
end
})
TEAM_SOSDRONE = DarkRP.createJob("SoS Drone", {
color = Color(127, 0, 255, 255),
model = {"models/player/jds_166/hcn_drone_gen3_playermodel.mdl"},
description = [[The Yanme'e are human sized insectoids that are covered in a natural chitinous exoskeleton that affords them limited armor protection against modern weaponry. These insectoids assist the SoS as an aerial unit, with their wings and flying capabilities.]],
weapons = {"tfa_hr_swep_plasma_rifle","halo3_dualplasmapistol", "tfa_hr_swep_needler", "halorepair_tool","weapon_plasmanade","climb_swep2"},
command = "sosdrone",
max = 5,
salary = 0,
admin = 0,
vote = false,
hasLiscense = true,
candemote = false,
-- CustomCheck
medic = false,
chief = false,
mayor = false,
hobo = false,
cook = false,
category = "Swords of Sanghelios",
PlayerSpawn = function(ply)
ply:SetHealth(250)
ply.HaloArmor=37
ply:SetMaxHealth(150)
ply:SetArmor(50)
end
})
TEAM_SILENTSHADOW = DarkRP.createJob("SoS Silent Shadow" , {
color = Color(0, 0, 0, 255),
model = {"models/sangheili/silentshadow/silent_shadow_player.mdl"},
description = [[The Silent Shadows are a subdivision of the Swords of Sanghelios Special Operations Branch. These warriors fight in stealth with their blood-red energy swords and their modified infiltration harness that has advanced active-camouflage.]],
weapons = {"h3_energy_sword_red", "tfa_hr_swep_concussion_rifle", "weapon_cuff_admin", "h3_esword_tfa","invisibility_cloak","weapon_plasmanade","keys","climb_swep2","halo3_dualplasmarifle","h3_beam_rifle_tfa","tfa_hr_swep_concussion_rifle","tfa_hr_swep_needler","tfa_h2_covenant_carbine","h3_esword_tfa_red","tfa_hr_swep_plasma_rifle_shadow"},
command = "sosss",
max = 4,
salary = 0,
admin = 0,
vote = false,
hasLiscense = true,
candemote = false,
-- CustomCheck
medic = false,
chief = false,
mayor = false,
hobo = false,
cook = false,
category = "Swords of Sanghelios",
PlayerSpawn = function(ply)
ply:SetHealth(750)
ply.HaloArmor=88
ply:SetMaxHealth(750)
ply:SetArmor(600)
ply:SetRunSpeed(290)
ply:SetWalkSpeed(190)
ply:SetJumpPower(250)
end
})
TEAM_DEMONSQDL = DarkRP.createJob("Demon Squad Leader", {
   color = Color(32, 32, 32, 255),
   model = {"models/player/suno/spectre/spectre.mdl"},   
   description = [[The Demon Squad]],
   weapons = {"tfa_doom_gauss", "deika_plasmarifle_tfa", "tfa_ishi_ma37", "tfa_rebirth_m6cs", "tfa_ins2_codol_free", "tfa_rec_hmin", "tfa_rebirth_m45e", "tfa_wtno_mark1960", "weapon_policeshield", "stungun", "weapon_cuff_mp", "Climb_swep2", "Weapon_sh_flashbang", "bw2_blauncher", "Weapon_slam", "Halo_frag", "csgo_huntsman"},
   command = "DEMONSQDL",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Demon Squad",
PlayerSpawn = function(ply)
   ply:SetHealth(400)
   ply.HaloArmor=40
   ply:SetMaxHealth(400)
   ply:SetArmor(300)
end
})
TEAM_DEMONSQDA = DarkRP.createJob("Demon Squad Assault", {
   color = Color(32, 32, 32, 255),
   model = {"models/player/suno/spectre/spectre.mdl"},   
   description = [[The Demon Squad]],
   weapons = {"deika_plasmarifle_tfa", "tfa_ishi_ma37", "tfa_rebirth_m6cs","Climb_swep2", "Weapon_sh_flashbang", "bw2_blauncher", "Weapon_slam", "Halo_frag", "csgo_huntsman", "Weapon_sh_doorcharge"},
   command = "DEMONSQDA",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Demon Squad",
PlayerSpawn = function(ply)
   ply:SetHealth(350)
   ply.HaloArmor=40
   ply:SetMaxHealth(350)
   ply:SetArmor(300)
end
})
TEAM_DEMONSQDS = DarkRP.createJob("Demon Squad Specialist", {
   color = Color(32, 32, 32, 255),
   model = {"models/player/suno/spectre/spectre.mdl"},   
   description = [[The Demon Squad]],
   weapons = {"tfa_ins2_codol_free", "tfa_ishi_ma37", "tfa_rebirth_m6cs", "tfa_rebirth_m45e", "tfa_wtno_mark1960", "weapon_cuff_mp", "stungun", "Climb_swep2", "invisibility_cloak", "bw2_blauncher", "csgo_huntsman"},
   command = "DEMONSQDS",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Demon Squad",
PlayerSpawn = function(ply)
   ply:SetHealth(350)
   ply.HaloArmor=40
   ply:SetMaxHealth(350)
   ply:SetArmor(300)
end
})
TEAM_DEMONSQDH = DarkRP.createJob("Demon Squad Heavy", {
   color = Color(32, 32, 32, 255),
   model = {"models/player/suno/spectre/spectre.mdl"},   
   description = [[The Demon Squad]],
   weapons = {"deika_plasmarifle_tfa", "tfa_ishi_ma37", "tfa_rebirth_m6cs", "tfa_rec_hmin", "tfa_rebirth_m41", "bw2_blauncher", "halo_frag", "weapon_policeshield", "Climb_swep2", "Revival_c4", "csgo_huntsman" },
   command = "DEMONSQDH",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Demon Squad",
PlayerSpawn = function(ply)
   ply:SetHealth(450)
   ply.HaloArmor=40
   ply:SetMaxHealth(450)
   ply:SetArmor(400)
end
})
TEAM_DEMONSQDSN = DarkRP.createJob("Demon Squad Sniper", {
   color = Color(32, 32, 32, 255),
   model = {"models/player/suno/spectre/spectre.mdl"},   
   description = [[The Demon Squad]],
   weapons = {"deika_plasmarifle_tfa", "tfa_ishi_ma37", "tfa_rebirth_m6cs", "tfa_wtno_mark1960", "Climb_swep2", "invisibility_cloak", "bw2_blauncher", "halo_frag", "csgo_huntsman" },
   command = "DEMONSQDSN",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Demon Squad",
PlayerSpawn = function(ply)
   ply:SetHealth(350)
   ply.HaloArmor=40
   ply:SetMaxHealth(350)
   ply:SetArmor(300)
end
})
TEAM_INFERNOSQL = DarkRP.createJob("Inferno Squad Lead", {
   color = Color(32, 32, 32, 255),
   model = {"models/player/w4ys3rs_garage/terminus/opus_marine_male.mdl"},   
   description = [[The Inferno Squad]],
   weapons = {"tfa_doom_gauss", "tfa_rebirth_ma37g", "halo_flamethrower ", "tfa_rebirth_m6cs", "tfa_ins2_codol_free", "tfa_rec_hmin", "tfa_rebirth_m90c", "tfa_wtno_mark1960", "stungun", "weapon_cuff_mp", "Climb_swep2", "Weapon_sh_flashbang", "bw2_blauncher", "revival_c4", "Halo_frag", "csgo_huntsman"},
   command = "INFERNOSQL",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Inferno Squad",
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply.HaloArmor=40
   ply:SetMaxHealth(300)
   ply:SetArmor(250)
end
})
TEAM_INFERNOA = DarkRP.createJob("Inferno Squad Assault", {
   color = Color(32, 32, 32, 255),
   model = {"models/player/w4ys3rs_garage/terminus/opus_marine_male.mdl"},   
   description = [[The Inferno Squad]],
   weapons = {"tfa_rebirth_ma37g", "halo_flamethrower ", "tfa_rebirth_m6cs","stungun","Climb_swep2", "Weapon_sh_flashbang", "bw2_blauncher","Halo_frag", "csgo_huntsman"},
   command = "INFERNOA",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Inferno Squad",
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply.HaloArmor=40
   ply:SetMaxHealth(250)
   ply:SetArmor(200)
end
})
TEAM_INFERNOS = DarkRP.createJob("Inferno Squad Specialist", {
   color = Color(32, 32, 32, 255),
   model = {"models/player/w4ys3rs_garage/terminus/opus_marine_male.mdl"},   
   description = [[The Inferno Squad]],
   weapons = {"halo_flamethrower ", "tfa_rebirth_ma37g", "tfa_rebirth_m6cs", "tfa_wtno_mark1960", "tfa_rebirth_m90c", "stungun", "weapon_cuff_mp", "Climb_swep2", "invisibility_cloak", "bw2_blauncher", "csgo_huntsman" },
   command = "INFERNOS",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Inferno Squad",
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply.HaloArmor=40
   ply:SetMaxHealth(250)
   ply:SetArmor(200)
end
})
TEAM_INFERNOH = DarkRP.createJob("Inferno Squad Heavy", {
   color = Color(32, 32, 32, 255),
   model = {"models/player/w4ys3rs_garage/terminus/opus_marine_male.mdl"},   
   description = [[The Inferno Squad]],
   weapons = {"halo_flamethrower ", "tfa_rebirth_ma37g", "tfa_rebirth_m6cs", "tfa_rec_hmini", "tfa_rebirth_m41", "bw2_blauncher", "Climb_swep2", "revival_c4", "csgo_huntsman"},
   command = "INFERNOH",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Inferno Squad",
PlayerSpawn = function(ply)
   ply:SetHealth(350)
   ply.HaloArmor=40
   ply:SetMaxHealth(350)
   ply:SetArmor(300)
end
})
TEAM_INFERNOSN = DarkRP.createJob("Inferno Squad Sniper", {
   color = Color(32, 32, 32, 255),
   model = {"models/player/w4ys3rs_garage/terminus/opus_marine_male.mdl"},   
   description = [[The Inferno Squad]],
   weapons = {"tfa_rebirth_ma37g", "tfa_rebirth_m6cs", "tfa_ins2_codol_free", "tfa_wtno_mark1960", "Climb_swep2", "invisibility_cloak", "csgo_huntsman"},
   command = "INFERNOSN",
   max = 10,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Inferno Squad",
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply.HaloArmor=40
   ply:SetMaxHealth(250)
   ply:SetArmor(200)
end
})
TEAM_BATTLEBOT = DarkRP.createJob("Battlebot Clasina", {
   color = Color(32, 32, 32, 255),
   model = {"models/dizcordum/reaper.mdl"},
   description = [[ONI Battlebot]],
   weapons = {"tfa_rebirth_ma37s", "halo_grav_hammer", "tfa_rebirth_srs99c2s", "csgo_huntsman", "climb_swep2", "weapon_cuff_mp", "h3_esword_tfa", "invisibility_cloak", "weapon_sh_flashbang", "weapon_bactanade", "halo_forerunner_weapon_binary", "bw2_bvolver"},
   command = "battlebot",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "ONI",
   PlayerSpawn = function(ply)
   ply:SetMaxHealth(800)
   ply:SetHealth(800)
   ply:SetArmor(400)
   ply.HaloArmor=95
   ply:SetRunSpeed(330)
   ply:SetWalkSpeed(210)
   ply:SetJumpPower(280)
end
})
TEAM_ARTEMISBATTLEBOT = DarkRP.createJob("ONI Battlebot Artemis", {
   color = Color(32, 32, 32, 255),
   model = {"models/ninja/mgs4_haven_trooper.mdl"},
   description = [[You are ONI BATTLEBOT Artemis. (Custom Class)]],
   weapons = {"tfa_rebirth_m7ds", "tfa_hr_swep_spartan_laser","tfa_rebirth_m6cs", "tfa_rebirth_m90", "tfa_doom_gauss", "tfa_rebirth_srs99s4am", "csgo_huntsman", "med_kit", "darkrp_defibrillator", "weapon_bactanade", "weapon_bactainjector", "invisibility_cloak", "tfa_rebirth_ma37g", "climb_swep2"},
   command = "battlebotart",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "ONI",
   PlayerSpawn = function(ply)
   ply:SetMaxHealth(500)
   ply:SetHealth(500)
   ply:SetArmor(400)
   ply.HaloArmor=75
end
})
TEAM_GENERALARMY = DarkRP.createJob("General of the Army", {
   color = Color(0, 102, 0, 255),
   model = {"models/chaosnarwhal/unscab_oni_outlander_base2.mdl"},
   description =  [[Phalanx commander]] ,
   weapons = {"Climb_swep2", "Halo_frag", "Radio", "Tfa_rebirth_m7ds", "Voice_amplifier", "Weapon_sh_flashbang", "weapon_cuff_mp", "invisibility_cloak", "tfa_l4d2_kfkat", "csgo_huntsman", "tfa_rebirth_br55", "Tfa_rebirth_m394", "Tfa_rebirth_m45e", "tfa_rebirth_m6cs", "Tfa_ishi_ma37", "jorge_chaingun", "revival_c4"},
   command = "GeneralArmy",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Phalanx Commander’s",
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply.HaloArmor=30
   ply:SetArmor(250)
   ply:SetMaxHealth(300)
end
})
TEAM_XOARMY = DarkRP.createJob("Phalanx Battalion XO", {
   color = Color(0, 102, 0, 255),
   model = {"models/chaosnarwhal/unscab_oni_outlander_base2.mdl"},
   description =  [[Phalanx XO]] ,
   weapons = {"Climb_swep2", "invisibility_cloak", "Halo_frag", "Radio", "csgo_huntsman", "Weapon_sh_flashbang", "Voice_amplifier", "weapon_cuff_mp", "Tfa_ishi_ma37", "tfa_rebirth_m6cs", "Tfa_rebirth_m392", "tfa_rebirth_m45e", "tfa_rebirth_m7ds", "tfa_rebirth_br55"},
   command = "XOARMY",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Phalanx Commander’s",
PlayerSpawn = function(ply)
   ply:SetHealth(250)
   ply.HaloArmor=30
   ply:SetArmor(200)
   ply:SetMaxHealth(250)
end
})
TEAM_SMAARMY = DarkRP.createJob("SMA of the Army", {
   color = Color(0, 102, 0, 255),
   model = {"models/chaosnarwhal/unscab_oni_outlander_base2.mdl"},
   description =  [[Phalanx XO]] ,
   weapons = {"tfa_rebirth_m392", "Halo_frag", "Radio", "csgo_huntsman", "Weapon_sh_flashbang", "weapon_cuff_mp", "Voice_amplifier", "invisibility_cloak", "Tfa_ishi_ma37", "Tfa_rebirth_m6cs", "Tfa_rebirth_m7ds", "Tfa_rebirth_m45e", "climb_swep2","tfa_rebirth_srs99c2s"},
   command = "SMAARMY",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Phalanx Commander’s",
PlayerSpawn = function(ply)
   ply:SetHealth(200)
   ply.HaloArmor=30
   ply:SetArmor(150)
   ply:SetMaxHealth(200)
end
})
TEAM_CRIMSONSQUADLEADER = DarkRP.createJob("Crimson Fireteam Leader", {
   color = Color(128,0,0,255),
   model = {"models/valk/haloreach/UNSC/Spartan/Spartan.mdl","models/suno/player/zeus/female/fang.mdl"},
   description = [[NULL DATA]],
   weapons = {"tfa_rec_hmini","tfa_rebirth_srs99s4am","weapon_sh_doorcharge","tfa_hr_swep_assault_rifle","Tfa_h5_br85n_h5","tfa_hr_swep_magnum","climb_swep2","weapon_fistsofreprisal","tfa_hr_swep_shotgun","tfa_rebirth_m7ds","stungun","weapon_cuff_mp","invisibility_cloak","halo_frag","weapon_sh_flashbang","tfa_hr_swep_spartan_laser"},
   command = "crimsonsquadleader",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Crimson",
   ammo = {["item_ammo_smg1_grenade"] = 50},
PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply.HaloArmor=70
   ply:SetArmor(500)
   ply:SetMaxHealth(600)
   ply:SetRunSpeed(330)
   ply:SetWalkSpeed(210)
   ply:SetJumpPower(280)
end
})
TEAM_CRIMSONRIFLEMAN = DarkRP.createJob("Crimson Rifleman", {
   color = Color(128,0,0,255),
   model = {"models/valk/haloreach/UNSC/Spartan/Spartan.mdl","models/suno/player/zeus/female/fang.mdl"},
   description = [[NULL DATA]],
   weapons = {"weapon_policeshield","tfa_hr_swep_shotgun","tfa_hr_swep_assault_rifle","tfa_hr_swep_dmr","tfa_hr_swep_magnum","climb_swep2","weapon_fistsofreprisal","weapon_bactainjector","weapon_bactanade","med_kit","darkrp_defibrillator","tfa_rebirth_m7ds","invisibility_cloak","stungun","weapon_cuff_mp","halo_frag","weapon_sh_flashbang","weapon_sh_doorcharge"},
   command = "crimsonrifleman",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Crimson",
   ammo = {["item_ammo_smg1_grenade"] = 50},
PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply.HaloArmor=70
   ply:SetArmor(500)
   ply:SetMaxHealth(600)
   ply:SetRunSpeed(330)
   ply:SetWalkSpeed(210)
   ply:SetJumpPower(280)
end
})
TEAM_CRIMSONSUPPORT = DarkRP.createJob("Crimson Support", {
   color = Color(128,0,0,255),
   model = {"models/valk/haloreach/UNSC/Spartan/Spartan.mdl","models/suno/player/zeus/female/fang.mdl"},
   description = [[NULL DATA]],
   weapons = {"tfa_rec_hmini","tfa_rebirth_m41","tfa_hr_swep_assault_rifle","tfa_hr_swep_magnum","climb_swep2","weapon_fistsofreprisal","tfa_hr_swep_shotgun","tfa_rebirth_m7ds","invisibility_cloak","revival_c4","stungun","weapon_cuff_mp","halo_frag","weapon_sh_flashbang","weapon_sh_doorcharge"},
   command = "crimsonsupport",
   max = 2,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Crimson",
   ammo = {["item_ammo_smg1_grenade"] = 50},
PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply.HaloArmor=70
   ply:SetArmor(500)
   ply:SetMaxHealth(600)
   ply:SetRunSpeed(330)
   ply:SetWalkSpeed(210)
   ply:SetJumpPower(280)
end
})
TEAM_CRIMSONSHARPSHOOTER = DarkRP.createJob("Crimson Sharpshooter", {
   color = Color(128,0,0,255),
   model = {"models/valk/haloreach/UNSC/Spartan/Spartan.mdl","models/suno/player/zeus/female/fang.mdl", "models/suno/player/zeus/male/formal_bill.mdl","models/suno/player/zeus/male/formal_louis.mdl"},
   description = [[NULL DATA]],
   weapons = {"tfa_hr_swep_shotgun","tfa_rebirth_srs99s4am","tfa_hr_swep_assault_rifle","tfa_hr_swep_magnum","climb_swep2","weapon_fistsofreprisal","tfa_rebirth_m7ds","invisibility_cloak","stungun","weapon_cuff_mp","halo_frag","weapon_sh_flashbang","weapon_sh_doorcharge"},
   command = "crimsonsharpshooter",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Crimson",
   ammo = {["item_ammo_smg1_grenade"] = 50},
PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply.HaloArmor=70
   ply:SetArmor(500)
   ply:SetMaxHealth(600)
   ply:SetRunSpeed(330)
   ply:SetWalkSpeed(210)
   ply:SetJumpPower(280)
end
})
TEAM_WARDOGS = DarkRP.createJob("War Dogs", {
   color = Color(218, 242, 67, 255),
   model = {"models/ishi/halo_rebirth/player/odst/female/odst_bella.mdl","models/ishi/halo_rebirth/player/odst/female/odst_dominique.mdl","models/ishi/halo_rebirth/player/odst/male/odst_snippy.mdl","models/ishi/halo_rebirth/player/odst/male/odst_jeffrey.mdl"},
   description = [[You are the War Dogs of the marines. Essentially a more equipped elite unit meant to be the frontline of the UNSC Marine Force.]],
   weapons = {"climb_swep2","csgo_huntsman","halo_frag","tfa_rebirth_m90","tfa_rebirth_ma37s", "tfa_rebirth_m394b", "tfa_rebirth_m7ds","tfa_rebirth_m6cs", "tfa_rebirth_srs99s2am", "weapon_slam", "weapon_camo"},
   command = "wardogs",
   max = 15,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "War Dogs",
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply:SetArmor(100)
   ply.HaloArmor=55
   ply:SetRunSpeed(250)
   ply:SetMaxHealth(300)
end
})

TEAM_ONIBOTV = DarkRP.createJob("ONI Battlebot V0yd", {
   color = Color(218, 242, 67, 255),
   model = {"models/blagod/mass_effect/pm/cerberus/nemesis_cerb.mdl"},
   description = [[You are ONI BATTLEBOT V0yd. (Custom Class)]],
   weapons = {"climb_swep2","h3_odst_socom_tfa","darkrp_defibilator","tfa_h4_spartan_weapon_saw","halo_grav_hammer","weapon_camo","csgo_huntsman","weapon_bactainjector","tfa_h5_br85n_h5","tfa_rebirth_m45e", "halo_forerunner_weapon_binary", "revival_c4", "tfa_rebirth_srs99s2am", "tfa_rebirth_xbr55", "weapon_cuff_mp", "stungun", "weapon_bactanade","weapon_sh_flashbang","tfa_hr_swep_spartan_laser","tfa_rebirth_m45s","halo_flamethrower","invisibility_cloak"},
   command = "battlebotv",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "ONI",
PlayerSpawn = function(ply)
   ply:SetHealth(1300)
   ply:SetMaxHealth(1300)
   ply.HaloArmor=99
   ply:SetArmor(600)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(210)
   ply:SetMaxHealth(800)
end
})
TEAM_TLEADER = DarkRP.createJob("Trident Fireteam Leader", {
   color = Color(75,0,130,255),
   model = {"models/valk/haloreach/UNSC/Spartan/Spartan.mdl","models/suno/player/zeus/female/fang.mdl", "models/suno/player/zeus/male/formal_bill.mdl"},
   description = [[NULL DATA]],
   weapons = {"tfa_hr_swep_shotgun", "tfa_hr_swep_dmr", "tfa_hr_swep_magnum", "tfa_hr_swep_assault_rifle", "tfa_hr_swep_assault_rifle", "tfa_hr_swep_spartan_laser", "tfa_h4_spartan_weapon_saw ", "tfa_rebirth_srs99s4am", "weapon_bactanade", "weapon_bactainjector", "stungun", "tfa_rebirth_m7ds", "weapon_cuff_mp", "tfa_h5_br85n_h5", "invisibility_cloak", "halo_frag", "weapon_sh_flashbang", "weapon_bactainjector ", "tfa_rebirth_m6c ", "tfa_h4_spartan_weapon_saw ", "tfa_rebirth_srs_99s2am ", "tfa_rebirth_ma37", "climb_swep2", "weapon_fistsofreprisal", "tfa_rec_hmini", "tfa_rebirth_m45e", "tfa_rebirth_m41"},
   command = "tleader",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Trident",
   PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply.HaloArmor=75
   ply:SetArmor(400)
   ply:SetMaxHealth(600)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(200)
end
})
TEAM_TSUPPORT1 = DarkRP.createJob("Trident Support", {
   color = Color(75,0,130,255),
   model = {"models/valk/haloreach/UNSC/Spartan/Spartan.mdl","models/suno/player/zeus/female/fang.mdl", "models/suno/player/zeus/female/hawke.mdl"},
   description = [[NULL DATA]],
   weapons = {"tfa_rebirth_m7ds", "tfa_hr_swep_assault_rifle", "tfa_hr_swep_magnum", "tfa_hr_swep_shotgun", "invisibility_cloak", "stungun", "weapon_cuff_mp", "halo_frag", "tfa_h5_br85n_h5", "weapon_sh_flashbang", "tfa_rebirth_m41", "tfa_rec_hmini", "tfa_rebirth_ma37", "climb_swep2", "weapon_fistsofreprisal", "weapon_bactainjector", "tfa_rebirth_m7d"},
   command = "tsupport1",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Trident",
   PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply.HaloArmor=75
   ply:SetArmor(400)
   ply:SetMaxHealth(600)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(200)
end
})
TEAM_TSHARPSHOT = DarkRP.createJob("Trident Sharpshooter", {
   color = Color(75,0,130,255),
   model = {"models/valk/haloreach/UNSC/Spartan/Spartan.mdl", "models/suno/player/zeus/female/hawke.mdl","models/suno/player/zeus/male/formal_bill.mdl","models/suno/player/zeus/male/formal_louis.mdl"},
   description = [[NULL DATA]],
   weapons = {"tfa_rebirth_m7ds", "tfa_hr_swep_assault_rifle", "tfa_hr_swep_magnum", "tfa_hr_swep_shotgun", "tfa_rebirth_srs99s4am", "invisibility_cloak", "stungun", "weapon_cuff_mp", "halo_frag", "tfa_h5_br85n_h5", "weapon_sh_flashbang", "tfa_rebirth_srs_99s2am ", "tfa_hr_swep_dmr", "tfa_rebirth_ma37", "climb_swep2", "weapon_fistsofreprisal", "tfa_rebirth_srs99s2am"},
   command = "tsharpshot",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Trident",
   PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply.HaloArmor=75
   ply:SetArmor(400)
   ply:SetMaxHealth(600)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(200)
end
})
TEAM_TSCOUT = DarkRP.createJob("Trident Rifleman", {
   color = Color(75,0,130,255),
   model = {"models/valk/haloreach/UNSC/Spartan/Spartan.mdl", "models/suno/player/zeus/female/female_02.mdl", "models/suno/player/zeus/female/hawke.mdl"},
   description = [[NULL DATA]],
   weapons = {"tfa_rebirth_m7ds", "tfa_hr_swep_assault_rifle", "tfa_hr_swep_magnum", "tfa_hr_swep_shotgun", "invisibility_cloak", "stungun", "weapon_cuff_mp", "halo_frag", "tfa_h5_br85n_h5", "weapon_sh_flashbang", "weapon_bactanade", "weapon_bactainjector", "darkrp_defibrillator", "tfa_rebirth_ma37", "climb_swep2", "weapon_fistsofreprisal", "invisibility_cloak", "tfa_rebirth_m7ds"},
   command = "tscout",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Trident",
   PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply.HaloArmor=75
   ply:SetArmor(400)
   ply:SetMaxHealth(600)
   ply:SetRunSpeed(310)
   ply:SetWalkSpeed(200)
end
})
TEAM_ORIONSQL = DarkRP.createJob("Orion Squad Leader", {
   color = Color(60,179,113,255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl","models/ishi/halo_rebirth/player/odst/female/odst_miia.mdl"},
   description = [[NULL DATA]],
   weapons = {"tfa_rebirth_m7ds", "tfa_rebirth_m90","tfa_ishi_ma37","tfa_rebirth_srs99c2b","tfa_rebirth_m6cs", "climb_swep2", "halo_frag", "weapon_sh_flashbang", "revival_c4", "csgo_huntsman" }, -- Done by Head General 4/14 (Changed Weapons)
   command = "orionsql",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Orion",
   PlayerSpawn = function(ply) -- Done by Head General 4/14 (Changed Stats)
   ply:SetHealth(350)
   ply.HaloArmor=55
   ply:SetMaxHealth(350)
end
})
TEAM_ORIONOP = DarkRP.createJob("Orion Operator", {
   color = Color(60,179,113,255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl","models/ishi/halo_rebirth/player/odst/female/odst_miia.mdl"},
   description = [[NULL DATA]],
   weapons = {"tfa_rebirth_m7ds", "tfa_rebirth_m90", "tfa_ishi_ma37", "tfa_rebirth_xbr55", "tfa_rebirth_m6cs", "med_kit", "weapon_bactainjector", "darkrp_defibrillator", "climb_swep2", "halo_frag", "weapon_sh_flashbang", "csgo_huntsman" }, -- Done by Head General 4/14 (Changed Weapons)
   command = "orionop",
   max = 2,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Orion",
   PlayerSpawn = function(ply) -- Done by Head General 4/14 (Changed Stats)
   ply:SetHealth(300)
   ply.HaloArmor=55
   ply:SetMaxHealth(300)
end
})
TEAM_ORIONHVY = DarkRP.createJob("Orion Heavy", {
   color = Color(60,179,113,255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl","models/ishi/halo_rebirth/player/odst/female/odst_miia.mdl"},
   description = [[NULL DATA]],
   weapons = {"tfa_rebirth_m7ds", "tfa_rebirth_m90", "tfa_ishi_ma37", "tfa_rec_hmini", "tfa_rebirth_m6cs","climb_swep2", "halo_frag", "weapon_sh_flashbang", "revival_c4", "weapon_sh_flashbang", "csgo_huntsman" }, -- Done by Head General 4/14 (Changed Weapons)
   command = "orionhvy",
   max = 2,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Orion",
   PlayerSpawn = function(ply) -- Done by Head General 4/14 (Changed Stats)
   ply:SetHealth(375)
   ply.HaloArmor=60
   ply:SetMaxHealth(375)
end
})
TEAM_ORIONHNT = DarkRP.createJob("Orion Hunter", {
   color = Color(60,179,113,255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl","models/ishi/halo_rebirth/player/odst/female/odst_miia.mdl"},
   description = [[NULL DATA]],
   weapons = {"tfa_rebirth_m7ds", "tfa_rebirth_m90", "tfa_ishi_ma37", "tfa_rebirth_srs99c2b", "tfa_rebirth_m6cs", "climb_swep2", "halo_frag", "weapon_sh_flashbang", "invisibility_cloak", "weapon_sh_flashbang", "csgo_huntsman" }, -- Done by Head General 4/14 (Changed Weapons)
   command = "orionhnt",
   max = 2,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Orion",
   PlayerSpawn = function(ply) -- Done by Head General 4/14 (Changed Stats)
   ply:SetHealth(300)
   ply.HaloArmor=55
   ply:SetMaxHealth(300)
end
})
TEAM_EXODUS = DarkRP.createJob("Exodus", {
   color = Color(50,50,50,255),
   model = {"models/ishi/halo_rebirth/player/odst/male/odst_eyecberg.mdl"},
   description = [[NULL DATA]],
   weapons = {"tfa_h5_br85n_h5", "Tfa_rebirth_m45s", "hr_tfa_shotgun", "Tfa_rebirth_m6cs", "Tfa_rebirth_m7ds", "Tfa_rebirth_srs99c2b", "tfa_ishi_ma37", "Tfa_hr_spartan_laser", "Invisibility_cloak", "Climb_swep2", "keys", "Halo_frag", "weapon_sh_flashbang", "Stungun", "Weapon_bactainjector", "Weapon_bactanade", "Med_kit", "Darkrp_defibrillator", "weapon_cuff_admin" },
   command = "exodus",
   max = 5,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "HIGHCOM",
   PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply.HaloArmor=80
   ply:SetArmor(400)
   ply:SetMaxHealth(600)
   ply:SetRunSpeed(270)
   ply:SetWalkSpeed(170)
end
})
TEAM_ONISPLT = DarkRP.createJob("Spartan Executive", {
   color = Color(0, 0, 0, 0),
   model = {"models/valk/haloreach/UNSC/Spartan/Spartan.mdl","models/suno/player/zeus/female/female_02.mdl","models/suno/player/zeus/male/formal_bill.mdl"},
   description = [[DATA EXPUNGED]],
   weapons = {"weapon_bactainjector","weapon_bactanade","tfa_rebirth_srs99s4am","weapon_sh_doorcharge","tfa_hr_swep_assault_rifle","tfa_hr_swep_dmr","tfa_hr_swep_magnum","climb_swep2","weapon_fistsofreprisal","tfa_hr_swep_shotgun","tfa_rebirth_m7ds","tfa_rebirth_m41","stungun","weapon_cuff_mp","halo_frag","invisibility_cloak","tfa_hr_swep_spartan_laser"},
   command = "spartanexec",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "ONI",
   ammo = {["item_ammo_smg1_grenade"] = 50},
PlayerSpawn = function(ply)
   ply:SetHealth(800)
   ply:SetArmor(600)
   ply.HaloArmor=75
   ply:SetMaxHealth(800)
   ply:SetRunSpeed(325)
   ply:SetWalkSpeed(225)
   ply:SetJumpPower(300)
end
})
TEAM_DEVILDOG = DarkRP.createJob("Devil Dogs", {
   color = Color(210, 180, 140, 255),
   model = {"models/ishi/halo_rebirth/player/odst/female/odst_bella.mdl","models/ishi/halo_rebirth/player/odst/female/odst_dominique.mdl","models/ishi/halo_rebirth/player/odst/male/odst_snippy.mdl","models/ishi/halo_rebirth/player/odst/male/odst_jeffrey.mdl"},
   description = [[Redacted]],
   weapons = {"tfa_rec_hmini", "hr_tfa_shotgun", "Tfa_rebirth_m90", "Tfa_rebirth_srs99s2am", "Climb_swep2", "Halo_frag", "Tfa_rebirth_m394b", "Tfa_rebirth_m7ds", "Tfa_rebirth_m6cs", "Tfa_rebirth_ma37s", },
   command = "devildog",
   max = 4,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Devil Dogs",
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply:SetArmor(250)
   ply.HaloArmor=80
   ply:SetMaxHealth(300)
   ply:SetRunSpeed(275)
   ply:SetWalkSpeed(175)
end
})
-- Monitors
TEAM_MONITOR = DarkRP.createJob("Monitor", {
    color = Color(51, 51, 255, 255),
    model = {"models/halo/monitor/monitor.mdl", "models/halo/monitor/monitor_green.mdl", "models/halo/monitor/monitor_orange.mdl", "models/halo/monitor/monitor_purple.mdl", "models/halo/monitor/monitor_red.mdl", "models/halo/monitor/monitor_yellow.mdl"},
    description = [[DATA EXPUNGED]],
    weapons = {"sfw_ember", "hr_swep_spartan_laser"},
    command = "monitor",
    max = 5,
    salary = 0,
    admin = 0,
    vote = false,
    hasLicense = true,
    candemote = false,
    -- CustomCheck
    medic = false,
    chief = false,
    mayor = false,
    hobo = false,
    cook = false,
    category = "Forerunner",
    PlayerSpawn = function(ply)
        ply:SetHealth(3500)
        ply:SetArmor(700)
        ply.HaloArmor=15
    end
})
TEAM_SEN = DarkRP.createJob("Sentinel", { -- Done by Head General 4/11 (Fixed a new Job
    color = Color(51, 51, 51, 255),
    model = {"models/sentinelphys.mdl"},
    description = [[DATA EXPUNGED]],
    weapons = {"weapon_ginseng_babyrailgun"},
    command = "sentinel",
    max = 8,
    salary = 0,
    admin = 0,
    vote = false,
    hasLicense = true,
    candemote = false,
    -- CustomCheck
    medic = false,
    chief = false,
    mayor = false,
    hobo = false,
    cook = false,
    category = "Forerunner",
    PlayerSpawn = function(ply)
        ply:SetHealth(3000)
        ply:SetArmor(500)
        ply.HaloArmor=25
    end
})
-- FREELANCERS
TEAM_FAUSTCOMPANY = DarkRP.createJob("Faust Company", {
   color = Color(32, 32, 32, 255),
   model = {"models/player/lordvipes/h3_spartans_mps/h3spartan_mps_cvp.mdl"},
   description = [[DATA EXPUNGED]],
   weapons = {"tfa_rebirth_m45e","halo_flamethrower","tfa_rebirth_ma37", "tfa_rec_hmini","tfa_rebirth_m6g","invisibility_cloak","climb_swep2", "csgo_huntsman", "tfa_rebirth_m7ds", "tfa_rebirth_m41", "revival_c4", "weapon_bactainjector"},
   command = "FCAGENT",
   max = 12,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Freelancer",
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply.HaloArmor=75
   ply:SetArmor(400)
   ply:SetMaxHealth(300)
   ply:SetRunSpeed(290)
   ply:SetWalkSpeed(185)
end
})
TEAM_HELLHOUNDAGENT = DarkRP.createJob("Hellhound Agent", {
   color = Color(32, 32, 32, 255),
   model = {"models/player/lordvipes/h3_spartans_mps/h3spartan_mps_cvp.mdl"},
   description = [[DATA EXPUNGED]],
   weapons = {"tfa_rebirth_m45e","tfa_rebirth_srs99c2s", "tfa_rec_hmini","tfa_rebirth_xbr55s","invisibility_cloak","climb_swep2", "csgo_huntsman", "tfa_rebirth_m7ds", "tfa_rebirth_m41", "revival_c4", "stungun", "weapon_bactainjector"},
   command = "HHAGENT",
   max = 7,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Freelancer",
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply.HaloArmor=75
   ply:SetArmor(400)
   ply:SetMaxHealth(300)
   ply:SetRunSpeed(290)
   ply:SetWalkSpeed(185)
end
})
TEAM_NOXCOMPANY = DarkRP.createJob("Nox Company", {
   color = Color(32, 32, 32, 255),
   model = {"models/player/lordvipes/h3_spartans_mps/h3spartan_mps_cvp.mdl"},
   description = [[DATA EXPUNGED]],
   weapons = {"tfa_rebirth_m45e","tfa_rebirth_srs99c2s", "tfa_rec_hmini","tfa_rebirth_m394b","invisibility_cloak","climb_swep2", "csgo_huntsman", "tfa_rebirth_m7ds", "stungun", "weapon_cuff_mp", "weapon_bactainjector"},
   command = "NCAGENT",
   max = 13,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Freelancer",
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply.HaloArmor=75
   ply:SetArmor(400)
   ply:SetMaxHealth(300)
   ply:SetRunSpeed(290)
   ply:SetWalkSpeed(185)
end
})
TEAM_82NDAIRBORNE = DarkRP.createJob("82nd Airborne", {
   color = Color(32, 32, 32, 255),
   model = {"models/player/lordvipes/h3_spartans_mps/h3spartan_mps_cvp.mdl"},
   description = [[DATA EXPUNGED]],
   weapons = {"tfa_rebirth_m45e","tfa_rebirth_srs99c2b", "tfa_rec_hmini","tfa_rebirth_m394","invisibility_cloak","climb_swep2", "csgo_huntsman", "tfa_rebirth_m7ds", "halorepair_tool", "weapon_bactainjector"},
   command = "82ndAGENT",
   max = 8,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Freelancer",
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply.HaloArmor=75
   ply:SetArmor(400)
   ply:SetMaxHealth(300)
   ply:SetRunSpeed(290)
   ply:SetWalkSpeed(185)
end
})
TEAM_FREELANCER = DarkRP.createJob("Freelancer", {
   color = Color(32, 32, 32, 255),
   model = {"models/player/lordvipes/h3_spartans_mps/h3spartan_mps_cvp.mdl"},
   description = [[DATA EXPUNGED]],
   weapons = {"tfa_rebirth_srs99c2s","tfa_rec_hmini", "tfa_rebirth_xbr55", "tfa_rebirth_m45e", "invisibility_cloak","climb_swep2", "csgo_huntsman", "Tfa_rebirth_m7ds", "weapon_bactainjector"},
   command = "freelancer",
   max = 15,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Freelancer",
PlayerSpawn = function(ply)
   ply:SetHealth(300)
   ply.HaloArmor=75
   ply:SetArmor(400)
   ply:SetMaxHealth(300)
   ply:SetRunSpeed(290)
   ply:SetWalkSpeed(185)
end
})
TEAM_HELLHOUNDSQUADLEAD = DarkRP.createJob("Hellhound Squad Lead", {
	color = Color(32, 32, 32, 255),
	model = {"models/player/lordvipes/h3_spartans_mps/h3spartan_mps_cvp.mdl"},
	description = [[REDACTED]],
	weapons = {"Tfa_rebirth_m7ds", "Climb_swep2","csgo_huntsman","tfa_hr_swep_shotgun","invisibility_cloak","tfa_rebirth_m45e","Weapon_bactainjector","revival_c4","tfa_rebirth_m41","tfa_rebirth_m45e","tfa_rebirth_srs99c2s","tfa_rebirth_xbr55s","Weapon_sh_flashbang","tfa_rec_hmini"},
	command = "hellhoundsql",
	max = 2,
	salary = 0,
	admin = 0,
	vote = false,
	hasLicense = true,
	candemote = false,
	-- CustomCheck
	medic = false,
	chief = false,
	mayor = false,
	hobo = false,
	cook = false,
	category = "Hellhound Squad Lead",
PlayerSpawn = function(ply)
	ply:SetHealth(600)
	ply.HaloArmor=70
	ply:SetMaxHealth(600)
	ply:SetArmor(400)
	ply:SetRunSpeed(290)
    ply:SetWalkSpeed(185)
end
})
-- END OF FREELANCERS
TEAM_MONITOROI = DarkRP.createJob("Monitor of the Installation", {
    color = Color(51, 51, 255, 255),
    model = {"models/halo/monitor/monitor.mdl", "models/halo/monitor/monitor_green.mdl", "models/halo/monitor/monitor_orange.mdl", "models/halo/monitor/monitor_purple.mdl", "models/halo/monitor/monitor_red.mdl", "models/halo/monitor/monitor_yellow.mdl"},
    description = [[DATA EXPUNGED]],
    weapons = {"tfa_hr_spartan_laser", "defabricator", "weapon_ginseng_babyrailgun", "weapon_ginseng_beamgun"},
    command = "monitoroi",
    max = 1,
    salary = 0,
    admin = 0,
    vote = false,
    hasLicense = true,
    candemote = false,
    -- CustomCheck
    medic = false,
    chief = false,
    mayor = false,
    hobo = false,
    cook = false,
    category = "Forerunner",
    PlayerSpawn = function(ply)
        ply:SetHealth(5000)
        ply:SetArmor(700)
        ply.HaloArmor = 25
    end
})
TEAM_HEAVYSEN = DarkRP.createJob("Heavy Sentinel", {
    color = Color(51, 51, 51, 255),
    model = {"models/sentinelphys.mdl"},
    description = [[DATA EXPUNGED]],
    weapons = {"defabricator", "weapon_ginseng_babyrailgun", "weapon_ginseng_beamgun"},
    command = "heavysen",
    max = 3,
    salary = 0,
    admin = 0,
    vote = false,
    hasLicense = true,
    candemote = false,
    -- CustomCheck
    medic = false,
    chief = false,
    mayor = false,
    hobo = false,
    cook = false,
    category = "Forerunner",
    PlayerSpawn = function(ply)
        ply:SetHealth(10000) -- Done by Head General 4/11
        ply:SetArmor(500)
        ply.HaloArmor=50 -- Done by Head General 4/11
    end
})
TEAM_AGRESSORSEN = DarkRP.createJob("Aggressor Sentinel", { -- Done by Head General 4/11
    color = Color(51, 51, 51, 255),
    model = {"models/sentinelphys.mdl"},
    description = [[DATA EXPUNGED]],
    weapons = {"weapon_ginseng_beamgun", "weapon_ginseng_babyrailgun"},
    command = "agressorsen",
    max = 5,
    salary = 0,
    admin = 0,
    vote = false,
    hasLicense = true,
    candemote = false,
    -- CustomCheck
    medic = false,
    chief = false,
    mayor = false,
    hobo = false,
    cook = false,
    category = "Forerunner",
    PlayerSpawn = function(ply)
        ply:SetHealth(3000)
        ply:SetArmor(500)
        ply.HaloArmor=25 -- Done by Head General 4/11 (Fixed the Spacing)
    end
})
TEAM_ENFORCERSEN = DarkRP.createJob("Enforcer Sentinel", {
    color = Color(51, 51, 51, 255),
    model = {"models/sentinelphys.mdl"},
    description = [[DATA EXPUNGED]],
    weapons = {"weapon_ginseng_babyrailgun"},
    command = "enforsen",
    max = 8,
    salary = 0,
    admin = 0,
    vote = false,
    hasLicense = true,
    candemote = false,
    -- CustomCheck
    medic = false,
    chief = false,
    mayor = false,
    hobo = false,
    cook = false,
    category = "Forerunner",
    PlayerSpawn = function(ply)
        ply:SetHealth(3000)
        ply:SetArmor(500)
        ply.HaloArmor = 55
    end
})
TEAM_PROTECTORECUM = DarkRP.createJob("Protector of the Ecumene", {
    color = Color(51, 51, 255, 255),
    model = {"models/player/hobo387/didact.mdl"},
    description = [[DATA EXPUNGED]],
    weapons = {"tfa_hr_spartan_laser", "defabricator", "weapon_ginseng_babyrailgun", "weapon_ginseng_beamgun", "weapon_plasmanadelauncher", "weapon_150watt", "weapon_75watt", "weapon_73watt", "weapon_20watt","weapon_ar1"},
    command = "protector",
    max = 1,
    salary = 0,
    admin = 0,
    vote = false,
    hasLicense = true,
    candemote = false,
    -- CustomCheck
    medic = false,
    chief = false,
    mayor = false,
    hobo = false,
    cook = false,
    category = "Forerunner",
    PlayerSpawn = function(ply)
        ply:SetHealth(7500)
        ply:SetArmor(3500)
        ply.HaloArmor = 75
    end
})
TEAM_PROMKNIGHTCOM = DarkRP.createJob("Promethean Knight Commander", {
    color = Color(51, 51, 255, 255),
    model = {"models/player/hobo387/didact.mdl"},
    description = [[DATA EXPUNGED]],
    weapons = {"weapon_20watt", "weapon_40watt", "weapon_ar3", "weapon_stentnine"},
    command = "promknightcom",
    max = 1,
    salary = 0,
    admin = 0,
    vote = false,
    hasLicense = true,
    candemote = false,
    -- CustomCheck
    medic = false,
    chief = false,
    mayor = false,
    hobo = false,
    cook = false,
    category = "Forerunner",
    PlayerSpawn = function(ply)
        ply:SetHealth(3750)
        ply:SetArmor(800)
        ply.HaloArmor = 65
    end
})
TEAM_PROMKNIGHT = DarkRP.createJob("Promethean Knight", {
    color = Color(51, 51, 255, 255),
    model = {"models/player/hobo387/didact.mdl"},
    description = [[DATA EXPUNGED]],
    weapons = {"weapon_20watt", "weapon_40watt", "weapon_ar3", "weapon_stentnine"},
    command = "promknight",
    max = 1,
    salary = 0,
    admin = 0,
    vote = false,
    hasLicense = true,
    candemote = false,
    -- CustomCheck
    medic = false,
    chief = false,
    mayor = false,
    hobo = false,
    cook = false,
    category = "Forerunner",
    PlayerSpawn = function(ply)
        ply:SetHealth(3500)
        ply:SetArmor(750)
        ply.HaloArmor = 60
    end
})
TEAM_AGENTRAGNAR = DarkRP.createJob("Agent Ragnar", {
   color = Color(32, 32, 32, 255),
   model = {"models/player/lordvipes/h3_spartans_mps/h3spartan_mps_cvp.mdl"},
   description = [[DATA EXPUNGED]],
   weapons = {"tfa_rebirth_ma37m","tfa_rebirth_xbr55s","hr_tfa_shotgun","stungun","Handcuffs","Tfa_rebirth_srs99c2s","tfa_rec_hmini","invisibility_cloak","climb_swep2", "csgo_huntsman", "Tfa_rebirth_m7ds", "weapon_bactainjector"},
   command = "agentragnar",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Freelancer",
PlayerSpawn = function(ply)
   ply:SetHealth(1000)
   ply.HaloArmor=80
   ply:SetArmor(1000)
   ply:SetMaxHealth(1000)
   ply:SetRunSpeed(290)
   ply:SetWalkSpeed(185)
end
})
TEAM_AGENTNEVADA = DarkRP.createJob("Agent Nevada", {
   color = Color(32, 32, 32, 255),
   model = {"models/player/lordvipes/h3_spartans_mps/h3spartan_mps_cvp.mdl"},
   description = [[DATA EXPUNGED]],
   weapons = {"tfa_rebirth_m90","tfa_rebirth_srs99c2s", "tfa_rec_hmini","tfa_rebirth_m6cs","invisibility_cloak","climb_swep2", "csgo_huntsman", "tfa_rebirth_m7ds", "tfa_h2_sniper", "hr_tfa_shotgun", "tfa_rebirth_ma5d"},
   command = "agentnevada",
   max = 1,
   salary = 0,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Freelancer",
PlayerSpawn = function(ply)
   ply:SetHealth(600)
   ply.HaloArmor=80
   ply:SetArmor(700)
   ply:SetMaxHealth(300)
   ply:SetRunSpeed(290)
   ply:SetWalkSpeed(185)
end
})
--[[---------------------------------------------------------------------------
Define which team joining players spawn into and what team you change to if demoted
---------------------------------------------------------------------------]]
GAMEMODE.DefaultTeam = TEAM_RECRUIT
--[[---------------------------------------------------------------------------
Define which teams belong to civil protection
Civil protection can set warrants, make people wanted and do some other police related things
---------------------------------------------------------------------------]]
GAMEMODE.CivilProtection = {
	[TEAM_POLICE] = true,
	[TEAM_CHIEF] = true,
	[TEAM_MAYOR] = true,
}
--[[---------------------------------------------------------------------------
Jobs that are hitmen (enables the hitman menu)
---------------------------------------------------------------------------]]
DarkRP.addHitmanTeam(TEAM_MOB)